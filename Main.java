/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package fullauto;

/**
 *
 * @author Satoshi
 */
import java.awt.Rectangle;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JFileChooser;
import javax.swing.SwingWorker;

import fullauto.Main.Learn;
import fullauto.Main.Prune;

 public  class Main extends javax.swing.JFrame {

    static protected FileOperationForNN trainingSample;
    static protected FileOperationForNN testSample;
    static protected FileOperationForNN testSample2;
    static public FileOperationForNN correctSample;
    static AN[] inputLayer;
    static protected AN[] hiddenLayer;
    static protected AN[] outputLayer;
    static protected double maxError;
    static protected int numOfAccordance;
    static protected boolean stopStatus = false;
    static protected double rate;
    static protected int numOfCut = 0;
    static protected boolean[][] tempTrainableItoH;
    static protected boolean[][] tempTrainableHtoO;
    static protected double[][] tempWeightItoH;
    static protected double[][] tempWeightHtoO;
    static public FileOperationForNN cValidationSample;
    static public FileOperationForNN allSample;
    static protected boolean[][] beforePruningTrainableItoH;
    static protected boolean[][] beforePruningTrainableHtoO;
    static protected double[][] beforePruningWeightItoH;
    static protected double[][] beforePruningWeightHtoO;
    static protected boolean[][] networkTable;
    static public ArrayList<Object>[] list;
    static public int [] Alist;
    static protected int selectedTrainNum;
    static protected double selectedWeight;
    static protected boolean learningStatus = false;
    //protected ArrayList list = new ArrayList();
    
    static protected boolean pruningflg = false; // 学習後の画面表示の仕方の判別に使用
    static public String ProjectName = "AutoTest";	// プロジェクト名。保存先フォルダ名に使用。
    static public int RoopNum = 1;
    static public String[] RuleName = {"PrimaryRule", "SecondaryRule", "ThirdlyRule"};
    static public File TrainingData;
    static public File TestData;
    static public String LearningDataName = "LD";
    
    /** Creates new form Main */
    public Main() {
        initComponents();
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
     void initComponents() {

    	jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jScrollPane2 = new javax.swing.JScrollPane();
        statusView = new javax.swing.JTextArea();
        statusView2 = new javax.swing.JTextArea();
        viewFileName1 = new javax.swing.JTextField();
        viewFileName2 = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        numOfHiddenUnitsTF = new javax.swing.JTextField();
        errorThresholdTF = new javax.swing.JTextField();
        numOfMaxLoopTF = new javax.swing.JTextField();
        numOfMaxSimplexLoopTF = new javax.swing.JTextField();
        epsilonTF = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        doNormalizationCB = new javax.swing.JCheckBox();
        doEpsilonSimplexCB = new javax.swing.JCheckBox();
        startLearning = new javax.swing.JButton();
        startRecog = new javax.swing.JButton();
        clearBT = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        zerTF = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        momentum = new javax.swing.JTextField();
        pruningBt = new javax.swing.JButton();
        stopProcedure = new javax.swing.JButton();
        pruningThresholdTF = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        endPruningTF = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        viewCValidationFileName = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        crossValidationBt = new javax.swing.JButton();
        recogAllDataBt = new javax.swing.JButton();
        allDataTF = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        recallBeforePruningBt = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        openAllDataMI = new javax.swing.JMenuItem();
        openTrainingSampleMI = new javax.swing.JMenuItem();
        jMenuItem1 = new javax.swing.JMenuItem();
        openTestSampleMI = new javax.swing.JMenuItem();
        terminateProgramMI = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jFullAutoButton = new javax.swing.JButton();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        AddEndPruningTF = new javax.swing.JTextField();
        MaxPruningTF = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        ProjectNameTF = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        δ1TF = new javax.swing.JTextField();
        δ2TF = new javax.swing.JTextField();
        Sδ1TF = new javax.swing.JTextField();
        Sδ2TF = new javax.swing.JTextField();
        Tδ1TF = new javax.swing.JTextField();
        Tδ2TF = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        LDTF = new javax.swing.JTextField();
        LDfTF = new javax.swing.JTextField();
        LDffTF = new javax.swing.JTextField();
        
        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setText("ステータス表示");

        statusView.setColumns(20);
        statusView.setRows(5);
        jScrollPane1.setViewportView(statusView);
        
        jLabel18.setText("精度表示");
        
        statusView.setColumns(20);
        statusView.setRows(5);
        jScrollPane2.setViewportView(statusView2);

        viewFileName1.setEditable(false);

        viewFileName2.setEditable(false);

        jLabel2.setText("トレーニングサンプル");

        jLabel3.setText("テストサンプル");

        numOfHiddenUnitsTF.setText("1");

        errorThresholdTF.setText("0.01");

        numOfMaxLoopTF.setText("1000");

        numOfMaxSimplexLoopTF.setText("25");

        epsilonTF.setText("0.1");

        jLabel4.setText("中間層の数");

        jLabel5.setText("誤差閾値");

        jLabel6.setText("学習回数");

        jLabel14.setText("シンプレックス修正回数");

        jLabel7.setText("重みの修正");

        doNormalizationCB.setText("正規化");

        doEpsilonSimplexCB.setText("修正シンプレクス");

        jLabel8.setText("学習終了判定係数");

        zerTF.setText("0.4");

        jLabel9.setText("慣性項係数");

        momentum.setText("0.0");

        pruningThresholdTF.setText("0.3");

        jLabel10.setText("枝刈り頻度");

        endPruningTF.setText("0.875");

        jLabel11.setText("枝刈り終了エラーレート");
        
        jLabel15.setText("追加枝刈り終了エラーレート");
        AddEndPruningTF.setText("0.025");
        
        jLabel16.setText("最大枝刈り終了エラーレート");
        MaxPruningTF.setText("0.950");
        
        jLabel19.setText("1段目δ1");
        δ1TF.setText("0.07");
        jLabel20.setText("1段目δ2");
        δ2TF.setText("0.07");
        jLabel24.setText("2段目δ1");
        Sδ1TF.setText("1.00");
        jLabel25.setText("2段目δ2");
        Sδ2TF.setText("1.00");
        jLabel26.setText("3段目δ1");
        Tδ1TF.setText("1.00");
        jLabel27.setText("3段目δ2");
        Tδ2TF.setText("1.00");
        
        jLabel17.setText("プロジェクト名");
        ProjectNameTF.setText("AutoTest");

        allDataTF.setEditable(false);

        jLabel13.setText("全データ");
        
        jLabel21.setText("LD割合(%)");
        LDTF.setText("80");
        
        jLabel22.setText("LDf割合(%)");
        LDfTF.setText("100");
        
        jLabel23.setText("LDff割合(%)");
        LDffTF.setText("100");
        
        jFullAutoButton.setText("全自動ver1.0");
        jFullAutoButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jFullAutoButtonActionPerformed(evt);
            }
        });
        
        jMenu1.setText("File");

        openAllDataMI.setText("データセットを開く");
        openAllDataMI.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                openAllDataMIActionPerformed(evt);
            }
        });

        jMenu1.add(openAllDataMI);

        openTrainingSampleMI.setText("トレーニングサンプルを開く");
        openTrainingSampleMI.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                openTrainingSampleMIActionPerformed(evt);
            }
        });
        jMenu1.add(openTrainingSampleMI);
        
        openTestSampleMI.setText("テストサンプルを開く");
        openTestSampleMI.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                openTestSampleMIActionPerformed(evt);
            }
        });
        jMenu1.add(openTestSampleMI);

        terminateProgramMI.setText("プログラムの終了");
        terminateProgramMI.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                terminateProgramMIActionPerformed(evt);
            }
        });
        jMenu1.add(terminateProgramMI);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Edit");
        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)                   
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 542, javax.swing.GroupLayout.PREFERRED_SIZE))       
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)                  
                    .addComponent(jLabel18)                   
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 542, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        	.addComponent(jLabel2)
                            .addComponent(jLabel11)
                            .addComponent(jLabel10)
                            .addComponent(jLabel3)
                            .addComponent(jLabel4)
                            .addComponent(jLabel5)
                            .addComponent(jLabel6)
                            .addComponent(jLabel14)
                            .addComponent(jLabel7)
                            .addComponent(jLabel8)
                            .addComponent(jLabel9)                
                            .addComponent(jLabel13)
                            .addComponent(jLabel15)
                            .addComponent(jLabel16)
                            .addComponent(jLabel17)
                            .addComponent(jLabel19)
                            .addComponent(jLabel20)
                            .addComponent(jLabel21)
                            .addComponent(jLabel22)
                            .addComponent(jLabel23)
                            .addComponent(jLabel24)
                            .addComponent(jLabel25)
                            .addComponent(jLabel26)
                            .addComponent(jLabel27))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(momentum, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 230, Short.MAX_VALUE)
                            .addComponent(zerTF, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 230, Short.MAX_VALUE)
                            .addComponent(numOfHiddenUnitsTF, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 230, Short.MAX_VALUE)
                            .addComponent(viewFileName2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 230, Short.MAX_VALUE)
                            .addComponent(errorThresholdTF, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 230, Short.MAX_VALUE)
                            .addComponent(numOfMaxLoopTF, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 230, Short.MAX_VALUE)
                            .addComponent(numOfMaxSimplexLoopTF, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 230, Short.MAX_VALUE)
                            .addComponent(epsilonTF, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 230, Short.MAX_VALUE)
                            .addComponent(pruningThresholdTF, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 230, Short.MAX_VALUE)
                            .addComponent(endPruningTF, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 230, Short.MAX_VALUE)
                            .addComponent(viewFileName1, javax.swing.GroupLayout.DEFAULT_SIZE, 230, Short.MAX_VALUE)
                            .addComponent(allDataTF, javax.swing.GroupLayout.DEFAULT_SIZE, 230, Short.MAX_VALUE)
                            .addComponent(AddEndPruningTF, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 230, Short.MAX_VALUE)
                            .addComponent(MaxPruningTF, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 230, Short.MAX_VALUE)
                            .addComponent(ProjectNameTF, javax.swing.GroupLayout.DEFAULT_SIZE, 230, Short.MAX_VALUE)
                            .addComponent(δ1TF, javax.swing.GroupLayout.DEFAULT_SIZE, 230, Short.MAX_VALUE)
                            .addComponent(δ2TF, javax.swing.GroupLayout.DEFAULT_SIZE, 230, Short.MAX_VALUE)
                            .addComponent(Sδ1TF, javax.swing.GroupLayout.DEFAULT_SIZE, 230, Short.MAX_VALUE)
                            .addComponent(Sδ2TF, javax.swing.GroupLayout.DEFAULT_SIZE, 230, Short.MAX_VALUE)
                            .addComponent(Tδ1TF, javax.swing.GroupLayout.DEFAULT_SIZE, 230, Short.MAX_VALUE)
                            .addComponent(Tδ2TF, javax.swing.GroupLayout.DEFAULT_SIZE, 230, Short.MAX_VALUE)
                            .addComponent(LDTF, javax.swing.GroupLayout.DEFAULT_SIZE, 230, Short.MAX_VALUE)
                            .addComponent(LDfTF, javax.swing.GroupLayout.DEFAULT_SIZE, 230, Short.MAX_VALUE)
                            .addComponent(LDffTF, javax.swing.GroupLayout.DEFAULT_SIZE, 230, Short.MAX_VALUE)
                            .addComponent(doNormalizationCB, javax.swing.GroupLayout.DEFAULT_SIZE, 230, Short.MAX_VALUE)
                            .addComponent(doEpsilonSimplexCB, javax.swing.GroupLayout.DEFAULT_SIZE, 230, Short.MAX_VALUE)
                            ))

                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGap(87, 87, 87)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)                    
                            .addComponent(jFullAutoButton, javax.swing.GroupLayout.DEFAULT_SIZE, 165, Short.MAX_VALUE))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addComponent(jLabel18)
                .addGap(6, 6, 6)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE))
                                .addGap(223, 223, 223))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER, false))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel17)
                                    .addComponent(ProjectNameTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                		
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel13)
                                    .addComponent(allDataTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))		
                                
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel2)
                                    .addComponent(viewFileName1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))    
                                    
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel3)
                                    .addComponent(viewFileName2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))		
                                
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel4)
                                    .addComponent(numOfHiddenUnitsTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))    
                                
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel5)
                                    .addComponent(errorThresholdTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))    
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel6)
                                    .addComponent(numOfMaxLoopTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel14)
                                    .addComponent(numOfMaxSimplexLoopTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))    
                                    
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel9)
                                    .addComponent(epsilonTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel7)
                                    .addComponent(zerTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))    
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)  
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel8)
                                    .addComponent(momentum, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(pruningThresholdTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel10))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(endPruningTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel11))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(AddEndPruningTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel15))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(MaxPruningTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel16))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(δ1TF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel19))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(δ2TF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel20))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(Sδ1TF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel24))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(Sδ2TF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel25))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(Tδ1TF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel26))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(Tδ2TF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel27))    
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(LDTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel21))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(LDfTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel22))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(LDffTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel23))))    
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(doNormalizationCB)
                        .addComponent(doEpsilonSimplexCB)
                        .addGap(18, 18, 18)                    
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        	.addComponent(jFullAutoButton))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 650, Short.MAX_VALUE)//556
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 650, Short.MAX_VALUE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

  /**NNの重みの初期化
   *
   * @param inputBLayer
   * @param hiddenBLayer
   * @param outputBLayer
   * @param i
   */
    static public void backInit(AN[][] inputBLayer,AN[][] hiddenBLayer,AN[][] outputBLayer,int i){
    	//i番目のANNを初期値に戻す
    		//入力層初期化
		for (int j = 0; j < trainingSample.getNumOfInputs(); j++) {
			//元の乱数を入れる
			for(int k = 0;k< trainingSample.getNumOfInputs();k++){
				inputBLayer[i][j].weight[k] = inputLayer[j].weight[k];
				inputBLayer[i][j].trainable[k] = true;
			}
        }
		inputBLayer[i][trainingSample.getNumOfInputs()] = new AN(1.0);

		//隠れ層初期化
		//for (int j = 0; j < Integer.parseInt(numOfHiddenUnitsTF.getText()); j++) {
		for (int j = 0; j < 1; j++) {
			for(int k = 0;k< inputLayer.length;k++){
				hiddenBLayer[i][j].weight[k] = hiddenLayer[j].weight[k];
				hiddenBLayer[i][j].trainable[k] = true;
			}
        }
		//hiddenBLayer[i][Integer.parseInt(numOfHiddenUnitsTF.getText())] = new AN(1.0);
		hiddenBLayer[i][1] = new AN(1.0);

		//出力層初期化
		for (int j = 0; j < trainingSample.getNumOfOutputs(); j++) {
			for(int k = 0;k< hiddenLayer.length;k++){
				outputBLayer[i][j].weight[k] = outputLayer[j].weight[k];
				outputBLayer[i][j].trainable[k] = true;
			}
        }
    }
   
    static public void crossValidate() {
        final int numOfHiddenUnits = Integer.parseInt(numOfHiddenUnitsTF.getText());
        double epsilon[] = new double[3];
        epsilon[0] = Double.parseDouble(epsilonTF.getText());
        int loopMax;

        //誤差
        double error = 0.0;



        double yBack[], hBack[], netInput;
        double sumOfErrors = 0.0;
        for (loopMax = 0; loopMax < Integer.parseInt(numOfMaxLoopTF.getText()); loopMax++) {  //学習上限回数までループ
            if (stopStatus) {
                break;
            }
            maxError = -1.0;
            sumOfErrors = 0.0;
            yBack = new double[outputLayer.length];
            hBack = new double[numOfHiddenUnits + 1];

            for (int iSample = 0; iSample < cValidationSample.getNumOfSamples(); iSample++) {    //全サンプル数分のループ

                error = 0.0;
                for (int j = 0; j < cValidationSample.getNumOfInputs(); j++) {
                    inputLayer[j].forward(cValidationSample.getInput()[iSample][j]);
                }
                inputLayer[cValidationSample.getNumOfInputs()].forward(1.0);

                for (int j = 0; j < numOfHiddenUnits; j++) {
                    hiddenLayer[j].forward(inputLayer);
                }
                hiddenLayer[numOfHiddenUnits].forward(1.0);

                for (int j = 0; j < cValidationSample.getNumOfOutputs(); j++) {
                    outputLayer[j].forward(hiddenLayer);
                    error += Math.pow(cValidationSample.getTarget()[iSample][j] - outputLayer[j].getOutput(), 2.0);
                }

                error = error / 2;
                if (error > maxError) {
                    maxError = error;
                }
                sumOfErrors += error;
                //以下、誤差逆伝播セクション

                for (int j = 0; j < cValidationSample.getNumOfOutputs(); j++) {
                    yBack[j] = (outputLayer[j].getOutput() - cValidationSample.getTarget()[iSample][j]) * (1.0 - outputLayer[j].getOutput()) * outputLayer[j].getOutput() + Double.parseDouble(momentum.getText()) * yBack[j];
                }
                for (int i = 0; i < numOfHiddenUnits + 1; i++) {
                    netInput = 0.0;
                    for (int j = 0; j < outputLayer.length; j++) {
                        netInput += outputLayer[j].getWeight()[i] * yBack[j];
                    }
                    hBack[i] = netInput * (1.0 - hiddenLayer[i].getOutput()) * hiddenLayer[i].getOutput() + Double.parseDouble(momentum.getText()) * hBack[i];
                }
                for (int i = 0; i < cValidationSample.getNumOfInputs() + 1; i++) {
                    for (int j = 0; j < numOfHiddenUnits; j++) {
                        if (hiddenLayer[j].isTrainable(i)) {
                            hiddenLayer[j].setWeight(i, hiddenLayer[j].getWeight()[i] - epsilon[0] * inputLayer[i].getOutput() * hBack[j]);
                        }
                    }
                }
                for (int i = 0; i < numOfHiddenUnits + 1; i++) {
                    for (int j = 0; j < cValidationSample.getNumOfOutputs(); j++) {
                        if (outputLayer[j].isTrainable(i)) {
                            outputLayer[j].setWeight(i, outputLayer[j].getWeight()[i] - epsilon[0] * hiddenLayer[i].getOutput() * yBack[j]);
                        }
                    }
                }
            }
            //エラーが閾値以下になったら学習を終了する
            if ((Double.parseDouble(errorThresholdTF.getText()) > maxError)) {
                break;
            }


        }

        double averageError = sumOfErrors / cValidationSample.getNumOfSamples();
        statusView.append("相互検証完了\n");
        statusView.append("\t学習回数 : " + String.valueOf(loopMax) + "\n");
        statusView.append("\t最大誤差 : " + String.valueOf(maxError) + "\n");
        statusView.append("\t平均誤差 : " + String.valueOf(averageError) + "\n");
    }

   
    // グローバル変数のルール名を変更する
//    static public void RuleNameSet() {
//	   if(RoopNum == 1) RuleName = "PrimaryRule";
//	   else if(RoopNum == 2) RuleName = "SecondaryRule";
//	   else if(RoopNum == 3) RuleName = "ThirdlyRule";
//	   else { RuleName = "aaaaaaaaa"; statusView.append("### RuleNameがループ回数４回以上を想定していません ###"); }
//    }
    
    // グローバル変数のLD名を変更する
    static public void LearningDataNameSet() {
    	if(RoopNum == 1) LearningDataName = "LD";
    	else if(RoopNum == 2) LearningDataName = "LDf";
    	else if(RoopNum == 3) LearningDataName = "LDff";
 	   	else { statusView.append("### LearningDataNameがループ回数４回以上を想定していません ###"); }

    }
    
    static public void learning() {

    	int loopMax = 0,fh = -1,fg = -1,fl = -1,sloop = Integer.parseInt(numOfMaxSimplexLoopTF.getText());
        final int numOfHiddenUnits = Integer.parseInt(numOfHiddenUnitsTF.getText());
        //error:Simplex法における関数f,epsilon:修正する点
    	int n=2;
    	int m=n+4;//頂点の数
        double yBack[], hBack[], netInput;
        double sumOfErrors = 0.0;
        double epsilon[] = new double[m];
        double error[] =new double[m];
        double simplex[][] = new double[m][n];
        epsilon[0] = Double.parseDouble(epsilonTF.getText());
        if (doEpsilonSimplexCB.isSelected()){//Simplex法適用（epsilon) || numOfHiddenUnits == 1
        	double r1 = 0.5,yr,yn,r2 = 1.5,xg[],xn[],xr[],e,eps = 1.0e-20,count = 0;

        	xg = new double [n];
        	xn = new double [n];
        	xr = new double [n];
        	AN[][] inputSimplexLayer = new AN[m][trainingSample.getNumOfInputs() + 1];
        	AN[][] hiddenSimplexLayer = new AN[m][Integer.parseInt(numOfHiddenUnitsTF.getText()) + 1];
        	AN[][] outputSimplexLayer = new AN[m][trainingSample.getNumOfOutputs()];
        	//i:0~2=シンプレクス法に使用する点,3：ループした後戻すべき数値
        	for(int i = 0;i < m;i++){
        		//入力層初期化
        		for (int j = 0; j < trainingSample.getNumOfInputs(); j++) {
        			//初期化
        			inputSimplexLayer[i][j] = new AN(trainingSample.getNumOfInputs());
        			//元の乱数を入れる
                }
        		inputSimplexLayer[i][trainingSample.getNumOfInputs()] = new AN(1.0);
        		//隠れ層初期化
        		for (int j = 0; j < numOfHiddenUnits; j++) {
        			hiddenSimplexLayer[i][j] = new AN(inputLayer.length);

                }
        		hiddenSimplexLayer[i][numOfHiddenUnits] = new AN(1.0);
        		//出力層初期化
        		for (int j = 0; j < trainingSample.getNumOfOutputs(); j++) {
        			outputSimplexLayer[i][j] = new AN(hiddenLayer.length);
                }
        	}
        	for(int i = 0;i<m;i++)
        		backInit(inputSimplexLayer,hiddenSimplexLayer,outputSimplexLayer,i);
        	//新simplex学習開始

        	for(int i=0;i < m;i++){
				simplex[i][0] = epsilon[0] + Math.random () * 0.4;//重み
				simplex[i][1] = 0.3 + Math.random() * 0.7;//学習割合
        	}

        	//新simplex学習
        	for(int i = 0;i < m;i++){
    			error[i]=newBP(inputSimplexLayer,hiddenSimplexLayer,outputSimplexLayer,i,simplex[i][0],simplex[i][1]);
        	}
        	//テストセットの分類率が高い順にfl,fg,fhを決める
        	//errorの小さい順にfl,fg,fhを決める


        	for(int i = 0;i < m;i++){
        		if (fh < 0 || error[i] > error[fh])
    				fh = i;
    			if (fl < 0 || error[i] < error[fl])
    				fl = i;
        	}
        	for(int i = 0;i < m;i++){
        		if (i != fh && (fg < 0 || error[i] > error[fg]))
        			fg = i;
        	}

        	//新Simplex法による修正
        	while (count < sloop) {
        		//epsilon[fl],epsilon[fg]の中点epsilon[fg]を定めその反射をxrとする
        		//xrが正しいか学習させ、よいならそれをfhと取り換える（epsilon[fl]=xr)
        		//ループ
        		count++;
        		System.out.println(count);
    			// 重心の計算
        		for (int i = 0; i < n; i++) {
        			xg[i] = 0.0;
        		}
    			for (int i = 0; i < m; i++) {
    				if (i != fh) {
    					for(int j = 0;j < n;j++)
    						xg[j] += simplex[i][j];
    				}
    			}

    			for(int i = 0;i < n;i++){
    				xg[i] /= n;
    			}

				// 新最大点の置き換え
    			for(int i = 0;i < n;i++)
    				xr[i] = 2.0 * xg[i] - simplex[fh][i];
    			constrainCheck(xr);//制約条件を満たすかチェック
    			backInit(inputSimplexLayer,hiddenSimplexLayer,outputSimplexLayer,n);
    			yr = newBP(inputSimplexLayer,hiddenSimplexLayer,outputSimplexLayer,n,xr[0],xr[1]);

    			if(yr >= error[fh]){//縮小
    				for(int i = 0;i < n;i++){
    					xr[i] = (1.0 - r1) * simplex[fh][i] + r1 * xr[i];
    				}
    				constrainCheck(xr);
    				backInit(inputSimplexLayer,hiddenSimplexLayer,outputSimplexLayer,n);
        			yr = newBP(inputSimplexLayer,hiddenSimplexLayer,outputSimplexLayer,n,xr[0],xr[1]);
    			}
    			else if(yr < (error[fl]+(r2-1.0)*error[fh])/r2){//拡大
    				for(int i = 0;i < n;i++){
    					xn[i] = r2 * xr[i] - (r2 - 1.0) * simplex[fh][i];
    				}
    				constrainCheck(xn);
    				backInit(inputSimplexLayer,hiddenSimplexLayer,outputSimplexLayer,n);
    				yn = newBP(inputSimplexLayer,hiddenSimplexLayer,outputSimplexLayer,n,xn[0],xn[1]);
    				if(yn <= yr){
    					for(int i = 0;i < n;i++)
    						xr[i] = xn[i];
    					yr = yn;
    				}
    			}
    			for(int i = 0;i < n;i++)
    				simplex[fh][i] = xr[i];
    			error[fh] = yr;

    			//新シンプレックス全体の縮小
    			if(error[fh] >= error[fg]){
    				for(int i = 0;i < m;i++){
    					if(i != fl){
    					for(int j = 0;j < n;j++)
    						simplex[i][j] = 0.5 * (simplex[i][j] + simplex[fl][j]);
    					constrainCheck(simplex[i]);
    					backInit(inputSimplexLayer,hiddenSimplexLayer,outputSimplexLayer,i);
    					error[i] = newBP(inputSimplexLayer,hiddenSimplexLayer,outputSimplexLayer,i,simplex[i][0],simplex[i][1]);
    					}
    				}
    			}
    			//最大値，最小値の計算
    			fh = -1;
    			fg = -1;
    			fl = -1;
    			for (int i = 0; i < m; i++) {
    				if (fh < 0 || error[i] > error[fh])
    					fh = i;
    				if (fl < 0 || error[i] < error[fl])
    					fl = i;
    			}
    			for (int i = 0; i < m; i++) {
    				if (i != fh && (fg < 0 || error[i] > error[fg]))
    					fg = i;
    			}


    			System.out.println("シンプレックス後分類率:"+(1-error[fl]));

    			// 収束判定
    			e = 0.0;
    			for (int i = 0; i < n; i++) {
    				if (i != fl) {
    					yr  = error[i] - error[fl];
    					e  += yr * yr;
    				}
    			}
    			if (e < eps){
    				System.out.println("シンプレックス終了");
    				break;
    			}
        	}
        	//最良の結果を入れる
        	System.out.println("Simplex結果");
        	System.out.println("最終重み修正値："+simplex[fl][0]);
        	System.out.println("最終トレーニングセット使用率："+simplex[fl][1]);
        	epsilon[0] = simplex[fl][0];
        	selectedWeight = simplex[fl][0];
        	selectedTrainNum = (int) (trainingSample.getNumOfSamples() * simplex[fl][1]);

        	//simplex終了後の学習
            for (loopMax = 0; loopMax < Integer.parseInt(numOfMaxLoopTF.getText()); loopMax++) {  //学習上限回数までループ
                if (stopStatus) {
                    break;
                }
                maxError = -1.0;
                sumOfErrors = 0.0;
                yBack = new double[outputLayer.length];
                hBack = new double[numOfHiddenUnits + 1];

                for (int iSample = 0; iSample < selectedTrainNum; iSample++) {    //全サンプル数分のループ

                    error[0] = 0.0;
                    for (int j = 0; j < trainingSample.getNumOfInputs(); j++) {
                        inputLayer[j].forward(trainingSample.getInput()[iSample][j]);
                    }
                    inputLayer[trainingSample.getNumOfInputs()].forward(1.0);

                    for (int j = 0; j < numOfHiddenUnits; j++) {
                        hiddenLayer[j].forward(inputLayer);
                    }
                    hiddenLayer[numOfHiddenUnits].forward(1.0);

                    for (int j = 0; j < trainingSample.getNumOfOutputs(); j++) {
                        outputLayer[j].forward(hiddenLayer);
                        error[0] += Math.pow(trainingSample.getTarget()[iSample][j] - outputLayer[j].getOutput(), 2.0);
                    }

                    error[0] = error[0] / 2;
                    if (error[0] > maxError) {
                        maxError = error[0];
                    }
                    sumOfErrors += error[0];
                    //以下、誤差逆伝播セクション

                    for (int j = 0; j < trainingSample.getNumOfOutputs(); j++) {
                        yBack[j] = (outputLayer[j].getOutput() - trainingSample.getTarget()[iSample][j]) * (1.0 - outputLayer[j].getOutput()) * outputLayer[j].getOutput() + Double.parseDouble(momentum.getText()) * yBack[j];
                    }
                    for (int i = 0; i < numOfHiddenUnits + 1; i++) {
                        netInput = 0.0;
                        for (int j = 0; j < outputLayer.length; j++) {
                            netInput += outputLayer[j].getWeight()[i] * yBack[j];
                        }
                        hBack[i] = netInput * (1.0 - hiddenLayer[i].getOutput()) * hiddenLayer[i].getOutput() + Double.parseDouble(momentum.getText()) * hBack[i];
                    }
                    for (int i = 0; i < trainingSample.getNumOfInputs() + 1; i++) {
                        for (int j = 0; j < numOfHiddenUnits; j++) {
                            if (hiddenLayer[j].isTrainable(i)) {
                                hiddenLayer[j].setWeight(i, hiddenLayer[j].getWeight()[i] - selectedWeight * inputLayer[i].getOutput() * hBack[j]);
                            }
                        }
                    }
                    for (int i = 0; i < numOfHiddenUnits + 1; i++) {
                        for (int j = 0; j < trainingSample.getNumOfOutputs(); j++) {
                            if (outputLayer[j].isTrainable(i)) {
                                outputLayer[j].setWeight(i, outputLayer[j].getWeight()[i] - selectedWeight * hiddenLayer[i].getOutput() * yBack[j]);
                            }
                        }
                    }
                }
                //エラーが閾値以下になったら学習を終了する
                if ((Double.parseDouble(errorThresholdTF.getText()) > maxError)) {
                    break;
                }
    	      }

        }//the end of simplex
        else{
	        for (loopMax = 0; loopMax < Integer.parseInt(numOfMaxLoopTF.getText()); loopMax++) {  //学習上限回数までループ
	            if (stopStatus) {
	                break;
	            }
	            maxError = -1.0;
	            sumOfErrors = 0.0;
	            yBack = new double[outputLayer.length];
	            hBack = new double[numOfHiddenUnits + 1];

	            for (int iSample = 0; iSample < trainingSample.getNumOfSamples(); iSample++) {    //全サンプル数分のループ

	                error[0] = 0.0;
	                for (int j = 0; j < trainingSample.getNumOfInputs(); j++) {
	                    inputLayer[j].forward(trainingSample.getInput()[iSample][j]);
	                }
	                inputLayer[trainingSample.getNumOfInputs()].forward(1.0);

	                for (int j = 0; j < numOfHiddenUnits; j++) {
	                    hiddenLayer[j].forward(inputLayer);
	                }
	                hiddenLayer[numOfHiddenUnits].forward(1.0);

	                for (int j = 0; j < trainingSample.getNumOfOutputs(); j++) {
	                    outputLayer[j].forward(hiddenLayer);
	                    error[0] += Math.pow(trainingSample.getTarget()[iSample][j] - outputLayer[j].getOutput(), 2.0);
	                }

	                error[0] = error[0] / 2;
	                if (error[0] > maxError) {
	                    maxError = error[0];
	                }
	                sumOfErrors += error[0];
	                //以下、誤差逆伝播セクション

	                for (int j = 0; j < trainingSample.getNumOfOutputs(); j++) {
	                    yBack[j] = (outputLayer[j].getOutput() - trainingSample.getTarget()[iSample][j]) * (1.0 - outputLayer[j].getOutput()) * outputLayer[j].getOutput() + Double.parseDouble(momentum.getText()) * yBack[j];
	                }
	                for (int i = 0; i < numOfHiddenUnits + 1; i++) {
	                    netInput = 0.0;
	                    for (int j = 0; j < outputLayer.length; j++) {
	                        netInput += outputLayer[j].getWeight()[i] * yBack[j];
	                    }
	                    hBack[i] = netInput * (1.0 - hiddenLayer[i].getOutput()) * hiddenLayer[i].getOutput() + Double.parseDouble(momentum.getText()) * hBack[i];
	                }
	                for (int i = 0; i < trainingSample.getNumOfInputs() + 1; i++) {
	                    for (int j = 0; j < numOfHiddenUnits; j++) {
	                        if (hiddenLayer[j].isTrainable(i)) {
	                            hiddenLayer[j].setWeight(i, hiddenLayer[j].getWeight()[i] - epsilon[0] * inputLayer[i].getOutput() * hBack[j]);
	                        }
	                    }
	                }
	                for (int i = 0; i < numOfHiddenUnits + 1; i++) {
	                    for (int j = 0; j < trainingSample.getNumOfOutputs(); j++) {
	                        if (outputLayer[j].isTrainable(i)) {
	                            outputLayer[j].setWeight(i, outputLayer[j].getWeight()[i] - epsilon[0] * hiddenLayer[i].getOutput() * yBack[j]);
	                        }
	                    }
	                }
	            }
	            //エラーが閾値以下になったら学習を終了する
	            if ((Double.parseDouble(errorThresholdTF.getText()) > maxError)) {
	                break;
	            }
		    }
        }

        double averageError = sumOfErrors / trainingSample.getNumOfSamples();
        if(!pruningflg){
        	statusView.append("### 学習完了###\n");
        	statusView.append("\t学習回数 : " + String.valueOf(loopMax) + "\n");
        	statusView.append("\t最大誤差 : " + String.valueOf(maxError) + "\n");
        	statusView.append("\t平均誤差 : " + String.valueOf(averageError) + "\n");
            statusView.update(statusView.getGraphics());
        }
        else{
        	System.out.println("\t### 学習完了###");
        	System.out.println("\t学習回数 : " + String.valueOf(loopMax));
        	System.out.println("\t最大誤差 : " + String.valueOf(maxError));
        	System.out.println("\t平均誤差 : " + String.valueOf(averageError));        	
        }
    }

    /**
     *制約条件を満たすかチェック
     */
    static public void constrainCheck(double check[]) {
    	if(check[0]<0.001)
    		check[0] = 0.001;
    	else if(check[0] > 1)
    		check[0] = 0.9 + Math.random() * 0.1;
    	if(check[1] < 0.08)
    		check[1] = 0.08;
    	else if (check[1] > 1.0)
    		check[1] = 0.95 + 0.05 * Math.random();
	}

	//simplex用バックプロパゲーション
    static public double BP(AN[][] inputBPLayer, AN[][] hiddenBPLayer,
			AN[][] outputBPLayer, int number, double eps) {
    	final int numOfHiddenUnits = Integer.parseInt(numOfHiddenUnitsTF.getText());
    	int loopMax = 0;
    	double sumOfErrors = 0.0,error;
    	double yBack[], hBack[], netInput;
        for (loopMax = 0; loopMax < Integer.parseInt(numOfMaxLoopTF.getText()); loopMax++) {  //学習上限回数までループ
            if (stopStatus) {
                break;
            }
            maxError = -1.0;
            sumOfErrors = 0.0;
            yBack = new double[outputLayer.length];
            hBack = new double[numOfHiddenUnits + 1];

            for (int iSample = 0; iSample < trainingSample.getNumOfSamples(); iSample++) {    //全サンプル数分のループ
            	error = 0.0;

                for (int j = 0; j < trainingSample.getNumOfInputs(); j++) {
                    inputBPLayer[number][j].forward(trainingSample.getInput()[iSample][j]);
                }
                inputBPLayer[number][trainingSample.getNumOfInputs()].forward(1.0);

                for (int j = 0; j < numOfHiddenUnits; j++) {
                    hiddenBPLayer[number][j].forward(inputBPLayer,number);
                }
                hiddenBPLayer[number][numOfHiddenUnits].forward(1.0);

                for (int j = 0; j < trainingSample.getNumOfOutputs(); j++) {
                    outputBPLayer[number][j].forward(hiddenBPLayer,number);
                    error += Math.pow(trainingSample.getTarget()[iSample][j] - outputBPLayer[number][j].getOutput(), 2.0);
                }

                /*for (int j = 0; j < numOfHiddenUnits; j++) {
                    hiddenLayer[j].forward(inputLayer);
                }
                hiddenLayer[numOfHiddenUnits].forward(1.0);

                for (int j = 0; j < trainingSample.getNumOfOutputs(); j++) {
                    outputLayer[j].forward(hiddenLayer);
                    error[0] += Math.pow(trainingSample.getTarget()[iSample][j] - outputLayer[j].getOutput(), 2.0);
                }*/


                error = error / 2;
                if (error > maxError) {
                    maxError = error;
                }
                sumOfErrors += error;
                //以下、誤差逆伝播セクション
                for (int j = 0; j < trainingSample.getNumOfOutputs(); j++) {
                    yBack[j] = (outputBPLayer[number][j].getOutput() - trainingSample.getTarget()[iSample][j]) * (1.0 - outputBPLayer[number][j].getOutput()) * outputBPLayer[number][j].getOutput() + Double.parseDouble(momentum.getText()) * yBack[j];
                }
                for (int i = 0; i < numOfHiddenUnits + 1; i++) {
                    netInput = 0.0;
                    for (int j = 0; j < outputLayer.length; j++) {
                        netInput += outputBPLayer[number][j].getWeight()[i] * yBack[j];
                    }
                    hBack[i] = netInput * (1.0 - hiddenBPLayer[number][i].getOutput()) * hiddenBPLayer[number][i].getOutput() + Double.parseDouble(momentum.getText()) * hBack[i];
                }
                for (int i = 0; i < trainingSample.getNumOfInputs() + 1; i++) {
                    for (int j = 0; j < numOfHiddenUnits; j++) {
                        if (hiddenBPLayer[number][j].isTrainable(i)) {
                            hiddenBPLayer[number][j].setWeight(i, hiddenBPLayer[number][j].getWeight()[i] - eps * inputBPLayer[number][i].getOutput() * hBack[j]);
                        }
                    }
                }
                for (int i = 0; i < numOfHiddenUnits + 1; i++) {
                    for (int j = 0; j < trainingSample.getNumOfOutputs(); j++) {
                        if (outputBPLayer[number][j].isTrainable(i)) {
                            outputBPLayer[number][j].setWeight(i, outputBPLayer[number][j].getWeight()[i] - eps * hiddenLayer[i].getOutput() * yBack[j]);
                        }
                    }
                }
            }
            //エラーが閾値以下になったら学習を終了する
            if ((Double.parseDouble(errorThresholdTF.getText()) > maxError)) {
                break;
            }


        }
        double averageError = sumOfErrors / trainingSample.getNumOfSamples();
		return averageError;
	}

    //新simplex用バックプロパゲーション
    static public double newBP(AN[][] inputBPLayer, AN[][] hiddenBPLayer,AN[][] outputBPLayer, int number, double eps,double per) {
    	final int numOfHiddenUnits = Integer.parseInt(numOfHiddenUnitsTF.getText());
    	double valrate = 0.25;
    	int trainNum = (int) ((1-valrate)*trainingSample.getNumOfSamples() * per);
    	int loopMax = 0;
    	double sumOfErrors = 0.0,error;
    	double yBack[], hBack[], netInput;
    	//System.out.println("重み修正値："+eps);
    	//System.out.println("トレーニング使用率："+per*100);
        for (loopMax = 0; loopMax < Integer.parseInt(numOfMaxLoopTF.getText()); loopMax++) {  //学習上限回数までループ
            if (stopStatus) {
                break;
            }
            maxError = -1.0;
            sumOfErrors = 0.0;
            yBack = new double[outputLayer.length];
            hBack = new double[numOfHiddenUnits + 1];
            for (int iSample = 0; iSample < trainNum; iSample++) {    //全サンプル数分のループ
            	error = 0.0;
                for (int j = 0; j < trainingSample.getNumOfInputs(); j++) {
                    inputBPLayer[number][j].forward(trainingSample.getInput()[iSample][j]);
                }
                inputBPLayer[number][trainingSample.getNumOfInputs()].forward(1.0);

                for (int j = 0; j < numOfHiddenUnits; j++) {
                    hiddenBPLayer[number][j].forward(inputBPLayer,number);
                }
                hiddenBPLayer[number][numOfHiddenUnits].forward(1.0);

                for (int j = 0; j < trainingSample.getNumOfOutputs(); j++) {
                    outputBPLayer[number][j].forward(hiddenBPLayer,number);
                    error += Math.pow(trainingSample.getTarget()[iSample][j] - outputBPLayer[number][j].getOutput(), 2.0);
                }

                error = error / 2;

                sumOfErrors += error;
                //以下、誤差逆伝播セクション
                for (int j = 0; j < trainingSample.getNumOfOutputs(); j++) {
                    yBack[j] = (outputBPLayer[number][j].getOutput() - trainingSample.getTarget()[iSample][j]) * (1.0 - outputBPLayer[number][j].getOutput()) * outputBPLayer[number][j].getOutput() + Double.parseDouble(momentum.getText()) * yBack[j];
                }
                for (int i = 0; i < numOfHiddenUnits + 1; i++) {
                    netInput = 0.0;
                    for (int j = 0; j < outputLayer.length; j++) {
                        netInput += outputBPLayer[number][j].getWeight()[i] * yBack[j];
                    }
                    hBack[i] = netInput * (1.0 - hiddenBPLayer[number][i].getOutput()) * hiddenBPLayer[number][i].getOutput() + Double.parseDouble(momentum.getText()) * hBack[i];
                }
                for (int i = 0; i < trainingSample.getNumOfInputs() + 1; i++) {
                    for (int j = 0; j < numOfHiddenUnits; j++) {
                        if (hiddenBPLayer[number][j].isTrainable(i)) {
                            hiddenBPLayer[number][j].setWeight(i, hiddenBPLayer[number][j].getWeight()[i] - eps * inputBPLayer[number][i].getOutput() * hBack[j]);
                        }
                    }
                }
                for (int i = 0; i < numOfHiddenUnits + 1; i++) {
                    for (int j = 0; j < trainingSample.getNumOfOutputs(); j++) {
                        if (outputBPLayer[number][j].isTrainable(i)) {
                            outputBPLayer[number][j].setWeight(i, outputBPLayer[number][j].getWeight()[i] - eps * hiddenBPLayer[number][i].getOutput() * yBack[j]);
                        }
                    }
                }
            }
            //エラーが閾値以下になったら学習を終了する
            /*if ((Double.parseDouble(errorThresholdTF.getText()) > maxError)) {
                break;
            }*/


        }
        double averageError = sumOfErrors / trainNum;
        System.out.println(averageError);
        //トレーニングサンプル認識率確認開始

        double ZER = Double.parseDouble(zerTF.getText());
        boolean flag = true;

        numOfAccordance = 0;
        for (int i = 0; i < trainNum; i++) {
            for (int j = 0; j < trainingSample.getNumOfInputs(); j++) {
                inputBPLayer[number][j].forward(trainingSample.getInput()[i][j]);
            }
            for (int j = 0; j < hiddenLayer.length - 1; j++) {
                hiddenBPLayer[number][j].forward(inputBPLayer,number);
            }
            for (int j = 0; j < outputLayer.length; j++) {
                outputBPLayer[number][j].forward(hiddenBPLayer,number);

            }

            for (int j = 0; j < outputLayer.length; j++) {
            	//System.out.println("予測output:"+outputBPLayer[number][j].getOutput()+"\ttarget:"+testSample.getTarget()[i][j]);
            	/*System.out.print("予測output:"+outputBPLayer[number][j].getOutput()+"\t");
            	if(j == 1)
            		System.out.println();
            	*/
            	if (outputBPLayer[number][j].getOutput() > 1.0 - ZER && outputBPLayer[number][j].getOutput() < 1.0 + ZER) {
                    if ((int) trainingSample.getTarget()[i][j] != 1) {
                        flag = false;
                    }
                } else if (outputBPLayer[number][j].getOutput() < ZER && outputBPLayer[number][j].getOutput() > -ZER) {
                    if ((int) trainingSample.getTarget()[i][j] != 0) {
                        flag = false;
                    }
                } else {
                    flag = false;
                }

            }
            if (flag) {
                numOfAccordance++;
            }
            flag = true;
        }
        rate = (double) numOfAccordance / (double) trainNum;
        //System.out.print("トレーニング認識率 : " + String.valueOf(rate * 100.0) + " %\t");

        //テストサンプル認識率確認開始
        flag = true;
        numOfAccordance = 0;
        for (int i = 0; i < testSample.getNumOfSamples(); i++) {
            for (int j = 0; j < testSample.getNumOfInputs(); j++) {
                inputBPLayer[number][j].forward(testSample.getInput()[i][j]);
            }
            for (int j = 0; j < hiddenLayer.length - 1; j++) {
                hiddenBPLayer[number][j].forward(inputBPLayer,number);
            }
            for (int j = 0; j < outputLayer.length; j++) {
                outputBPLayer[number][j].forward(hiddenBPLayer,number);

            }

            for (int j = 0; j < outputLayer.length; j++) {
            	//System.out.println("予測output:"+outputBPLayer[number][j].getOutput()+"\ttarget:"+testSample.getTarget()[i][j]);
                if (outputBPLayer[number][j].getOutput() > 1.0 - ZER && outputBPLayer[number][j].getOutput() < 1.0 + ZER) {
                    if ((int) testSample.getTarget()[i][j] != 1) {
                        flag = false;
                    }
                } else if (outputBPLayer[number][j].getOutput() < ZER && outputBPLayer[number][j].getOutput() > -ZER) {
                    if ((int) testSample.getTarget()[i][j] != 0) {
                        flag = false;
                    }
                } else {
                    flag = false;
                }

            }
            if (flag) {
                numOfAccordance++;
            }
            flag = true;
        }
        rate = (double) numOfAccordance / (double) testSample.getNumOfSamples();
        //System.out.print("テスト認識率 : " + String.valueOf(((double) numOfAccordance / (double) testSample.getNumOfSamples()) * 100.0) + " %\t");


        //validation認識率確認
        numOfAccordance = 0;
        flag = true;
        for (int i = (int)((1-valrate)*trainingSample.getNumOfSamples()); i < trainingSample.getNumOfSamples(); i++) {
            for (int j = 0; j < trainingSample.getNumOfInputs(); j++) {
                inputBPLayer[number][j].forward(trainingSample.getInput()[i][j]);
            }
            for (int j = 0; j < hiddenLayer.length - 1; j++) {
                hiddenBPLayer[number][j].forward(inputBPLayer,number);
            }
            for (int j = 0; j < outputLayer.length; j++) {
                outputBPLayer[number][j].forward(hiddenBPLayer,number);

            }

            for (int j = 0; j < outputLayer.length; j++) {
                if (outputBPLayer[number][j].getOutput() > 1.0 - ZER && outputBPLayer[number][j].getOutput() < 1.0 + ZER) {
                    if ((int) trainingSample.getTarget()[i][j] != 1) {
                        flag = false;
                    }
                } else if (outputBPLayer[number][j].getOutput() < ZER && outputBPLayer[number][j].getOutput() > -ZER) {
                    if ((int) trainingSample.getTarget()[i][j] != 0) {
                        flag = false;
                    }
                } else {
                    flag = false;
                }

            }
            if (flag) {
                numOfAccordance++;
            }
            flag = true;
        }
        rate = (double) numOfAccordance / (int) (valrate*trainingSample.getNumOfSamples());



        //statusView.append("認識率 : " + String.valueOf(rate * 100.0) + " %\n");
        //System.out.println("バリデーション認識率 : " + String.valueOf(rate * 100.0) + " %");
        //テストサンプル認識率確認終了
		return (1-rate);
	}

	static public void saveWeight() {
        tempTrainableItoH = new boolean[trainingSample.getNumOfInputs() + 1][Integer.parseInt(numOfHiddenUnitsTF.getText())];
        tempTrainableHtoO = new boolean[Integer.parseInt(numOfHiddenUnitsTF.getText()) + 1][trainingSample.getNumOfOutputs()];
        tempWeightItoH = new double[trainingSample.getNumOfInputs() + 1][Integer.parseInt(numOfHiddenUnitsTF.getText())];
        tempWeightHtoO = new double[Integer.parseInt(numOfHiddenUnitsTF.getText()) + 1][trainingSample.getNumOfOutputs()];

        for (int i = 0; i < trainingSample.getNumOfInputs() + 1; i++) {
            for (int j = 0; j < Integer.parseInt(numOfHiddenUnitsTF.getText()); j++) {
                tempTrainableItoH[i][j] = hiddenLayer[j].isTrainable(i);
                tempWeightItoH[i][j] = hiddenLayer[j].getWeight()[i];
            }
        }

        for (int i = 0; i < Integer.parseInt(numOfHiddenUnitsTF.getText()) + 1; i++) {
            for (int j = 0; j < trainingSample.getNumOfOutputs(); j++) {
                tempTrainableHtoO[i][j] = outputLayer[j].isTrainable(i);
                tempWeightHtoO[i][j] = outputLayer[j].getWeight()[i];
            }
        }
    }

    static public void initNetworkTable() {
        networkTable = new boolean[allSample.getNumOfInputs()][Integer.parseInt(numOfHiddenUnitsTF.getText())];
        for (int i = 0; i < allSample.getNumOfInputs(); i++) {
            for (int h = 0; h < Integer.parseInt(numOfHiddenUnitsTF.getText()); h++) {
                networkTable[i][h] = true;
            }
        }
    }

    static public void openAllData() {
        JFileChooser fc = new JFileChooser(".");
        int res = fc.showOpenDialog(null);
        fc.getSelectedFile();
        if (res == JFileChooser.APPROVE_OPTION) {
            File file = fc.getSelectedFile();
            System.out.println("Loading test file has been completed. (filename : " + file.getName() + ")");
            statusView.append("全サンプル読み込み完了（ファイル名 : " + file.getName() + "）\n");
            allDataTF.setText(file.getName());
            if (doNormalizationCB.isSelected()) {
                allSample.normalize();
                statusView.append("正規化を行いました.\n");
            }
        } else if (res == JFileChooser.CANCEL_OPTION) {
            System.out.println("Loading all sample file has been canceled.");
        }
        DivisionAllData();
    }

    //AllDataファイルを受け取り自動的にトレーニング、テストデータに分割
    //作成したデータセットをトレーニング、テストデータとして読み込ませる
    static public void DivisionAllData(){
    	
    }
    
    

    static public void recallNetwork() {
        for (int i = 0; i < trainingSample.getNumOfInputs() + 1; i++) {
            for (int j = 0; j < Integer.parseInt(numOfHiddenUnitsTF.getText()); j++) {
                hiddenLayer[j].setTrainable(i, tempTrainableItoH[i][j]);
                hiddenLayer[j].setWeight(i, tempWeightItoH[i][j]);

            }
        }

        for (int i = 0; i < Integer.parseInt(numOfHiddenUnitsTF.getText()) + 1; i++) {
            for (int j = 0; j < trainingSample.getNumOfOutputs(); j++) {
                outputLayer[j].setTrainable(i, tempTrainableHtoO[i][j]);
                outputLayer[j].setWeight(i, tempWeightHtoO[i][j]);
            }
        }
    }

   static public void recallBeforePruning() {
        for (int i = 0; i < trainingSample.getNumOfInputs() + 1; i++) {
            for (int j = 0; j < Integer.parseInt(numOfHiddenUnitsTF.getText()); j++) {
                hiddenLayer[j].setTrainable(i, beforePruningTrainableItoH[i][j]);
                hiddenLayer[j].setWeight(i, beforePruningWeightItoH[i][j]);

            }
        }

        for (int i = 0; i < Integer.parseInt(numOfHiddenUnitsTF.getText()) + 1; i++) {
            for (int j = 0; j < trainingSample.getNumOfOutputs(); j++) {
                outputLayer[j].setTrainable(i, beforePruningTrainableHtoO[i][j]);
                outputLayer[j].setWeight(i, beforePruningWeightHtoO[i][j]);
            }
        }
    }

    static public void pruning() {
        saveBeforePruning();
        stopStatus = false;
        numOfCut = 0;
        boolean EF = false;
        boolean CheckPruned = false;
        boolean notPruned1 = false;
        boolean notPruned2 = false;
        double EP = Double.parseDouble(endPruningTF.getText());
        double MaxPruning = Double.parseDouble(MaxPruningTF.getText());
        statusView.append("プルーニング終了レート： " + EP + "\n");
        while(!EF){//枝刈り終了エラーレートを上回るまで実行する
        	while (!stopStatus) {
        		saveWeight();
        		CheckPruned = pruningMain();
        		learning();
        		checkRate();
        		
        		if(rate < EP){//枝刈りの終了判定
        			EF = true;
        			break;
        		}
        		
        		if(!CheckPruned){
        			if(!notPruned1){//1回目の枝刈り失敗
        				notPruned1 = true;
        			}
        			else{//2回目の枝刈り失敗
        				notPruned2 = true;
        			}
        		}
        		
        		if(notPruned2){//枝刈りが終了しない場合の処理
        			recallBeforePruning();
        			EP = ChangePruningRate(EP);
        			notPruned1 = false;
        			notPruned2 = false;
        			break;
        		}
        	}
        }
        numOfCut--;
    	recallNetwork();
    	System.out.println("### プルーニング終了 ###");
    	System.out.println("枝刈りされた接続 : " + numOfCut);
    	statusView.append("### プルーニング終了 ###\n");
    	statusView.append("枝刈りされた接続 : " + numOfCut + "\n");
    	statusView.update(statusView.getGraphics());
    }
    
    //枝刈りが終わらない場合、枝刈り終了エラーレートを変えて再度枝刈りを行う
    static public double ChangePruningRate(double EP){
    	double AEP = Double.parseDouble(AddEndPruningTF.getText());
    	EP += AEP;
    	numOfCut = 0;
    	System.out.println("プルーニング終了レートを "+EP+" に変更してやり直します");
		statusView.append("プルーニング終了レートを "+EP+" に変更してやり直します\n");
		statusView.update(statusView.getGraphics());
		jScrollPane1.update(jScrollPane1.getGraphics());
		
		return(EP);
    }
    
    static public boolean pruningMain() {
        double EATA2 = Double.parseDouble(pruningThresholdTF.getText());
        double maxW1W2 = 0.0;
        double minW1W2 = 0.0;
        double w1w2;
        int i1 = 0;
        int h1 = 0;
        boolean notPruned = true; 



        for (int i = 0; i < inputLayer.length; i++) {
            for (int j = 0; j < hiddenLayer.length - 1; j++) {
                maxW1W2 = 0.0;
                for (int k = 0; k < outputLayer.length; k++) {
                    if (hiddenLayer[j].isTrainable(i) && outputLayer[k].isTrainable(j)) {
                        w1w2 = Math.abs(hiddenLayer[j].getWeight()[i] * outputLayer[k].getWeight()[j]);
                        if (minW1W2 == 0.0) {
                            minW1W2 = w1w2;
                            i1 = i;
                            h1 = j;
                        }
                        if (maxW1W2 < w1w2) {
                            maxW1W2 = w1w2;
                        }
                    }

                }
                if (maxW1W2 != 0.0 && maxW1W2 <= 4 * EATA2) {
                    System.out.println("条件1\ti = " + i + ", h = " + j);
                    //statusView.append("条件1\ti = " + String.valueOf(i) + ", h = " + String.valueOf(j) + "\n");
                    hiddenLayer[j].delWeight(i);
                    numOfCut++;
                    notPruned = false;
                }
                if (maxW1W2 != 0.0 && minW1W2 > maxW1W2) {
                    minW1W2 = maxW1W2;
                    i1 = i;
                    h1 = j;
                }
            }
        }

        for (int i = 0; i < hiddenLayer.length; i++) {
            for (int j = 0; j < outputLayer.length; j++) {
                if (outputLayer[j].isTrainable(i)) {
                    if (Math.abs(outputLayer[j].getWeight()[i]) <= 4 * EATA2) {
                        System.out.println("条件2\th = " + i + ", o = " + j);
                        //statusView.append("条件2\th = " + i + ", o = " + j + "\n");
                        outputLayer[j].delWeight(i);
                        numOfCut++;
                        notPruned = false;
                    }
                }
            }
        }

        if (notPruned) {
            if (hiddenLayer[h1].isTrainable(i1)) {
                System.out.println("条件3\ti = " + i1 + ", h = " + h1);
                //statusView.append("条件3\ti = " + i1 + ", h = " + h1 + "\n");
                hiddenLayer[h1].delWeight(i1);
                numOfCut++;
            } 
            else {
                System.out.println("Not pruned");
                return(false);//枝刈り失敗
            }
        }
        return(true);//枝刈り成功
    }

    static public void checkRate() {
        double ZER = Double.parseDouble(zerTF.getText());
        boolean flag = true;
        numOfAccordance = 0;
        for (int i = 0; i < trainingSample.getNumOfSamples(); i++) {
            for (int j = 0; j < trainingSample.getNumOfInputs(); j++) {
                inputLayer[j].forward(trainingSample.getInput()[i][j]);
            }
            for (int j = 0; j < hiddenLayer.length - 1; j++) {
                hiddenLayer[j].forward(inputLayer);
            }
            for (int j = 0; j < outputLayer.length; j++) {
                outputLayer[j].forward(hiddenLayer);

            }

            for (int j = 0; j < outputLayer.length; j++) {
                if (outputLayer[j].getOutput() > 1.0 - ZER && outputLayer[j].getOutput() < 1.0 + ZER) {

                    if ((int) trainingSample.getTarget()[i][j] != 1) {
                        flag = false;
                    }
                } else if (outputLayer[j].getOutput() < ZER && outputLayer[j].getOutput() > -ZER) {

                    if ((int) trainingSample.getTarget()[i][j] != 0) {
                        flag = false;
                    }
                } else {

                    flag = false;
                }

            }
            if (flag) {
                numOfAccordance++;
            }

            flag = true;
        }
        rate = (double) numOfAccordance / (double) trainingSample.getNumOfSamples();

    }

    static public void recogAllData() {
        double ZER = Double.parseDouble(zerTF.getText());
        boolean flag = true;
        numOfAccordance = 0;
        for (int i = 0; i < allSample.getNumOfSamples(); i++) {
            for (int j = 0; j < allSample.getNumOfInputs(); j++) {
                inputLayer[j].forward(allSample.getInput()[i][j]);
            }
            for (int j = 0; j < hiddenLayer.length - 1; j++) {
                hiddenLayer[j].forward(inputLayer);
            }
            for (int j = 0; j < outputLayer.length; j++) {
                outputLayer[j].forward(hiddenLayer);

            }
            statusView.append("第" + (i + 1) + "サンプルの目標出力 : ");
            for (int j = 0; j < allSample.getNumOfOutputs(); j++) {
                statusView.append(String.valueOf((int) (allSample.getTarget()[i][j])) + "\t");
            }
            statusView.append("ネットワークの出力 : ");
            for (int j = 0; j < outputLayer.length; j++) {
                if (outputLayer[j].getOutput() > 1.0 - ZER && outputLayer[j].getOutput() < 1.0 + ZER) {
                    statusView.append("1\t");
                    if ((int) allSample.getTarget()[i][j] != 1) {
                        flag = false;
                    }
                } else if (outputLayer[j].getOutput() < ZER && outputLayer[j].getOutput() > -ZER) {
                    statusView.append("0\t");
                    if ((int) allSample.getTarget()[i][j] != 0) {
                        flag = false;
                    }
                } else {
                    statusView.append("*\t");
                    flag = false;
                }
            }
            if (flag) {
                numOfAccordance++;

            }
            statusView.append("\n");
            flag = true;
        }
        rate = (double) numOfAccordance / (double) allSample.getNumOfSamples();
        statusView.append("認識率 : " + String.valueOf(rate * 100.0) + " %\n");
        statusView.append("正しく認識されたサンプル数 : " + numOfAccordance + "\n");
    }

    static public void saveBeforePruning() {
        beforePruningTrainableItoH = new boolean[trainingSample.getNumOfInputs() + 1][Integer.parseInt(numOfHiddenUnitsTF.getText())];
        beforePruningTrainableHtoO = new boolean[Integer.parseInt(numOfHiddenUnitsTF.getText()) + 1][trainingSample.getNumOfOutputs()];
        beforePruningWeightItoH = new double[trainingSample.getNumOfInputs() + 1][Integer.parseInt(numOfHiddenUnitsTF.getText())];
        beforePruningWeightHtoO = new double[Integer.parseInt(numOfHiddenUnitsTF.getText()) + 1][trainingSample.getNumOfOutputs()];

        for (int i = 0; i < trainingSample.getNumOfInputs() + 1; i++) {
            for (int j = 0; j < Integer.parseInt(numOfHiddenUnitsTF.getText()); j++) {
                beforePruningTrainableItoH[i][j] = hiddenLayer[j].isTrainable(i);
                beforePruningWeightItoH[i][j] = hiddenLayer[j].getWeight()[i];
            }
        }

        for (int i = 0; i < Integer.parseInt(numOfHiddenUnitsTF.getText()) + 1; i++) {
            for (int j = 0; j < trainingSample.getNumOfOutputs(); j++) {
                beforePruningTrainableHtoO[i][j] = outputLayer[j].isTrainable(i);
                beforePruningWeightHtoO[i][j] = outputLayer[j].getWeight()[i];
            }
        }

    }
    
    static public void openTrainingSample() {
        JFileChooser fc = new JFileChooser(".");
        int res = fc.showOpenDialog(null);
        if (res == JFileChooser.APPROVE_OPTION) {
            File file = fc.getSelectedFile();
            TrainingData = file;
            trainingSample = new FileOperationForNN(file);
            testSample = new FileOperationForNN(file);
            System.out.println("Loading training file has been completed. (filename : " + file.getName() + ")");
            statusView.append("トレーニングサンプル読み込み完了（ファイル名 : " + file.getName() + "）\n");
            viewFileName1.setText(file.getName());
            if (doNormalizationCB.isSelected()) {
                trainingSample.normalize();
                testSample.normalize();
                statusView.append("正規化を行いました.\n");
            }
            int numOfHiddenUnits = Integer.parseInt(numOfHiddenUnitsTF.getText());
            //入力層のユニット数は閾値処理のため１つ増やす
            inputLayer = new AN[trainingSample.getNumOfInputs() + 1];
            //同様に中間層ユニット数もひとつ増やす
            hiddenLayer = new AN[Integer.parseInt(numOfHiddenUnitsTF.getText()) + 1];
            //出力層のユニット数
            outputLayer = new AN[trainingSample.getNumOfOutputs()];
            //各層の初期化
            for (int i = 0; i < trainingSample.getNumOfInputs(); i++) {
                inputLayer[i] = new AN(trainingSample.getNumOfInputs());
            }
            inputLayer[trainingSample.getNumOfInputs()] = new AN(1.0);

            for (int i = 0; i < numOfHiddenUnits; i++) {
                hiddenLayer[i] = new AN(inputLayer.length);
            }
            hiddenLayer[numOfHiddenUnits] = new AN(1.0);

            for (int i = 0; i < trainingSample.getNumOfOutputs(); i++) {
                outputLayer[i] = new AN(hiddenLayer.length);
            }

        } else if (res == JFileChooser.CANCEL_OPTION) {
            System.out.println("Loading training file has been canceled.");
        }
    }

    static public void openTestSample() {
        JFileChooser fc = new JFileChooser(".");
        int res = fc.showOpenDialog(null);
        fc.getSelectedFile();
        if (res == JFileChooser.APPROVE_OPTION) {
            File file = fc.getSelectedFile();
            TestData = file;
            //testSample2 = new FileOperationForNN(file);
            System.out.println("Loading test file has been completed. (filename : " + file.getName() + ")");
            statusView.append("テストサンプル読み込み完了（ファイル名 : " + file.getName() + "）\n");
            viewFileName2.setText(file.getName());
            if (doNormalizationCB.isSelected()) {
                testSample2.normalize();
                statusView.append("正規化を行いました.\n");
            }
        } else if (res == JFileChooser.CANCEL_OPTION) {
            System.out.println("Loading test file has been canceled.");
        }
    }
    
    //**全データファイルを開いたときの処理**
    //**現在未実装**
    private void openAllDataMIActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_openTrainingSampleMIActionPerformed
    	openAllData();
    }//GEN-LAST:event_openTrainingSampleMIActionPerformed    
    
    private void openTrainingSampleMIActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_openTrainingSampleMIActionPerformed
    	openTrainingSample();
    }//GEN-LAST:event_openTrainingSampleMIActionPerformed

    private void openTestSampleMIActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_openTestSampleMIActionPerformed
    	openTestSample();
    }//GEN-LAST:event_openTestSampleMIActionPerformed

    private void terminateProgramMIActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_terminateProgramMIActionPerformed
    	System.out.println("GoodBye!");
    	System.exit(0);
    }//GEN-LAST:event_terminateProgramMIActionPerformed

    /*
     * 全自動用ボタン制御
     */
    private void jFullAutoButtonActionPerformed(java.awt.event.ActionEvent evt) {
    	ProjectName = ProjectNameTF.getText();
    	// 実験用プロジェクトフォルダ作成
    	SelectedLearningDataSet.MakeDirectory();
    	//　プロジェクトフォルダ直下に3段目までの階層別フォルダを作成
	
    	//**4段目以降を実装時には作成フォルダを追加する必要有り**
	
    	for(int i=1;i<=3;i++){
    		SelectedLearningDataSet.MakeFileFolder(RuleName[i-1]);
    	}
    	// トレーニングデータとテストデータを実験用フォルダにコピー
    	FileCopy();
    	if(TrainingData != null && TestData != null){
    		statusView.append("※プルーニングにしばらく時間がかかります\n");
    		statusView.update(statusView.getGraphics());
    		FullAutoAction();
    	}
    }

    private void FileCopy(){
    	String originfile, copyfile;
    	//トレーニングデータのコピー
    	if (TrainingData != null){
    		originfile = TrainingData.getPath();
    		copyfile = "c:\\FullAutoReRX\\" + ProjectName + "\\" + RuleName[RoopNum-1] + "\\" + LearningDataName + ".txt";
    		FileCopy(originfile, copyfile);		
    	}else{
    		statusView.append("### トレーニングデータセットを選択してください ###\n");
    	}
    	//テストデータのコピー
    	//3段目までの全てのフォルダにテストデータをコピーする
    	//**4段目以降を実装時には作成フォルダを追加する必要有り**
    	if (TestData != null){
    		originfile = TestData.getPath();
    		for(int i=1;i<=3;i++){
    			copyfile = "c:\\FullAutoReRX\\" + ProjectName + "\\" + RuleName[i-1] + "\\"+ ProjectName +".test.txt";
    			FileCopy(originfile, copyfile);
    		}
    	}else{
    		statusView.append("### テストデータセットを選択してください ###\n");
    	}
    	statusView.update(statusView.getGraphics());
    }

    private void FileCopy(String origin, String filename){
    	try {
    		//Fileオブジェクトを生成する
    		FileInputStream fis = new FileInputStream(origin);
    		FileOutputStream fos = new FileOutputStream(filename);
    		//入力ファイルをそのまま出力ファイルに書き出す
    		byte buf[] = new byte[256];
    		int len;
    		while ((len = fis.read(buf)) != -1) {
    			fos.write(buf, 0, len);
    		}
    		//終了処理
    		fos.flush();
    		fos.close();
    		fis.close();
    		System.out.println("コピーが完了しました。");
    	} catch (IOException ex) {
    		//例外時処理
    		System.out.println("コピーに失敗しました。");
    		ex.printStackTrace();
    	}
    }

    private void FullAutoAction(){
    	
    	//過学習防止用LD'の作成
    	String LDPath = "c:\\FullAutoReRX\\"+Main.ProjectName+"\\" + Main.RuleName[Main.RoopNum-1] + "\\"+Main.LearningDataName+".txt";
    	Double parsent = (double)(Double.parseDouble(LDTF.getText())/(double)100);
    	File LDDASH = SelectedLearningDataSet.OriginalFile(LDPath);
    	SelectedLearningDataSet.PreventOverFitting(LDDASH,parsent,LearningDataName);
    	File TrainingFile = new File("c:\\FullAutoReRX\\" + ProjectName + "\\" + RuleName[RoopNum-1] + "\\"+ LearningDataName+"'.txt");
    	openTrainingSample2(TrainingFile);
    	// 学習
    	FirstStartTime = System.currentTimeMillis();
    	learning();
    	// プルーニング
    	pruningflg = true;
    	pruning();
    	pruningflg = false;
    	// テスト
    	startRecog(0);
    	try{
    		J48Test.J_48();
    	}catch(Exception e){
    		System.out.println("Error: Exceptionです．");
    	}
    	// ルール細分化(再起)と評価とLDf作成
    	AutoException.Exception();
    	
    	FirstEndTime = System.currentTimeMillis();
    	// 二段目へ
    	Secondary();
    }

    private void Secondary(){
    	SecondStartTime = System.currentTimeMillis();
    	ReName(); //RuleNameを更新
    	//一段目のフォルダにあるLDfを二段目へコピーする
    	String LDf = "c:\\FullAutoReRX\\"+Main.ProjectName+"\\" + Main.RuleName[Main.RoopNum-2] + "\\"+Main.LearningDataName+".txt";
    	String LDf2 = "c:\\FullAutoReRX\\"+Main.ProjectName+"\\" + Main.RuleName[Main.RoopNum-1] + "\\"+Main.LearningDataName+".txt";
    	FileCopy(LDf,LDf2);
    	//二段目のLDfからLDf'を作成する
    	Double parsent = (double)(Double.parseDouble(LDfTF.getText())/(double)100);
    	File LDfDASH = SelectedLearningDataSet.OriginalFile(LDf2);
    	SelectedLearningDataSet.PreventOverFitting(LDfDASH,parsent,LearningDataName);
    	
    	File TrainingFile = new File("c:\\FullAutoReRX\\" + ProjectName + "\\" + RuleName[RoopNum-1] + "\\"+ LearningDataName+"'.txt");
    	openTrainingSample2(TrainingFile);
    	learning();
    	pruningflg = true;
    	pruning();
    	pruningflg = false;
    	startRecog(0); //テストデータ認識開始
    	try{
    		J48Test.J_48();
    	}catch(Exception e){
    		System.out.println("Error: Exceptionです．");
    	}
    	// ルール細分化と評価とルール統合とLDff作成
    	AutoException.Exception();
	
    	SecondEndTime = System.currentTimeMillis();
	
    	// 三段目へ
    	Thirdly();
    }
    private void Thirdly(){
    	ThirdStartTime = System.currentTimeMillis();
    	ReName();
    	//2段目にあるLDｆｆを3段目にコピーする
    	String LDff = "c:\\FullAutoReRX\\"+Main.ProjectName+"\\" + Main.RuleName[Main.RoopNum-2] + "\\"+Main.LearningDataName+".txt";
    	String LDff2 = "c:\\FullAutoReRX\\"+Main.ProjectName+"\\" + Main.RuleName[Main.RoopNum-1] + "\\"+Main.LearningDataName+".txt";
    	FileCopy(LDff,LDff2);
    	
    	//3段目のLDffからLDff'を作成する
    	Double parsent = (double)(Double.parseDouble(LDffTF.getText())/(double)100);
    	File LDffDASH = SelectedLearningDataSet.OriginalFile(LDff2);
    	SelectedLearningDataSet.PreventOverFitting(LDffDASH,parsent,LearningDataName);
    	
    	File TrainingFile = new File("c:\\FullAutoReRX\\" + ProjectName + "\\" + RuleName[RoopNum-1] + "\\"+ LearningDataName+"'.txt");
    	openTrainingSample2(TrainingFile);
    	learning();
    	pruningflg = true;
    	pruning();
    	pruningflg = false;
    	startRecog(0); //テストデータ認識開始
    	try{
    		J48Test.J_48();
    	}catch(Exception e){
    		System.out.println("Error: Exceptionです．");
    	}
    	// ルール細分化と評価とルール統合とLDff作成
    	AutoException.Exception();
	
    	ThirdEndTime = System.currentTimeMillis();
    	 IndicationTime();
    }
    
    //計算時間の表示　処理が完了した段までの計算時間を表示する
    //**4段目以降実装時には要変更**
    static public void IndicationTime(){
    	if(RoopNum == 1){//LDfが存在しない場合　1段目で処理が終了
    		statusView2.append("1段目の計算時間"+((FirstEndTime-FirstStartTime)/(double)1000)+"(s)\n");
    	}
    	if(RoopNum == 2){//LDffが存在しない場合　2段目で処理が終了
    		statusView2.append("1段目の計算時間"+((FirstEndTime-FirstStartTime)/(double)1000)+"(s)\n");
    		statusView2.append("2段目の計算時間"+((SecondEndTime-SecondStartTime)/(double)1000)+"(s)\n");
    		statusView2.append("全体の計算時間"+((SecondEndTime-FirstStartTime)/(double)1000)+"(s)\n");
    	}
    	if(RoopNum == 3){//3段目までの処理が全て終了した場合
    		statusView2.append("1段目の計算時間"+((FirstEndTime-FirstStartTime)/(double)1000)+"(s)\n");
    		statusView2.append("2段目の計算時間"+((SecondEndTime-SecondStartTime)/(double)1000)+"(s)\n");
    		statusView2.append("3段目の計算時間"+((ThirdEndTime-ThirdStartTime)/(double)1000)+"(s)\n\n");
    		statusView2.append("全体の計算時間"+((ThirdEndTime-FirstStartTime)/(double)1000)+"(s)\n");
    	}
    }
    
    
    // 各グローバス変数を更新する
    private void ReName(){
    	RoopNum++;
    	LearningDataNameSet();
    }

    /**
    * @param args the command line arguments
    */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Main().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    static public javax.swing.JTextField allDataTF;
    static public javax.swing.JButton clearBT;
    static public javax.swing.JButton crossValidationBt;
    static public javax.swing.JCheckBox doNormalizationCB;
    static public javax.swing.JCheckBox doEpsilonSimplexCB;
    static public javax.swing.JTextField endPruningTF;
    static public javax.swing.JTextField epsilonTF;
    static public javax.swing.JTextField errorThresholdTF;
    static public javax.swing.JButton jButton1;
    static public javax.swing.JLabel jLabel1;
    static public javax.swing.JLabel jLabel10;
    static public javax.swing.JLabel jLabel11;
    static public javax.swing.JLabel jLabel12;
    static public javax.swing.JLabel jLabel13;
    static public javax.swing.JLabel jLabel2;
    static public javax.swing.JLabel jLabel3;
    static public javax.swing.JLabel jLabel4;
    static public javax.swing.JLabel jLabel5;
    static public javax.swing.JLabel jLabel6;
    static public javax.swing.JLabel jLabel7;
    static public javax.swing.JLabel jLabel8;
    static public javax.swing.JLabel jLabel9;
    static public javax.swing.JLabel jLabel14;
    static public javax.swing.JLabel jLabel15;
    static public javax.swing.JLabel jLabel16;
    static public javax.swing.JLabel jLabel17;
    static public javax.swing.JLabel jLabel18;
    static public javax.swing.JLabel jLabel19;
    static public javax.swing.JLabel jLabel20;
    static public javax.swing.JLabel jLabel21;
    static public javax.swing.JLabel jLabel22;
    static public javax.swing.JLabel jLabel23;
    static public javax.swing.JLabel jLabel24;
    static public javax.swing.JLabel jLabel25;
    static public javax.swing.JLabel jLabel26;
    static public javax.swing.JLabel jLabel27;
    static public javax.swing.JLabel jLabel28;
    static public javax.swing.JMenu jMenu1;
    static public javax.swing.JMenu jMenu2;
    static public javax.swing.JMenuBar jMenuBar1;
    static public javax.swing.JMenuItem jMenuItem1;
    static public javax.swing.JScrollPane jScrollPane1;
    static public javax.swing.JScrollPane jScrollPane2;
    static public javax.swing.JTextField momentum;
    static public javax.swing.JTextField numOfHiddenUnitsTF;
    static public javax.swing.JTextField numOfMaxLoopTF;
    static public javax.swing.JTextField numOfMaxSimplexLoopTF;
    static public javax.swing.JTextField AddEndPruningTF;
    static public javax.swing.JTextField MaxPruningTF;
    static public javax.swing.JTextField ProjectNameTF;
    static public javax.swing.JTextField δ1TF;
    static public javax.swing.JTextField δ2TF;
    static public javax.swing.JTextField Sδ1TF;
    static public javax.swing.JTextField Sδ2TF;
    static public javax.swing.JTextField Tδ1TF;
    static public javax.swing.JTextField Tδ2TF;
    static public javax.swing.JMenuItem openAllDataMI;
    static public javax.swing.JMenuItem openTestSampleMI;
    static public javax.swing.JMenuItem openTrainingSampleMI;
    static public javax.swing.JButton pruningBt;
    static public javax.swing.JTextField pruningThresholdTF;
    static public javax.swing.JButton recallBeforePruningBt;
    static public javax.swing.JButton recogAllDataBt;
    static public javax.swing.JButton startLearning;
    static public javax.swing.JButton startRecog;
    static public javax.swing.JTextArea statusView;
    static public javax.swing.JTextArea statusView2;   
    static public javax.swing.JButton stopProcedure;
    static public javax.swing.JMenuItem terminateProgramMI;
    static public javax.swing.JTextField viewCValidationFileName;
    static public javax.swing.JTextField viewFileName1;
    static public javax.swing.JTextField viewFileName2;
    static public javax.swing.JTextField zerTF;
    static public javax.swing.JTextField LDTF;
    static public javax.swing.JTextField LDfTF;
    static public javax.swing.JTextField LDffTF;
    static public javax.swing.JButton jFullAutoButton;
    static public double FirstStartTime,FirstEndTime;
    static public double SecondStartTime,SecondEndTime;
    static public double ThirdStartTime,ThirdEndTime;
    // End of variables declaration//GEN-END:variables



    class Prune extends SwingWorker<Object, Object> {

        @Override
        protected Object doInBackground() throws Exception {
            pruning();
            return null;
        }

        @Override
        protected void done() {
            pruningBt.setEnabled(true);
            pruningBt.setText("プルーニング");
            stopProcedure.setEnabled(false);
        }

    }

    static class Learn extends SwingWorker<Object, Object> {

        @Override
        protected Object doInBackground() throws Exception {

            learning();
            return null;
        }

        @Override
        protected void done() {
            stopStatus = false;
            stopProcedure.setEnabled(false);
            startLearning.setEnabled(true);
            startLearning.setText("学習開始");
        }




    }

    class CrossValidate extends SwingWorker<Object, Object> {

        @Override
        protected Object doInBackground() throws Exception {
            crossValidate();
            return null;
        }

        @Override
        protected void done() {
            stopStatus = false;
            stopProcedure.setEnabled(false);
            crossValidationBt.setEnabled(true);
            crossValidationBt.setText("相互検証");
        }

    }
    
    //初期設定以外のトレーニングサンプルの読み込み
    //細分化及び二段目以降に使用
    static public void openTrainingSample2(File file) {
            trainingSample = new FileOperationForNN(file);
            testSample = new FileOperationForNN(file);
            System.out.println("Loading training file has been completed. (filename : " + file.getName() + ")");
            System.out.println("トレーニングサンプル読み込み完了（ファイル名 : " + file.getName() + "）\n");
            viewFileName1.setText(file.getName());
            
            if (doNormalizationCB.isSelected()) {//正規化の有無
                trainingSample.normalize();
                testSample.normalize();
                System.out.println("正規化を行いました.\n");
            }
           
            int numOfHiddenUnits = Integer.parseInt(numOfHiddenUnitsTF.getText());;
            //入力層のユニット数は閾値処理のため１つ増やす
            inputLayer = new AN[trainingSample.getNumOfInputs() + 1];
            //同様に中間層ユニット数もひとつ増やす
            hiddenLayer =new AN[(numOfHiddenUnits + 1)];
            //出力層のユニット数
            outputLayer = new AN[trainingSample.getNumOfOutputs()];
            //各層の初期化
            for (int i = 0; i < trainingSample.getNumOfInputs(); i++) {
                inputLayer[i] = new AN(trainingSample.getNumOfInputs());
            }
            inputLayer[trainingSample.getNumOfInputs()] = new AN(1.0);

            for (int i = 0; i < numOfHiddenUnits; i++) {
                hiddenLayer[i] = new AN(inputLayer.length);
            }
            hiddenLayer[numOfHiddenUnits] = new AN(1.0);

            for (int i = 0; i < trainingSample.getNumOfOutputs(); i++) {
                outputLayer[i] = new AN(hiddenLayer.length);
            }
        } 
   
   //テストデータ読み込み以降の処理
   
   static public void startRecog(int Ri) {
       writeOutNameFile(Ri);
       String cls = null;
       Alist = new int[testSample.getNumOfInputs()];//使用属性をまとめたリスト
       Arrays.fill(Alist,0);//初期値は0に設定
       
       list = new ArrayList[testSample.getNumOfSamples()];
       for (int i = 0; i < testSample.getNumOfSamples(); i++) {
           list[i] = new ArrayList<Object>();
       }

       double ZER = Double.parseDouble(zerTF.getText());
       boolean flag = true;
       boolean ExsistD = false; //離散値があるかどうかの判別用フラグ
       numOfAccordance = 0;
       for(int j = 0; j < testSample.getNumOfInputs(); j++){
    	   if(testSample.getWritable(j)){//書き込むデータに離散値があればフラグを立てる
    		   if(testSample.getLabel(j).codePointAt(0) == 'D'){
    			   ExsistD = true; //書き込むデータに離散値があればフラグを立てる
    		   }
    	   }
       }
       for (int j = 0; j < testSample.getNumOfInputs(); j++) {
    	   if(ExsistD == true){//書き込むデータに離散値がある場合、連続値は書き込まない
    		   if(testSample.getLabel(j).codePointAt(0) == 'C'){
    			   testSample.setWritable(j,false);
    		   }
    		   else{//連続値でない使用する属性＝離散値属性
    			   if(testSample.getWritable(j)){
    				   Alist[j] = 1;//属性を使用した印として値を１に変える 
    			   }
    		   }
    	   }
    	   else{//離散値がなければ残った属性を全て使用する
    		   if(testSample.getWritable(j)){
    			   Alist[j] = 1;//属性を使用した印として値を１に変える
    		   }
    	   }
       }
       for (int i = 0; i < testSample.getNumOfSamples(); i++) {
           for (int j = 0; j < testSample.getNumOfInputs(); j++) {
               inputLayer[j].forward(testSample.getInput()[i][j]);
           }
           for (int j = 0; j < hiddenLayer.length - 1; j++) {
               hiddenLayer[j].forward(inputLayer);
           }
           for (int j = 0; j < outputLayer.length; j++) {
               outputLayer[j].forward(hiddenLayer);

           }
           //System.out.println("第" + (i + 1) + "サンプルの目標出力 : ");
           for (int j = 0; j < testSample.getNumOfOutputs(); j++) {
            //  System.out.println(String.valueOf((int) (testSample.getTarget()[i][j])) + "\t");
           }
           //System.out.println("ネットワークの出力 : ");
           for (int j = 0; j < outputLayer.length; j++) {
               if (outputLayer[j].getOutput() > 1.0 - ZER && outputLayer[j].getOutput() < 1.0 + ZER) {
            	  // System.out.println("1\t");
                   if ((int) testSample.getTarget()[i][j] != 1) {
                       flag = false;
                   }
               } else if (outputLayer[j].getOutput() < ZER && outputLayer[j].getOutput() > -ZER) {
            	  // System.out.println("0\t");
                   if ((int) testSample.getTarget()[i][j] != 0) {
                       flag = false;
                   }
               } else {
            	  // System.out.println("*\t");
                   flag = false;
               }

           }
           if (flag) {
               // 正しく認識できたデータを配列に格納
               for (int j = 0; j < testSample.getNumOfInputs(); j++) {
            	   if (testSample.getWritable(j)) {
            		   list[i].add(testSample.getInput(i, j));
                   }
               }
               if(((int) testSample.getTarget()[i][0] == 0) && ((int) testSample.getTarget()[i][1] == 1)) {
                   list[i].add("Class 1");
               } else if(((int) testSample.getTarget()[i][0] == 1) && ((int) testSample.getTarget()[i][1] == 0)) {
                   list[i].add("Class 2");
               }
               numOfAccordance++;
           }
          // System.out.println("");
           flag = true;
       }
       rate = (double) numOfAccordance / (double) testSample.getNumOfSamples();
       statusView.append("認識率 : " + String.valueOf(rate * 100.0) + "%\n");
       //writeOutNameFile();

       /* dataファイルの作成 */


       writeOutArffFile(list,Ri,Alist);
       writeOutDataFile(list,Ri);
       writeOutWeightFile(Ri);

   }

   static public void writeOutNameFile(int Ri) {
      // boolean flag = false; // 入力が省けるかどうかのフラグの作成
	   
       try {
    	   if(Ri == 0){
    		   FileOutputStream nameFileStream = new FileOutputStream("c:\\FullAutoReRX\\" + ProjectName + "\\" + RuleName[Main.RoopNum-1] + "\\" + RuleName[RoopNum-1] +".names");   	
               OutputStreamWriter nameFileOSW = new OutputStreamWriter(nameFileStream);
               BufferedWriter nameFileBW = new BufferedWriter(nameFileOSW);

               nameFileBW.write("Class 1, Class 2.");
               nameFileBW.newLine();
               nameFileBW.newLine();

               for (int i = 0; i < inputLayer.length - 1; i++) {
                   for (int j = 0; j < hiddenLayer.length - 1; j++) {
                       if (hiddenLayer[j].isTrainable(i)) {
                           testSample.setWritable(i, true);
                       }
                   }
                   if (testSample.getWritable(i)) {
                       nameFileBW.write(testSample.getLabel(i) + ": " + testSample.getType(i) + ".");
                       nameFileBW.newLine();
                   }
               }

               nameFileBW.close();
    	   }
    	   else{
    		   FileOutputStream nameFileStream = new FileOutputStream("c:\\FullAutoReRX\\" + ProjectName + "\\" + RuleName[Main.RoopNum-1] + "\\" + RuleName[RoopNum-1] +Ri+".names");   	
               OutputStreamWriter nameFileOSW = new OutputStreamWriter(nameFileStream);
               BufferedWriter nameFileBW = new BufferedWriter(nameFileOSW);

               nameFileBW.write("Class 1, Class 2.");
               nameFileBW.newLine();
               nameFileBW.newLine();

               for (int i = 0; i < inputLayer.length - 1; i++) {
                   for (int j = 0; j < hiddenLayer.length - 1; j++) {
                       if (hiddenLayer[j].isTrainable(i)) {
                           testSample.setWritable(i, true);
                       }
                   }
                   if (testSample.getWritable(i)) {
                       nameFileBW.write(testSample.getLabel(i) + ": " + testSample.getType(i) + ".");
                       nameFileBW.newLine();
                   }
               }

               nameFileBW.close();
    	   }
       } catch (IOException e) {
           System.err.println(e);
       }


   }

   
   static public void writeOutArffFile(ArrayList<Object>[] list,int Ri,int alist[]){
       FileOutputStream arffFileStream = null;

       try {
           if(Ri == 0){
        	   arffFileStream = new FileOutputStream("c:\\FullAutoReRX\\" + ProjectName + "\\" + RuleName[Main.RoopNum-1] + "\\" + RuleName[RoopNum-1] +".arff");
               OutputStreamWriter arffFileOSW = new OutputStreamWriter(arffFileStream);
               BufferedWriter arffFileBW = new BufferedWriter(arffFileOSW);

               //データの種類を書き出す
               arffFileBW.write("@relation " + ProjectName);
               arffFileBW.newLine();
               arffFileBW.newLine();

               //属性を書き出す
               //@attribute D7 real  D7: discrete 2.
               //System.out.println("hiddenLayer: " + hiddenLayer.length );
               for (int i = 0; i < inputLayer.length - 1; i++) {
                   for (int j = 0; j < hiddenLayer.length - 1; j++) {
                       if (hiddenLayer[j].isTrainable(i)) {
                           testSample.setWritable(i, true);
                           //System.out.println("i: " + i +", trainable: " + hiddenLayer[j].isTrainable(i));
                       }
                       else if(!hiddenLayer[j].isTrainable(i)){
                           testSample.setWritable(i, false);
                          //System.out.println("i: " + i +", trainable: " + hiddenLayer[j].isTrainable(i));
                       }
                   }
               }
               for (int j = 0; j < testSample.getNumOfInputs(); j++) {
            	   if (alist[j] == 1) {
            		   arffFileBW.write("@attribute " + testSample.getLabel(j) + " real");
            		   arffFileBW.newLine();
            	   }
               }
         
               //クラスを書く
               arffFileBW.write("@attribute Play {yes, no}");
               arffFileBW.newLine();
               arffFileBW.newLine();

               arffFileBW.write("@data");
               arffFileBW.newLine();

               for (int i = 0; i < list.length; i++) {
                   for (int j = 0; j < list[i].size(); j++) {
                       if (String.valueOf(list[i].get(j)).equals("0.0")) {
                           arffFileBW.write(String.valueOf("0, "));
                       } else if (String.valueOf(list[i].get(j)).equals("1.0")) {
                           arffFileBW.write(String.valueOf("1, "));
                       } else if (String.valueOf(list[i].get(j)).equals("Class 1")){
                           arffFileBW.write("yes");
                       }else if (String.valueOf(list[i].get(j)).equals("Class 2")){
                           arffFileBW.write("no");
                       }else {
                           arffFileBW.write(String.valueOf(list[i].get(j)) + ", ");
                       }

                   }


//                   if (String.valueOf(list[i].get(list[i].size() - 1)).equals("0.0")) {
//                       dataFileBW.write(String.valueOf("0"));
//                   } else if (String.valueOf(list[i].get(list[i].size() - 1)).equals("1.0")) {
//                       dataFileBW.write(String.valueOf("1"));
//                   } else {
//                       dataFileBW.write(String.valueOf(list[i].get(list[i].size() - 1)));
//                   }
                   arffFileBW.newLine();
               }

               arffFileBW.close();
           }
           else{
        	   arffFileStream = new FileOutputStream("c:\\FullAutoReRX\\" + ProjectName + "\\" + RuleName[Main.RoopNum-1] + "\\" + RuleName[RoopNum-1] +Ri+".arff");
               OutputStreamWriter arffFileOSW = new OutputStreamWriter(arffFileStream);
               BufferedWriter arffFileBW = new BufferedWriter(arffFileOSW);

               //データの種類を書き出す
               arffFileBW.write("@relation " + ProjectName);
               arffFileBW.newLine();
               arffFileBW.newLine();

               //属性を書き出す
               //@attribute D7 real  D7: discrete 2.
               //System.out.println("hiddenLayer: " + hiddenLayer.length );
               for (int i = 0; i < inputLayer.length - 1; i++) {
                   for (int j = 0; j < hiddenLayer.length - 1; j++) {
                       if (hiddenLayer[j].isTrainable(i)) {
                           testSample.setWritable(i, true);
                           //System.out.println("i: " + i +", trainable: " + hiddenLayer[j].isTrainable(i));
                       }
                       else if(!hiddenLayer[j].isTrainable(i)){
                           testSample.setWritable(i, false);
                          //System.out.println("i: " + i +", trainable: " + hiddenLayer[j].isTrainable(i));
                       }
                   }
               }
               for (int j = 0; j < testSample.getNumOfInputs(); j++) {
            	   if (alist[j] == 1) {
            		   arffFileBW.write("@attribute " + testSample.getLabel(j) + " real");
            		   arffFileBW.newLine();
            	   }
               }
         
               //クラスを書く
               arffFileBW.write("@attribute Play {yes, no}");
               arffFileBW.newLine();
               arffFileBW.newLine();

               arffFileBW.write("@data");
               arffFileBW.newLine();

               for (int i = 0; i < list.length; i++) {
                   for (int j = 0; j < list[i].size(); j++) {
                       if (String.valueOf(list[i].get(j)).equals("0.0")) {
                           arffFileBW.write(String.valueOf("0, "));
                       } else if (String.valueOf(list[i].get(j)).equals("1.0")) {
                           arffFileBW.write(String.valueOf("1, "));
                       } else if (String.valueOf(list[i].get(j)).equals("Class 1")){
                           arffFileBW.write("yes");
                       }else if (String.valueOf(list[i].get(j)).equals("Class 2")){
                           arffFileBW.write("no");
                       }else {
                           arffFileBW.write(String.valueOf(list[i].get(j)) + ", ");
                       }

                   }


//                   if (String.valueOf(list[i].get(list[i].size() - 1)).equals("0.0")) {
//                       dataFileBW.write(String.valueOf("0"));
//                   } else if (String.valueOf(list[i].get(list[i].size() - 1)).equals("1.0")) {
//                       dataFileBW.write(String.valueOf("1"));
//                   } else {
//                       dataFileBW.write(String.valueOf(list[i].get(list[i].size() - 1)));
//                   }
                   arffFileBW.newLine();
               }

               arffFileBW.close();
           }
       } catch (IOException e) {
           System.err.println(e);
       } finally {
           try {
               arffFileStream.close();
           } catch (IOException ex) {
               Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
           }
       }

   }
   
   static public void writeOutWeightFile(int Ri){

      // boolean flag = false; // 入力が省けるかどうかのフラグの作成

       try {
    	   if(Ri == 0){
    		   FileOutputStream weightFileStream = new FileOutputStream("c:\\FullAutoReRX\\"+ProjectName+"\\" + RuleName[Main.RoopNum-1] + "\\"+RuleName[RoopNum-1]+".txt");
               OutputStreamWriter weightFileOSW = new OutputStreamWriter(weightFileStream);
               BufferedWriter weightFileBW = new BufferedWriter(weightFileOSW);

               for (int i = 0; i < inputLayer.length - 1; i++) {
                   for (int j = 0; j < hiddenLayer.length - 1; j++) {
                       if (hiddenLayer[j].isTrainable(i)) {
                           testSample.setWritable(i, true);
                       }
                   }
                   if (testSample.getWritable(i)) {
                       weightFileBW.write(testSample.getLabel(i) + ": " + Double.toString(inputLayer[i].getWeight()[i]));
                       weightFileBW.newLine();
                   }
               }

               weightFileBW.close();
    	   }
    	   else{
    		   FileOutputStream weightFileStream = new FileOutputStream("c:\\FullAutoReRX\\"+ProjectName+"\\" + RuleName[Main.RoopNum-1] + "\\"+RuleName[RoopNum-1]+Ri+".txt");
               OutputStreamWriter weightFileOSW = new OutputStreamWriter(weightFileStream);
               BufferedWriter weightFileBW = new BufferedWriter(weightFileOSW);

               for (int i = 0; i < inputLayer.length - 1; i++) {
                   for (int j = 0; j < hiddenLayer.length - 1; j++) {
                       if (hiddenLayer[j].isTrainable(i)) {
                           testSample.setWritable(i, true);
                       }
                   }
                   if (testSample.getWritable(i)) {
                       weightFileBW.write(testSample.getLabel(i) + ": " + Double.toString(inputLayer[i].getWeight()[i]));
                       weightFileBW.newLine();
                   }
               }

               weightFileBW.close();
    	   }
       } catch (IOException e) {
           System.err.println(e);
       }


   }

   static public void writeOutDataFile(ArrayList<Object>[] list,int Ri) {
       FileOutputStream dataFileStream = null;
       try {
    	   if(Ri == 0){
    		   dataFileStream = new FileOutputStream("c:\\FullAutoReRX\\" + ProjectName + "\\" + RuleName[RoopNum-1] + "\\" + RuleName[RoopNum-1] +".data");
               OutputStreamWriter dataFileOSW = new OutputStreamWriter(dataFileStream);
               BufferedWriter dataFileBW = new BufferedWriter(dataFileOSW);

               for (int i = 0; i < list.length; i++) {
                   for (int j = 0; j < list[i].size(); j++) {
                       if (String.valueOf(list[i].get(j)).equals("0.0")) {
                           dataFileBW.write(String.valueOf("0, "));
                       } else if (String.valueOf(list[i].get(j)).equals("1.0")) {
                           dataFileBW.write(String.valueOf("1, "));
                       }  else if (String.valueOf(list[i].get(j)).equals("Class 1")){
                           dataFileBW.write("Class 1.");
                       }   else if (String.valueOf(list[i].get(j)).equals("Class 2")){
                           dataFileBW.write("Class 2.");
                       }
                       else {
                           dataFileBW.write(String.valueOf(list[i].get(j) + ", "));
                       }

                   }


//                   if (String.valueOf(list[i].get(list[i].size() - 1)).equals("0.0")) {
//                       dataFileBW.write(String.valueOf("0"));
//                   } else if (String.valueOf(list[i].get(list[i].size() - 1)).equals("1.0")) {
//                       dataFileBW.write(String.valueOf("1"));
//                   } else {
//                       dataFileBW.write(String.valueOf(list[i].get(list[i].size() - 1)));
//                   }
                   dataFileBW.newLine();
               }

               dataFileBW.close();
    	   }
    	   else{
    		   dataFileStream = new FileOutputStream("c:\\FullAutoReRX\\" + ProjectName + "\\" + RuleName[Main.RoopNum-1] + "\\" + RuleName[RoopNum-1] +Ri+".data");
               OutputStreamWriter dataFileOSW = new OutputStreamWriter(dataFileStream);
               BufferedWriter dataFileBW = new BufferedWriter(dataFileOSW);

               for (int i = 0; i < list.length; i++) {
                   for (int j = 0; j < list[i].size(); j++) {
                       if (String.valueOf(list[i].get(j)).equals("0.0")) {
                           dataFileBW.write(String.valueOf("0, "));
                       } else if (String.valueOf(list[i].get(j)).equals("1.0")) {
                           dataFileBW.write(String.valueOf("1, "));
                       }  else if (String.valueOf(list[i].get(j)).equals("Class 1")){
                           dataFileBW.write("Class 1.");
                       }   else if (String.valueOf(list[i].get(j)).equals("Class 2")){
                           dataFileBW.write("Class 2.");
                       }
                       else {
                           dataFileBW.write(String.valueOf(list[i].get(j) + ", "));
                       }

                   }


//                   if (String.valueOf(list[i].get(list[i].size() - 1)).equals("0.0")) {
//                       dataFileBW.write(String.valueOf("0"));
//                   } else if (String.valueOf(list[i].get(list[i].size() - 1)).equals("1.0")) {
//                       dataFileBW.write(String.valueOf("1"));
//                   } else {
//                       dataFileBW.write(String.valueOf(list[i].get(list[i].size() - 1)));
//                   }
                   dataFileBW.newLine();
               }

               dataFileBW.close();
    	   }
       } catch (IOException e) {
           System.err.println(e);
       } finally {
           try {
               dataFileStream.close();
           } catch (IOException ex) {
               Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
           }
       }

   }

   
   
}

