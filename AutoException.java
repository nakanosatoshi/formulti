package fullauto;

import javax.swing.*;

import java.text.DecimalFormat;
import java.util.Arrays;
import java.io.*;
import java.awt.BorderLayout;
import java.awt.event.*;
import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StreamTokenizer;


public class AutoException {

	JLabel label,label2;
	static String DataFilePath;//データファイルのパスを保存しておく
	static String RuleFilePath;//ルールファイル(初期)のパス
	static String TestFilePath;//テストデータのファイルパス
	static String FirstDataFilePath; //初期(一段目で使用する)トレーニングデータのファイルパス　二段目以降精度比較に使用
	static String FinalRuleFilePath;//細分化を行い最終的にできたルールファイルのパス
	//小数10桁表示
	static DecimalFormat df = new DecimalFormat("#.##########");

	static int zokusei; // 全属性数　クラスは含まない
	static int FirstRuleNum; // 最初のルールの細分化が全て終了したら更新される
	static int BeforeRuleNum; // 細分化前のルールの数を記憶する
	static int RuleNum; // ルールが追加されるたびに更新される
	static int DataNum; // トレーニングデータ数
	static int TestDataNum; // テストデータ数
	static int FirstDataNum; // 全トレーニングデータ(LD)数
	static int SubdivisionCount = 0; //細分化を行った回数を記憶する。初回と二回目以降の判別用
	static boolean SubdivisionCheck = false; //細分化が行われたかどうかのチェック。
	  
	//二次元配列にルールを格納していく.RuleArray[ルール番号][属性]=値になっている.各ルールで使用しない属性は値2を入れて比較時に無視できるようにする.
	//二次元配列にデータを入れていく. DataArray[][0]には0を初期値とし、ルールに沿っていれば1に書き換え、0のままならばnewfileに書き込んでいく.
	
	 static BigDecimal[][] DataArray; //ルール適合フラグ用の箱とクラス用の箱を用意
	 static BigDecimal[][] RuleArray; //ルール番号・属性番号を合わせるため1つずらし、また、クラス用の箱も用意
	 static BigDecimal[][] BeforeRuleArray; //細分化二回目以降で比較する前のデータを記憶した配列
	 static BigDecimal[][] SavingRuleArray; //ルールの書き換え時に使う作業用配列
	 static BigDecimal[][] FirstDataArray; //二段目以降のトレーニングデータとの精度比較用
	 //ルールに沿ったデータの数をカウントする
	 static int TrueNum = 0;
	 //再帰するデータセット作成用カウント変数
	 static int DataCount = 1;
	 //連続値の場合、＞≧＝≦＜の5パターン考えられる.それぞれ順に、1,2,3,4,5を値に加算し、判別できるようにする.
	 static int[][] CDistinction;
	 static int[][] SavingCDistinction; //CDistinctionを再生成するときの退避用配列
	 
     //配列に各ルールが離散値か連続値かの情報を残す.出力時に離散値の小数点を破棄するため.
	 //離散なら0,連続なら1を入れる.
	 static int[] DCArray;	//属性番号を合わせるため1つずらして使用する.
	 
	 static BigDecimal[] RuleAccuracyArray;//各ルールの精度
	 static BigDecimal[] SupportRateArray;//各ルールのサポート率
	 static BigDecimal[] ErrorRateArray;//各ルールのエラー率
	 static int[] CorrectDataNumArray;//各ルールに沿ったデータ数
	 static int[] ErrorDataNumArray;//各ルールに沿わない（クラスだけ異なる）データ数
	 
	 static BigDecimal[][] RecursiveDataArray; //δ超えの再帰用データ配列
	 static BigDecimal[][] TestDataArray; //テストデータの配列
	 static BigDecimal δ1;
	 static BigDecimal δ2;
	
	 static int ResultCount = 1;
	 static int CountTrueTestDataNum = 0;
	 static int CountTrueFirstDataNum = 0;
	 
	public static void Exception(){
		File TrainingDataFile = LoadDataFile();
		if(TrainingDataFile != null){
			InsertDataNum(TrainingDataFile);
		}
		File RuleFile = LoadRuleFile();
		if(RuleFile != null){
			FirstRuleNum = InsertRuleNum(RuleFile);
			RuleNum = FirstRuleNum;
		}
		File TestFile = LoadTestFile();
		if(TestFile != null){
			InsertTestDataNum(TestFile);
		}
		File FirstTrainingDataFile = LoadFirstDataFile();
		if(FirstTrainingDataFile != null){
			InsertFirstDataNum(FirstTrainingDataFile);
		}
		ScanDelta();
		Start();
	}
	
	public static void ScanDelta(){
		BigDecimal str1=BigDecimal.valueOf(0);
		BigDecimal str2=BigDecimal.valueOf(0);
		if(Main.RoopNum == 1){
			str1 = new BigDecimal(Main.δ1TF.getText());
			str2 = new BigDecimal(Main.δ2TF.getText());
		}
		else if(Main.RoopNum == 2){
			str1 = new BigDecimal(Main.Sδ1TF.getText());
			str2 = new BigDecimal(Main.Sδ2TF.getText());
		}
		else if(Main.RoopNum == 3){
			str1 = new BigDecimal(Main.Tδ1TF.getText());
			str2 = new BigDecimal(Main.Tδ2TF.getText());
		}
		δ1 = str1;
		δ2 = str2;
		Main.statusView.append("\tδ1 = "+δ1+"\n");
		Main.statusView.append("\tδ2 = "+δ2+"\n");
		Main.statusView.update(Main.statusView.getGraphics());
		SubdivisionCount = 0; // 細分化回数リセット
	}
	
	//データファイルを受け取りファイルパスを記憶する
	private static File LoadDataFile(){
		File DataFile = new File("c:\\FullAutoReRX\\" + Main.ProjectName + "\\" + Main.RuleName[Main.RoopNum-1] + "\\" + Main.LearningDataName + ".txt");
		if(!DataFile.exists())
			Main.statusView.append("###### "+DataFile.getName()+"が存在しません\n");
		else{
			DataFilePath = DataFile.getAbsolutePath();
			//Main.statusView.append(DataFile.getName()+"を読み込みました\n");
		}
		return(DataFile);
	}
	
	//ルールファイルを受け取りそのパスを記憶する
	private static File LoadRuleFile(){
		File RuleFile = new File("c:\\FullAutoReRX\\"+Main.ProjectName+"\\" + Main.RuleName[Main.RoopNum-1] + "\\"+Main.RuleName[Main.RoopNum-1]+".txt");
		RuleFilePath = RuleFile.getAbsolutePath();
		//Main.statusView.append(RuleFile.getName()+"を読み込みました\n");
		return(RuleFile);
	}
	
	//テストファイルを受け取りそのパスを記憶する
	private static File LoadTestFile(){
		File TestFile = new File("c:\\FullAutoReRX\\" + Main.ProjectName + "\\" + Main.RuleName[Main.RoopNum-1] + "\\"+ Main.ProjectName +".test.txt");
		TestFilePath = TestFile.getAbsolutePath();
		//Main.statusView.append(TestFile.getName()+"を読み込みました\n");
		return(TestFile);
	}
	
	//全トレーニングデータのパスを記憶する
	//常に1段目フォルダの”LD"ファイルを指定
	private static File LoadFirstDataFile(){
		File FirstDataFile = new File("c:\\FullAutoReRX\\" + Main.ProjectName + "\\" + Main.RuleName[0] + "\\" +  "LD.txt");
		if(!FirstDataFile.exists())
			Main.statusView.append("###### "+FirstDataFile.getName()+"が存在しません\n");
		else{
			FirstDataFilePath = FirstDataFile.getAbsolutePath();
			//Main.statusView.append(FirstDataFile.getName()+"を読み込みました\n");
		}
		return(FirstDataFile);
	}
	
	//データファイルを引数にとってそのデータ数を読み取る　データ数はDataNumに保存
	private static void InsertDataNum(File DataFile){
		try{
			FileReader fr = new FileReader(DataFile);
			StreamTokenizer token = new StreamTokenizer(fr);
			token.eolIsSignificant(true);
			token.nextToken();
			DataNum = (int)token.nval;
			token.nextToken(); //改行文字
			token.nextToken();
			zokusei = (int)token.nval;
			Main.statusView.append("■DataNum = "+DataNum+"\n");
			Main.statusView.append("■zokusei = "+zokusei+"\n");
		}catch(FileNotFoundException e){
			Main.statusView.append("### AutoException.InsertDataNum(File DataFile)内でError: FileNotFoundExceptionです．\n");
		}catch(IOException e){
			Main.statusView.append("### Error: IOExceptionです．\n");
		}
	}
	
	//ルールファイルを引数にとってルール数を読み取る　戻り値としてルール数をとる　int型
	private static int InsertRuleNum(File RuleFile){
		try{
			FileReader fr = new FileReader(RuleFile);
			StreamTokenizer token = new StreamTokenizer(fr);
			token.eolIsSignificant(true);
			boolean eof = false;
			boolean eol = false;
			int counteol = 0; //改行数＝ルール数
			int tmp;
			tmp = token.nextToken();
			if(tmp == StreamTokenizer.TT_EOF){//ルール無し(白紙のテキスト)用処理
				eol = true;
				eof = true;
			}
			while(!eof){
				eol = false;
				while(!eol){
					tmp = token.nextToken();
					switch(tmp){
					case StreamTokenizer.TT_EOL:
						counteol++;
						eol = true;
						break;	
					case StreamTokenizer.TT_EOF:
						counteol++;
						eol = true;
						eof = true;
						break;
					default: break;
					}
				}
			}
			//counteolはルール数より一つ多く読み込まれるため一つ減らす(wekaで作成されたルールファイルは最終行に改行が入り、tokenがEOLになるため）
			//空のファイル(counteolの値が0のときは0のままにする)
			Main.statusView.append("■"+RuleFile.getName()+"'s RuleNum = "+counteol+"\n");
			if(counteol>=1){
				counteol--;
			}
			return(counteol);
		}catch(FileNotFoundException e){
			Main.statusView.append("AutoException.InsertRuleNum内でError: FileNotFoundExceptionです．\n");
		}catch(IOException e){
			Main.statusView.append("Error: IOExceptionです．\n");
		}
		return(0);
	}
	
	//テストデータファイルを引数にとってデータ数を読み取る　TestDataNumに保存
	private static void InsertTestDataNum(File TestFile){
		try{
			FileReader fr = new FileReader(TestFile);
			StreamTokenizer token = new StreamTokenizer(fr);
			token.eolIsSignificant(true);
			token.nextToken();
			TestDataNum = (int)token.nval;
			Main.statusView.append("■TestDataNum = "+TestDataNum+"\n");
		}catch(FileNotFoundException e){
			Main.statusView.append("AutoException.InsertTestDataNum内でError: FileNotFoundExceptionです．\n");
		}catch(IOException e){
			Main.statusView.append("Error: IOExceptionです．\n");
		}
	}
	
	//LDのデータ数を読み取る　二段目以降の精度表示に使用
	private static void InsertFirstDataNum(File FirstDataFile){
		try{
			FileReader fr = new FileReader(FirstDataFile);
			StreamTokenizer token = new StreamTokenizer(fr);
			token.eolIsSignificant(true);
			token.nextToken();
			FirstDataNum = (int)token.nval;
			token.nextToken(); //改行文字
			token.nextToken();
			zokusei = (int)token.nval;
			Main.statusView.append("■FirstDataNum = "+FirstDataNum+"\n");
			Main.statusView.append("■zokusei = "+zokusei+"\n");
		}catch(FileNotFoundException e){
			Main.statusView.append("### AutoException.InsertFirstDataNum(File FirstDataFile)内でError: FileNotFoundExceptionです．\n");
		}catch(IOException e){
			Main.statusView.append("### Error: IOExceptionです．\n");
		}
	}
	
	//ルール数によらない(途中で再生成の必要のない)配列の作成
	private static void CreateArray1(){
		 DataArray = new BigDecimal[DataNum+1][zokusei+1+2];	//ルール適合フラグ用の箱とクラス用の箱を用意	
		 DCArray = new int[zokusei+1];	//属性番号を合わせるため1つずらして使用する.
		 RecursiveDataArray = new BigDecimal[DataNum+1][zokusei+1+2]; //δ1,2を超えたデータ配列
		 TestDataArray = new BigDecimal[TestDataNum+1][zokusei+1+2]; //テストデータを記憶する配列
		 FirstDataArray = new BigDecimal[FirstDataNum+1][zokusei+1+2];
	}
	
	//ルール数による(ルールが変わるたびに再生成する)配列の作成　
	private static void CreateArray2(){
		RuleAccuracyArray = new BigDecimal[RuleNum+1];//各ルールの精度
		SupportRateArray = new BigDecimal[RuleNum+1];//各ルールの精度
		ErrorRateArray = new BigDecimal[RuleNum+1];//各ルールの精度
		CorrectDataNumArray = new int[RuleNum+1];//各ルールに沿ったデータ数
		ErrorDataNumArray = new int[RuleNum+1];//各ルールに沿わない（クラスだけ異なる）データ数
	}
	
	//CDistinctionとRuleArrayは細分化の途中で逐次再生成が必要なので別メソッドで作成
	//Saving=退避(作業)用配列　
	private static void CreateSavingCDistinction(){
		SavingCDistinction = new int[RuleNum+1][zokusei+2+1];
		for(int i=1;i<=RuleNum;i++){
			for(int j=1;j<=zokusei+2;j++){
				SavingCDistinction[i][j] = CDistinction[i][j];
			}
		}
	}
	
	private static void CreateCDistinction(int Rulenum){
		CDistinction = new int[Rulenum+1][zokusei+1+2];
	}
	
	private static void ReturnCDistinction(){
		for(int i=1;i<=RuleNum;i++){
			for(int j=1;j<=zokusei+2;j++){
				CDistinction[i][j] = SavingCDistinction[i][j];
			}
		}
	}
	
	private static void CreateRuleArray(){
		RuleArray = new BigDecimal[RuleNum+1][zokusei+1+2];
	}
	
	private static void ArrayAction(){
		File DataFile = new File(DataFilePath);
		File RuleFile = new File(RuleFilePath);
		File TestFile = new File(TestFilePath);
		File FirstDataFile = new File(FirstDataFilePath);
		DataArrayAction(DataFile);
		RuleArrayAction(RuleFile);
		TestArrayAction(TestFile);
		FirstDataArrayAction(FirstDataFile);
	}
	
	private static void DataArrayAction(File DataFile){
		//データ配列の初期化と格納、表示
		FormatDataArray();
		InsertDataArray(DataFile);
		//ListDataArray();
		//データからDC配列を作成、表示
		FormatDCArray();			
		CreateDCArray(DataFile);
		ListDCArray();
	}
	
	private static void RuleArrayAction(File RuleFile){
		//ルール配列の初期化＋データ格納＋表示とCDistinctionの初期化
		FormatRuleArray();
		InsertRuleArray(RuleFile);
		ListRuleArray();
	}
	
	private static void TestArrayAction(File TestFile){
		//データ配列の初期化と格納
		FormatTestDataArray();
		InsertTestDataArray(TestFile);
	}
	
	private static void FirstDataArrayAction(File FirstDataFile){
		//データ配列の初期化と格納
		FormatFirstDataArray();
		InsertFirstDataArray(FirstDataFile);
	}
	
	private static void Start(){
		CreateArray1(); //　配列の作成（要素数の指定）
		CreateArray2();
		CreateRuleArray();
		CreateCDistinction(RuleNum);
		ArrayAction(); // 配列の中身を代入
		Recursive(); // 精度チェック＋ルールの細分化　再帰的に呼び出す
		
		// ルールの統合
		if(Main.RoopNum > 1){
			try {
				AutoIntegration.Integrate();
			} catch (IOException e) {
				e.printStackTrace();
			}
			// RuleArray[][]を統合ルールに更新
			File IntegratedRuleFile = new File("c:\\FullAutoReRX\\" + Main.ProjectName + "\\" + Main.RuleName[Main.RoopNum-1] + "\\IntegratedRuleSet" + (Main.RoopNum-1) + ".txt");
			RuleNum = InsertRuleNum(IntegratedRuleFile);
			CreateRuleArray();
			RuleArrayAction(IntegratedRuleFile);
			// 精度計算
			// 統合後のルールと比較するため、一度ルールに沿っているかの情報を初期化してから計算する
			CreateArray2(); //エラー回避のため配列を再生成する。中身は使用しない
			FormatDataArray();
			CompareRule();
			CountTrueNum();
		}
		//以下統合後の精度計算・表示
		//統合後はトレーニング、テスト、LD、全データとの精度計算を行う
		//ResultCount=0は統合後の精度表示を行うことを示す
		//トレーニングデータとの精度表示後にResultCountはインクリメントされ、次段開始時には1から始まる
		ResultCount = 0;
		//統合後ルールのトレーニングデータとの精度計算
		IndicationResult(ResultCount);	
		
		// 統合後ルールのテストデータとの精度計算
		FormatTestDataArray(); // 精度計算のためテストデータ配列の初期化
		CompareTest(); //ルールとの比較
		CountTrueTestData(); //ルールに沿ったデータ数の計算
		TestDataAccuracy(); // 精度計算結果表示
		
		//統合後の全トレーニングデータ(LD)との精度計算
		FormatFirstDataArray(); // 精度計算のためテストデータ配列の初期化
		CompareFirstData(); //ルールとの比較
		CountTrueFirstData(); //ルールに沿ったデータ数の計算
		FirstDataAccuracy(); // 精度計算結果表示
		
		//統合後の全データ(LD+テストデータ)との精度計算
		AllDataAccuracy();
		
		CreateCorrectPreserve(); //ルールに沿ったデータの保管ファイルを作成
		CreateNoisePreserve(); //ルールに沿わないデータの保管ファイルを作成
		WriteCorrectData();	//ルールに沿ったデータをファイルに書き込み
		WriteNoiseData(); //ノイズデータをファイルに書き込み
		
		
	}
	
	
	//DataArrayの初期化([][0]=0の状態にする)
	private static void FormatDataArray(){
		for(int i=0;i<=DataNum;i++){
			DataArray[i][0] = BigDecimal.valueOf(0); ;
		}
	}
	
	
	//DataArrayの代入　
	private static void InsertDataArray(File f){
		try{
			//fileReaderオブジェクトの作成
			FileReader fr = new FileReader(f);
			//StreamTokenizerオブジェクトを作成.取得したトークンは数字ならnval, 文字列ならsval, その他ならttypeにデータが入る
			StreamTokenizer token = new StreamTokenizer(fr);
			//EOLを読み込むためのパラメータ
			token.eolIsSignificant(true);
			int i = 0, j = 0;
			//ファイル読み込みの終了フラグ
			boolean eof = false;
			//ファイルの改行の読み込み(データの切れ目)を示す
			boolean eol = false;
			boolean startdata = false;
			int counteol = 0;
			int tmp;
			while(!startdata){
				if(counteol == 4){
					startdata = true;
					break;
				}
				tmp = token.nextToken();
				switch(tmp){
				case StreamTokenizer.TT_EOL:
					counteol++;
					break;	
				default: break;
				}
			}
			while(!eof){
				i++;
				j = 0;
				eol = false;
				while(!eol){
					tmp = token.nextToken();
					switch(tmp){
					case StreamTokenizer.TT_NUMBER:
						//データの読み込みと配列の格納
						j++;
						DataArray[i][j] = BigDecimal.valueOf(token.nval);
						break;
					case StreamTokenizer.TT_WORD:
						break;
					case StreamTokenizer.TT_EOL:
						//1つデータの読み込みが終わったことを示す
						eol = true;
						break;	
					case StreamTokenizer.TT_EOF:
						//ファイルの終了(全体の読み込みを終了する)
						eol = true;
						eof = true;
						break;
					default: break;
					}
				}
			}	
			System.out.println();
		}catch(FileNotFoundException e){
			System.out.println("AutoException.InsertDataArray内でError: FileNotFoundExceptionです．");
		}catch(IOException e){
			System.out.println("Error: IOExceptionです．");
		}
	}
	
	
	//DataArrayの表示
	private void ListDataArray(){
		for(int i=1;i<=DataNum;i++){
			for(int j=1;j<=zokusei+2;j++){
				System.out.print(df.format(DataArray[i][j]));
				if(j!=zokusei+2)
					System.out.print(",");
			}
			System.out.println();
		}
	}
	
	
	//RuleArrayの初期化　全データに未使用属性を示す2を格納
	private static void FormatRuleArray(){
		for(int i=1;i<=RuleNum;i++){
			for(int j=1;j<=zokusei;j++){
				RuleArray[i][j] = BigDecimal.valueOf(2);
			}
		}
	}
	
	
	//RuleArrayの代入
	private static void InsertRuleArray(File Rulefile){
		try{
			FileReader fr = new FileReader(Rulefile);
			StreamTokenizer token = new StreamTokenizer(fr);
			token.eolIsSignificant(true);
			String str;
			int tmp;
			int i=1,j=1;
			//不等号判別用に一時的に記憶する配列　
			int a[] = new int[2];
			boolean eof = false;
		while(!eof){	
			tmp = token.nextToken();
			switch(tmp){
			case StreamTokenizer.TT_NUMBER:
				break;
			case StreamTokenizer.TT_WORD:
				str = token.sval.replaceAll("[0-9]", "");
				if(str.equals("D")){
					j = Integer.parseInt(token.sval.replaceAll("[^0-9]", ""));
					tmp = token.nextToken();	//=
					tmp = token.nextToken();	//値まで移動
					RuleArray[i][j] = BigDecimal.valueOf(token.nval);	//　DJの値を格納(正確には上書き更新)
				}
				if(str.equals("C")){
					j = Integer.parseInt(token.sval.replaceAll("[^0-9]", ""));
					tmp = token.nextToken();
					a[0] = tmp;		//一つ目の記号
					tmp = token.nextToken();
					if(tmp != StreamTokenizer.TT_NUMBER){
						a[1] = tmp;  //次も記号なら読み込む	
						tmp = token.nextToken();
					}
					InsertCDistinction(a,i,j);	//CDistinctionの格納
					CrearArray(a,2);		//記号読み取り用配列のクリア
					RuleArray[i][j] = BigDecimal.valueOf(token.nval);
				}
				if(str.equals("Class")){
					tmp = token.nextToken();
					if(token.nval == 1){
						RuleArray[i][zokusei+1] = BigDecimal.valueOf(0);
						RuleArray[i][zokusei+2] = BigDecimal.valueOf(1);
					}
					else{
						RuleArray[i][zokusei+1] = BigDecimal.valueOf(1);
						RuleArray[i][zokusei+2] = BigDecimal.valueOf(0);
					}
				}
				break;
				
			case StreamTokenizer.TT_EOL:
				i++;  //改行の数でルールの数を把握する
				break;
				
			case StreamTokenizer.TT_EOF:
				eof = true; 
				break;
				
			default:	
				break;
			}
		}
		}catch(FileNotFoundException e){
			System.out.println("AutoException.InsertRuleArray内でError: FileNotFoundExceptionです．");
		}catch(IOException e){
			System.out.println("Error: IOExceptionです．");
		}
	}
	
	
	//RuleArrayの表示
	private static void ListRuleArray(){
		for(int i=1;i<=RuleNum;i++){
			Main.statusView.append("R" + i + ": ");
			for(int j=1;j<=zokusei;j++){
				if(RuleArray[i][j].compareTo(BigDecimal.valueOf(2)) != 0){
					if(DCArray[j]==0){
						Main.statusView.append("D" + j + " = " + RuleArray[i][j].setScale(0) + " ");
					}
					if(DCArray[j]==1){ 
						if(CDistinction[i][j] == 1){
							Main.statusView.append("C" + j + " > " + df.format(RuleArray[i][j]) + " ");
						}
						else if(CDistinction[i][j] == 2){
							Main.statusView.append("C" + j + " >= " + df.format(RuleArray[i][j]) + " ");
						}
						else if(CDistinction[i][j] == 3){
							Main.statusView.append("C" + j + " = " + df.format(RuleArray[i][j]) + " ");
						}
						else if(CDistinction[i][j] == 4){
							Main.statusView.append("C" + j + " <= " + df.format(RuleArray[i][j]) + " ");
						}
						else if(CDistinction[i][j] == 5){
							Main.statusView.append("C" + j + " < " + df.format(RuleArray[i][j]) + " ");
						}
					}
				}
				if(j==zokusei){
					if(RuleArray[i][zokusei+1].compareTo(BigDecimal.valueOf(0)) == 0){
						Main.statusView.append("then Class 1\n");
					}
					else{
						Main.statusView.append("then Class 2\n");
					}
				}
			}
		}
	}
	
	private static void FormatTestDataArray(){
		for(int i=0;i<=TestDataNum;i++){
			TestDataArray[i][0] = BigDecimal.valueOf(0); ;
		}
	}
	
	private static void InsertTestDataArray(File TestFile){
		try{
			//fileReaderオブジェクトの作成
			FileReader fr = new FileReader(TestFile);
			//StreamTokenizerオブジェクトを作成.取得したトークンは数字ならnval, 文字列ならsval, その他ならttypeにデータが入る
			StreamTokenizer token = new StreamTokenizer(fr);
			//EOLを読み込むためのパラメータ
			token.eolIsSignificant(true);
			int i = 0, j = 0;
			//ファイル読み込みの終了フラグ
			boolean eof = false;
			//ファイルの改行の読み込み(データの切れ目)を示す
			boolean eol = false;
			boolean startdata = false;
			int counteol = 0;
			int tmp;
			while(!startdata){
				if(counteol == 4){
					startdata = true;
					break;
				}
				tmp = token.nextToken();
				switch(tmp){
				case StreamTokenizer.TT_EOL:
					counteol++;
					break;	
				default: break;
				}
			}
			while(!eof){
				i++;
				j = 0;
				eol = false;
				while(!eol){
					tmp = token.nextToken();
					switch(tmp){
					case StreamTokenizer.TT_NUMBER:
						//データの読み込みと配列の格納
						j++;
						TestDataArray[i][j] = BigDecimal.valueOf(token.nval);
						break;
					case StreamTokenizer.TT_WORD:
						break;
					case StreamTokenizer.TT_EOL:
						//1つデータの読み込みが終わったことを示す
						eol = true;
						break;	
					case StreamTokenizer.TT_EOF:
						//ファイルの終了(全体の読み込みを終了する)
						eol = true;
						eof = true;
						break;
					default: break;
					}
				}
			}	
			System.out.println();
		}catch(FileNotFoundException e){
			System.out.println("AutoException.InsertTestDataArray内でError: FileNotFoundExceptionです．");
		}catch(IOException e){
			System.out.println("Error: IOExceptionです．");
		}
	}
	

	//DataArrayの初期化([][0]=0の状態にする)
	private static void FormatFirstDataArray(){
		for(int i=0;i<=FirstDataNum;i++){
			FirstDataArray[i][0] = BigDecimal.valueOf(0); ;
		}
	}
	
	
	//DataArrayの代入　
	private static void InsertFirstDataArray(File f){
		try{
			//fileReaderオブジェクトの作成
			FileReader fr = new FileReader(f);
			//StreamTokenizerオブジェクトを作成.取得したトークンは数字ならnval, 文字列ならsval, その他ならttypeにデータが入る
			StreamTokenizer token = new StreamTokenizer(fr);
			//EOLを読み込むためのパラメータ
			token.eolIsSignificant(true);
			int i = 0, j = 0;
			//ファイル読み込みの終了フラグ
			boolean eof = false;
			//ファイルの改行の読み込み(データの切れ目)を示す
			boolean eol = false;
			boolean startdata = false;
			int counteol = 0;
			int tmp;
			while(!startdata){
				if(counteol == 4){
					startdata = true;
					break;
				}
				tmp = token.nextToken();
				switch(tmp){
				case StreamTokenizer.TT_EOL:
					counteol++;
					break;	
				default: break;
				}
			}
			while(!eof){
				i++;
				j = 0;
				eol = false;
				while(!eol){
					tmp = token.nextToken();
					switch(tmp){
					case StreamTokenizer.TT_NUMBER:
						//データの読み込みと配列の格納
						j++;
						FirstDataArray[i][j] = BigDecimal.valueOf(token.nval);
						break;
					case StreamTokenizer.TT_WORD:
						break;
					case StreamTokenizer.TT_EOL:
						//1つデータの読み込みが終わったことを示す
						eol = true;
						break;	
					case StreamTokenizer.TT_EOF:
						//ファイルの終了(全体の読み込みを終了する)
						eol = true;
						eof = true;
						break;
					default: break;
					}
				}
			}	
			System.out.println();
		}catch(FileNotFoundException e){
			System.out.println("AutoException.InsertDataArray内でError: FileNotFoundExceptionです．");
		}catch(IOException e){
			System.out.println("Error: IOExceptionです．");
		}
	}
	
	
	//CDistinctionの初期化
	private static void FormatCDistinction(int Rulenum){ 
		for(int i=1;i<=Rulenum;i++){
			for(int j=1;j<=zokusei;j++){
				CDistinction[i][j] = 0;
			}
		}
	}
	
	
	//CDistinctionに値を代入
	private static  void InsertCDistinction(int a[],int i, int j){
		if(a[0] == '>'){
			if(a[1] == '='){
				CDistinction[i][j] = 2;
			}
			else{
				CDistinction[i][j] = 1;
			}
		}
		else if(a[0] == '='){
			CDistinction[i][j] = 3;
		}
		else if(a[0] == '<'){
			if(a[1] == '='){
				CDistinction[i][j] = 4;
			}
			else{
				CDistinction[i][j] = 5;
			}
		}
	}
		
		
		//不等号判別用配列を初期化
	private static void CrearArray(int a[],int b){
		for(int i=0;i<b;i++){
			a[i] = 0;
		}
	}
	
	
	//DCArrayの初期化　判別用に2を代入する
	private static void FormatDCArray(){
		for(int i=1;i<=zokusei;i++){
			DCArray[i] = 2;
		}
	}
	
	//各属性のDC判別用配列、DCArray作成
		private static void CreateDCArray(File file){
			try{	
				FileReader fr = new FileReader(file);		
				StreamTokenizer token = new StreamTokenizer(fr);
				token.eolIsSignificant(true);
				int i = 1;
				String str;
				boolean eol = false;	
				int tmp;
				for(int j=0;j<6;j++){//4行目の手前まで移動
					tmp = token.nextToken();
				}
				while(!eol){
					tmp = token.nextToken();
					switch(tmp){
					case StreamTokenizer.TT_NUMBER:
						break;
					case StreamTokenizer.TT_WORD:
						str = token.sval.replaceAll("[0-9]", "");
						if(str.equals("D")){
							DCArray[i] = 0;
							i++;
							tmp = token.nextToken();	//,
						}
						if(str.equals("C")){
							DCArray[i] = 1;
							i++;
							tmp = token.nextToken();	//,
						}
						break;
					case StreamTokenizer.TT_EOL:
						eol = true;
						break;	
					default: break;
					}
				}
			}catch(FileNotFoundException e){
				System.out.println("AutoException.CreateDCArray内でError: FileNotFoundExceptionです．");
			}catch(IOException e){
				System.out.println("Error: IOExceptionです．");
			}
		}
	
	
	//DCArrayの表示	
	private static void ListDCArray(){
		for(int i=1;i<=zokusei;i++){
			if(DCArray[i] == 0)
				System.out.print("D"+i);
			else if(DCArray[i] == 1){
				System.out.print("C"+i);
			}
			if(i < zokusei){
				System.out.print(",");
			}
			else{
				System.out.println("");
			}
		}
	}
	
	
	// NoisePreserveを作成
	// LDとPrimaryRuleを比べた場合は、LDfにあたる。
	private static void CreateNoisePreserve(){
		try{
			File newfile = new File("c:\\FullAutoReRX\\"+Main.ProjectName+"\\" + Main.RuleName[Main.RoopNum-1] + "\\"+Main.LearningDataName+"f.txt");
			if (newfile.createNewFile()){
				System.out.println(Main.LearningDataName+"f.txtの作成に成功しました");
			}else{
				System.out.println(Main.LearningDataName+"f.txtはすでに存在しているため上書きします");
			}
		}catch(IOException e){
			System.out.println(e);
		}
	}
	
	
	//CorrectPreserveを作成
	private static void CreateCorrectPreserve(){
		try{
			File newfile = new File("c:\\FullAutoReRX\\"+Main.ProjectName+"\\" + Main.RuleName[Main.RoopNum-1] + "\\CorrectPreserve"+Main.RoopNum+".txt");
			if (newfile.createNewFile()){
				System.out.println("CorrectPreserve.txtの作成に成功しました");
			}else{
				System.out.println("CorrectPreserve.txtはすでに存在しているため上書きします");
			}
		}catch(IOException e){
			System.out.println(e);
		}
	}
	
	
	//データがルールに沿っているかを調べる
	//各ルールの使われている属性に対して、すでにルールに沿っているデータ以外を比較
	//比較はCompareDataメソッドを呼び出す
	private static void CompareRule(){
		int Di=1,Ri=1,Rj=1,OutRule=0;
		for(Ri=1;Ri<=RuleNum;Ri++){
			for(Di=1;Di<=DataNum;Di++){
				OutRule = 0;
				for(Rj=1;Rj<=zokusei;Rj++){
					if(DataArray[Di][0].compareTo(BigDecimal.valueOf(1)) != 0){
						if(RuleArray[Ri][Rj].compareTo(BigDecimal.valueOf(2)) != 0){
							OutRule += CompareData(Di,Ri,Rj);
						}
					}
				}
				if(DataArray[Di][0].compareTo(BigDecimal.valueOf(1)) != 0){
					if(OutRule == 0){
						if(ClassErrorCheck(Ri,Di) == false){
							CorrectDataNumArray[Ri]++;
							DataArray[Di][0] = BigDecimal.valueOf(1);
						}
						else{
							ErrorDataNumArray[Ri]++;
						}
					}
				}
			}
		}
	}
	
	
	//データがルールに沿っているかを調べる
	//沿っていれば0を、沿っていなければ1を返す
	private static int CompareData(int Di,int Ri,int Rj){
		boolean OutRule = false;
		//離散値データ
		if(DCArray[Rj] == 0){
			if(RuleArray[Ri][Rj].compareTo(DataArray[Di][Rj]) != 0){
				OutRule = true;		
			}
		}
		//連続値データ
		else if(DCArray[Rj]==1){
				switch(CDistinction[Ri][Rj]){
				case 1:	//>
					if(DataArray[Di][Rj].compareTo(RuleArray[Ri][Rj]) <= 0)
						OutRule = true;	
					break;
				case 2:	//>=
					if(DataArray[Di][Rj].compareTo(RuleArray[Ri][Rj]) < 0)
						OutRule = true;	
					break;
				case 3:	//=
					if(DataArray[Di][Rj].compareTo(RuleArray[Ri][Rj]) != 0)
						OutRule = true;	
					break;
				case 4:	//<=
					if(DataArray[Di][Rj].compareTo(RuleArray[Ri][Rj]) > 0)
						OutRule = true;	
					break;
				case 5:	//<
					if(DataArray[Di][Rj].compareTo(RuleArray[Ri][Rj]) >= 0)
						OutRule = true;	
					break;
				default:
					break;
				}
			}
		if(OutRule == true)
			return(1);
		else{
			return(0);
		}
	}
	
	private static boolean ClassErrorCheck(int Ri,int Di){
		if(RuleArray[Ri][zokusei+1].compareTo(DataArray[Di][zokusei+1]) != 0)
			return(true);
		else
			return(false);
	}
	
	private static void CompareTest(){
		int OutRule = 0;
		for(int Ri=1;Ri<=RuleNum;Ri++){
			for(int Di=1;Di<=TestDataNum;Di++){
				OutRule = 0;
				for(int Rj=1;Rj<=zokusei;Rj++){
					if(TestDataArray[Di][0].compareTo(BigDecimal.valueOf(1)) != 0){
						if(RuleArray[Ri][Rj].compareTo(BigDecimal.valueOf(2)) != 0){
							OutRule += CompareTestData(Di,Ri,Rj);
							OutRule += TestClassErrorCheck(Ri,Di);
						}
					}
				}
				if(OutRule == 0)
					TestDataArray[Di][0] = BigDecimal.valueOf(1);
			}
		}
	}
	
	private static int CompareTestData(int Di,int Ri,int Rj){
		boolean OutRule = false;
		//離散値データ
		if(DCArray[Rj] == 0){
			if(RuleArray[Ri][Rj].compareTo(TestDataArray[Di][Rj]) != 0){
				OutRule = true;		
			}
		}
		//連続値データ
		else if(DCArray[Rj]==1){
				switch(CDistinction[Ri][Rj]){
				case 1:	//>
					if(TestDataArray[Di][Rj].compareTo(RuleArray[Ri][Rj]) <= 0)
						OutRule = true;	
					break;
				case 2:	//>=
					if(TestDataArray[Di][Rj].compareTo(RuleArray[Ri][Rj]) < 0)
						OutRule = true;	
					break;
				case 3:	//=
					if(TestDataArray[Di][Rj].compareTo(RuleArray[Ri][Rj]) != 0)
						OutRule = true;	
					break;
				case 4:	//<=
					if(TestDataArray[Di][Rj].compareTo(RuleArray[Ri][Rj]) > 0)
						OutRule = true;	
					break;
				case 5:	//<
					if(TestDataArray[Di][Rj].compareTo(RuleArray[Ri][Rj]) >= 0)
						OutRule = true;	
					break;
				default:
					break;
				}
			}
		if(OutRule == true)
			return(1);
		else{
			return(0);
		}
	}
	
	private static int TestClassErrorCheck(int Ri,int Di){
		if(RuleArray[Ri][zokusei+1].compareTo(TestDataArray[Di][zokusei+1]) != 0)
			return(1); //クラスが異なる
		else
			return(0); //クラスが同じ
	}
	
	private static void CountTrueTestData(){
		CountTrueTestDataNum = 0;
		for(int Di=1;Di<=TestDataNum;Di++){
			if(TestDataArray[Di][0].compareTo(BigDecimal.valueOf(1)) == 0){
				CountTrueTestDataNum++;
			}
		}
	}
	
	private static void TestDataAccuracy(){
		Main.statusView2.append("統合後の"+Main.RuleName[Main.RoopNum-1]+"のテストデータ認識率: "+ (((double)CountTrueTestDataNum/(double)TestDataNum)*100) +"%\n");
		Main.statusView2.append("データ数:"+TestDataNum+", 適合数:"+CountTrueTestDataNum+", エラー数:"+(TestDataNum-CountTrueTestDataNum)+"\n");
	}
	
	private static void CompareFirstData(){
		int OutRule = 0;
		for(int Ri=1;Ri<=RuleNum;Ri++){
			for(int Di=1;Di<=FirstDataNum;Di++){
				OutRule = 0;
				for(int Rj=1;Rj<=zokusei;Rj++){
					if(FirstDataArray[Di][0].compareTo(BigDecimal.valueOf(1)) != 0){
						if(RuleArray[Ri][Rj].compareTo(BigDecimal.valueOf(2)) != 0){
							OutRule += CompareFirstData(Di,Ri,Rj);
							OutRule += FirstDataClassErrorCheck(Ri,Di);
						}
					}
				}
				if(OutRule == 0)
					FirstDataArray[Di][0] = BigDecimal.valueOf(1);
			}
		}
	}
	
	private static int CompareFirstData(int Di,int Ri,int Rj){
		boolean OutRule = false;
		//離散値データ
		if(DCArray[Rj] == 0){
			if(RuleArray[Ri][Rj].compareTo(FirstDataArray[Di][Rj]) != 0){
				OutRule = true;		
			}
		}
		//連続値データ
		else if(DCArray[Rj]==1){
				switch(CDistinction[Ri][Rj]){
				case 1:	//>
					if(FirstDataArray[Di][Rj].compareTo(RuleArray[Ri][Rj]) <= 0)
						OutRule = true;	
					break;
				case 2:	//>=
					if(FirstDataArray[Di][Rj].compareTo(RuleArray[Ri][Rj]) < 0)
						OutRule = true;	
					break;
				case 3:	//=
					if(FirstDataArray[Di][Rj].compareTo(RuleArray[Ri][Rj]) != 0)
						OutRule = true;	
					break;
				case 4:	//<=
					if(FirstDataArray[Di][Rj].compareTo(RuleArray[Ri][Rj]) > 0)
						OutRule = true;	
					break;
				case 5:	//<
					if(FirstDataArray[Di][Rj].compareTo(RuleArray[Ri][Rj]) >= 0)
						OutRule = true;	
					break;
				default:
					break;
				}
			}
		if(OutRule == true)
			return(1);
		else{
			return(0);
		}
	}
	
	private static int FirstDataClassErrorCheck(int Ri,int Di){
		if(RuleArray[Ri][zokusei+1].compareTo(FirstDataArray[Di][zokusei+1]) != 0)
			return(1); //クラスが異なる
		else
			return(0); //クラスが同じ
	}
	
	private static void CountTrueFirstData(){
		CountTrueFirstDataNum = 0;
		for(int Di=1;Di<=FirstDataNum;Di++){
			if(FirstDataArray[Di][0].compareTo(BigDecimal.valueOf(1)) == 0){
				CountTrueFirstDataNum++;
			}
		}
	}
	
	private static void FirstDataAccuracy(){
		Main.statusView2.append("統合後の"+Main.RuleName[Main.RoopNum-1]+"の全トレーニングデータ認識率: "+ (((double)CountTrueFirstDataNum/(double)FirstDataNum)*100) +"%\n");	
		Main.statusView2.append("データ数:"+FirstDataNum+", 適合数:"+CountTrueFirstDataNum+", エラー数:"+(FirstDataNum-CountTrueFirstDataNum)+"\n");
	}
	
	//全データ(LD+テストデータ)との精度の比較
	private static void AllDataAccuracy(){
		int AllDataNum = (FirstDataNum + TestDataNum);
		int TrueAllDataNum = (CountTrueFirstDataNum + CountTrueTestDataNum);
		int ErrorAllDataNum = (AllDataNum - TrueAllDataNum);
		double AllDataAccuracy = ((double)TrueAllDataNum / (double)AllDataNum); 
		Main.statusView2.append("統合後の"+Main.RuleName[Main.RoopNum-1]+"の全データ認識率: "+ (AllDataAccuracy * 100) +"%\n");
		Main.statusView2.append("データ数:"+AllDataNum+", 適合数:"+TrueAllDataNum+", エラー数:"+ErrorAllDataNum+"\n");
		Main.statusView2.append("\n\n");
	}
	
	
	//ノイズデータの書き込み
	private static void WriteNoiseData(){
		try{
			File newfile = new File("c:\\FullAutoReRX\\"+Main.ProjectName+"\\" + Main.RuleName[Main.RoopNum-1] + "\\"+Main.LearningDataName+"f.txt");
			PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter(newfile)));
			pw.println(DataNum-TrueNum);
			pw.println(zokusei);
			pw.println("2");
			for(int i=1;i<=zokusei;i++){
				if(DCArray[i] == 0)
					pw.print("D"+i);
				else if(DCArray[i] == 1){
					pw.print("C"+i);
				}
				if(i < zokusei){
					pw.print(",");
				}
				else{
					pw.println("");
				}
			}
			for(int Di=1;Di<=DataNum;Di++){
				if(DataArray[Di][0].compareTo(BigDecimal.valueOf(1)) != 0){
					for(int Dj=1;Dj<=zokusei;Dj++){
						//離散値属性の場合,int型に直す
						if(DCArray[Dj] == 0){
							pw.print(df.format(DataArray[Di][Dj]) + ",");
						}
						//連続値属性の場合はそのまま書き込む
						else if(DCArray[Dj] == 1){
							pw.print(df.format((BigDecimal)DataArray[Di][Dj]) + ",");
						}
					}
					pw.print(df.format(DataArray[Di][zokusei+1])+","+df.format(DataArray[Di][zokusei+2]));
					pw.println();
				}	
			}
			pw.close();
		}catch(FileNotFoundException e){
			System.out.println("AutoException.WriteNoiseData内でError: FileNotFoundExceptionです．");
		}catch(IOException e){
			System.out.println("Error: IOExceptionです．");
		}
		//2段目以前のノイズデータ数が0ならばその場で全プログラムを終了する
		//**4段目以降実装時には要変更**
        if((Main.RoopNum <= 2) && ((DataNum-TrueNum) == 0)){
        	System.out.println("トレーニングデータのデータ数が0になったためプログラムを終了します");
        	if(Main.RoopNum == 1)
        		Main.FirstEndTime = System.currentTimeMillis();
        	else if(Main.RoopNum == 2)
        		Main.SecondEndTime = System.currentTimeMillis();
        	Main.IndicationTime();
        	System.exit(0);
        }
	}
	
	
	//ルールに沿ったデータの書き込み
	private static void WriteCorrectData(){
		try{
			File newfile = new File("c:\\FullAutoReRX\\"+Main.ProjectName+"\\" + Main.RuleName[Main.RoopNum-1] + "\\CorrectPreserve"+Main.RoopNum+".txt");
			PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter(newfile)));
			pw.println(TrueNum);
			pw.println(zokusei);
			pw.println("2");
			for(int i=1;i<=zokusei;i++){
				if(DCArray[i] == 0)
					pw.print("D"+i);
				else if(DCArray[i] == 1){
					pw.print("C"+i);
				}
				if(i < zokusei){
					pw.print(",");
				}
				else{
					pw.println("");
				}
			}
			for(int Di=1;Di<=DataNum;Di++){
				if(DataArray[Di][0].compareTo(BigDecimal.valueOf(1)) == 0){
					for(int Dj=1;Dj<=zokusei;Dj++){
						//離散値属性の場合,int型に直す
						if(DCArray[Dj] == 0){
							pw.print(df.format(DataArray[Di][Dj]) + ",");
						}
						//連続値属性の場合はそのまま書き込む
						else if(DCArray[Dj] == 1){
							pw.print(df.format((BigDecimal)DataArray[Di][Dj]) + ",");
						}
					}
					pw.print(df.format(DataArray[Di][zokusei+1])+","+df.format(DataArray[Di][zokusei+2]));
					pw.println();
				}	
			}
			pw.close();
		}catch(FileNotFoundException e){
			System.out.println("AutoException.WriteCorrectData内でError: FileNotFoundExceptionです．");
		}catch(IOException e){
			System.out.println("Error: IOExceptionです．");
		}
	}
	
	
	private static void CountTrueNum(){
		TrueNum = 0; 
		for(int Di=1;Di<=DataNum;Di++){
			if(DataArray[Di][0].compareTo(BigDecimal.valueOf(1)) == 0){
				TrueNum++;
			}
		}
	}
	
	
	private static void IndicationResult(int resultCount){
		if(resultCount == 0){
			Main.statusView2.append("\n\n");
			Main.statusView2.append("統合後の"+Main.RuleName[Main.RoopNum-1]+"のトレーニングデータ認識率: "+ (((double)TrueNum/(double)DataNum)*100) +"%\n");
		}
		else{
			Main.statusView2.append(resultCount+"回目の"+Main.RuleName[Main.RoopNum-1]+"のトレーニングデータ認識率:: "+ (((double)TrueNum/(double)DataNum)*100) +"%\n");
		}
		Main.statusView2.append("データ数:"+DataNum+", 適合数:"+TrueNum+", エラー数:"+(DataNum-TrueNum)+"\n");
		ResultCount++;
	}
	
	
	
	//各ルールに沿っているデータ数と沿っていないデータ数の計算
	//%表示のため100倍
	private static void AccuracyList(){
		BigDecimal RuleAccuracy,SupportRate,ErrorRate;
		for(int Ri=1;Ri<=RuleNum;Ri++){
			System.out.println("CorrectDataNum"+Ri+"= "+CorrectDataNumArray[Ri]);
			System.out.println("ErrorDataNum"+Ri+"= "+ErrorDataNumArray[Ri]);
			RuleAccuracy = RuleAccuracyArray[Ri].multiply(BigDecimal.valueOf(100));
			SupportRate = SupportRateArray[Ri].multiply(BigDecimal.valueOf(100));
			ErrorRate = ErrorRateArray[Ri].multiply(BigDecimal.valueOf(100));
			System.out.println("Rule"+Ri+" Accuracy= "+RuleAccuracy+"%");
			System.out.println("Rule"+Ri+" SupportRate= "+SupportRate+"%");
			System.out.println("Rule"+Ri+" ErrorRate= "+ErrorRate+"%");
			System.out.println("");
		}
	}

	
	//各ルールの精度,サポート率、エラー率の計算
	private static void RuleAccuracyCalculation(){
		for(int Ri=1;Ri<=RuleNum;Ri++){
			 BigDecimal Correct = BigDecimal.valueOf(CorrectDataNumArray[Ri]);
			 BigDecimal Error = BigDecimal.valueOf(ErrorDataNumArray[Ri]);
			 BigDecimal AllData = BigDecimal.valueOf(DataNum);
			 BigDecimal Total = Correct.add(Error);
			 if(Total.compareTo(BigDecimal.valueOf(0)) == 0){//分母が0のときは0%と表示する
				 RuleAccuracyArray[Ri] = BigDecimal.valueOf(0);
				 SupportRateArray[Ri] = BigDecimal.valueOf(0);
				 ErrorRateArray[Ri] = BigDecimal.valueOf(0);
			 }
			 else{
				 RuleAccuracyArray[Ri] = Correct.divide(Total,10, BigDecimal.ROUND_HALF_UP);
				 SupportRateArray[Ri] = Total.divide(AllData,10, BigDecimal.ROUND_HALF_UP);
				 ErrorRateArray[Ri] = Error.divide(Total,10, BigDecimal.ROUND_HALF_UP);
			 }
		}
	}
	
	private static void Recursive(){
		SubdivisionCount++; //細分化回数
		SubdivisionCheck = false; //細分化を行ったかどうか（再帰するかどうか）のチェック
		CompareRule();	//ルールとデータの比較
		CountTrueNum();	//ルールに沿ったデータの数を数える
		IndicationResult(ResultCount); // トレーニングデータとのルール精度の表示
		RuleAccuracyCalculation();//ルール毎の精度の計算
		AccuracyList(); //ルール毎の精度の表示
		GoCompareRate(); //δ1,δ2との比較 および再帰 ルールの追加までこの中で行く
		FirstRuleNum = RuleNum; //一周終わったらFirstRuleNumを書き換える
		if(SubdivisionCheck == true){ //細分化が行われたときの動作
			CreateArray2(); //RuleNumに関係する配列は再生成する
			FormatDataArray(); //データがルールに沿っているかどうかの情報をクリアする
			Recursive(); //再帰的に呼び出す
		}
	}
	
	
	//δ1,δ2との比較
	//共に超えていればRecursiveDataSetファイルにルールに沿ったデータが書き込まれる
	private static void GoCompareRate(){
		boolean OutRate = false;
		for(int Ri=1;Ri<=FirstRuleNum;Ri++){
			OutRate = CompareSupportRate(Ri);
			if(OutRate == true){
				OutRate = CompareErrorRate(Ri);
				if(OutRate == true){
					if(SubdivisionCount >= 2){//細分化2回目以降はルールが書き換わっているかを調べる
						if(CompareBeforeRule(Ri) == false){
							System.out.println("R"+Ri+"がδ1、δ2を超えています");
							if(SubdivisionCheck == false){//各回の細分化一度目のみ行う動作
								SubdivisionCheck = true;//細分化が行われた
								SaveBeforeRule(); //書き換え前のルールの保存
							}
							CreateRecursiveDataArray(Ri); //再帰用に属性数を削ったデータセットを作成
							CreateRecursiveDataSetFile(Ri);//再帰用データセットを書き込むファイルを作成
							WriteRecursiveDataSet(Ri);//再帰用データセットをファイルに書き込み
							GoRecursive(Ri);//再帰開始
							DataCount = 1;//データ数のカウントを初期状態に戻す
						}
					}
					else{//細分化一回目は書き換わりのチェック無しで細分化を行う
						System.out.println("R"+Ri+"がδ1、δ2を超えています");
						if(SubdivisionCheck == false){//細分化一回目のみ行う動作
							SubdivisionCheck = true;//細分化が行われた
							SaveBeforeRule(); //書き換え前のルールの保存
						}
						CreateRecursiveDataArray(Ri);
						CreateRecursiveDataSetFile(Ri);
						WriteRecursiveDataSet(Ri);
						GoRecursive(Ri);
						DataCount = 1;
					}
				}
			}
		}
	}
	
	//δ1との比較
	private static boolean CompareSupportRate(int Ri){
		if(SupportRateArray[Ri].compareTo(δ1) > 0 )
			return(true);
		else
			return(false);
	}
	
	//δ2との比較
	private static boolean CompareErrorRate(int Ri){
		if(ErrorRateArray[Ri].compareTo(δ2) > 0 )
			return(true);
		else
			return(false);
	}
	
	//以前のルールを保存　次回の比較に使用する
	private static void SaveBeforeRule(){
		BeforeRuleArray = new BigDecimal[FirstRuleNum+1][zokusei+2+1]; 
		BeforeRuleNum = FirstRuleNum;
		for(int i=1;i<=FirstRuleNum;i++){
			for(int j=1;j<=zokusei+2;j++){
				BeforeRuleArray[i][j] = RuleArray[i][j];
			}
		}
	}
	
	//以前のルールと比較して変わっていなければ細分化は行わない
	private static boolean CompareBeforeRule(int Ri){
		if(Ri > BeforeRuleNum) //ルール数が増えている場合 
			return(false);
		if(Arrays.equals(RuleArray[Ri],BeforeRuleArray[Ri]))
			return(true);
		else
			return(false);
	}
	
	//再帰するデータを書き込むRecursiveDataSetファイルの作成
	private static void CreateRecursiveDataSetFile(int Ri){
		try{
			File newfile = new File("c:\\FullAutoReRX\\"+ Main.ProjectName +"\\" + Main.RuleName[Main.RoopNum-1] + "\\RecursiveDataSet "+Main.RuleName[Main.RoopNum-1]+Ri+".txt");
			if (newfile.createNewFile()){
				System.out.println("RecursiveDataSet"+Main.RuleName[Main.RoopNum-1]+Ri+".txtの作成に成功しました");
			}else{
				System.out.println("RecursiveDataSet"+Main.RuleName[Main.RoopNum-1]+Ri+".txtはすでに存在しているため上書きします");
			}
		}catch(IOException e){
			System.out.println(e);
		}
	}
	
	//再帰するデータを集めた配列を作成
	private static void CreateRecursiveDataArray(int Ri){
		int Di = 1,Rj = 1,check = 0;
		for(Di=1;Di<=DataNum;Di++){
			check = 0;
			for(Rj=1;Rj<=zokusei;Rj++){
				if(RuleArray[Ri][Rj].compareTo(BigDecimal.valueOf(2)) != 0){
					check += CompareData(Di,Ri,Rj);
				}
			}
			if(check == 0){
				for(int i=0;i<=zokusei+2;i++){
					RecursiveDataArray[DataCount][i] = DataArray[Di][i];
				}
				DataCount++;
			}
		}
	}
	
	//再帰用データをファイルに書き込み
	private static void WriteRecursiveDataSet(int Ri){
		int countzokusei = 0;
		for(int Rj=1;Rj<=zokusei;Rj++){
			if(RuleArray[Ri][Rj].compareTo(BigDecimal.valueOf(2)) != 0){//2は未使用属性
				countzokusei++;
			}
		}
		try{
			File newfile = new File("c:\\FullAutoReRX\\"+ Main.ProjectName +"\\" + Main.RuleName[Main.RoopNum-1] + "\\RecursiveDataSet "+Main.RuleName[Main.RoopNum-1]+Ri+".txt");
			FinalRuleFilePath = newfile.getAbsolutePath();
			PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter(newfile)));
			pw.println(DataCount-1);
			pw.println((zokusei-countzokusei));
			pw.println("2");
			for(int i=1;i<=zokusei;i++){
				if(RuleArray[Ri][i].compareTo(BigDecimal.valueOf(2)) == 0){
					if(DCArray[i] == 0)
						pw.print("D"+i);
					else if(DCArray[i] == 1){
						pw.print("C"+i);
					}
				}
				if(RuleArray[Ri][i].compareTo(BigDecimal.valueOf(2)) == 0 && i < zokusei){
					pw.print(",");
				}
				else if(i == zokusei){
					pw.println("");
				}
			}
			for(int Ei=1;Ei<DataCount;Ei++){
				for(int Ej=1;Ej<=zokusei;Ej++){
					if(RuleArray[Ri][Ej].compareTo(BigDecimal.valueOf(2)) == 0){
						//離散値属性
						if(DCArray[Ej] == 0){
							pw.print(df.format(RecursiveDataArray[Ei][Ej]) + ",");
						}
						//連続値属性
						else if(DCArray[Ej] == 1){
							pw.print(df.format((BigDecimal)RecursiveDataArray[Ei][Ej]) + ",");
						}
					}
				}
				pw.print(df.format(RecursiveDataArray[Ei][zokusei+1])+","+df.format(RecursiveDataArray[Ei][zokusei+2]));
				pw.println();
			}	
			pw.close();
		}catch(FileNotFoundException e){
			Main.statusView.append("Error: WriteRecursiveDataSet(int Ri)内でFileNotFoundExceptionです．");
		}catch(IOException e){
			Main.statusView.append("Error: WriteRecursiveDataSet(int Ri)内でIOExceptionです．");
		}
	}
	
	//細分化の開始
	private static void GoRecursive(int Ri){
		File TrainingFile = new File("c:\\FullAutoReRX\\"+ Main.ProjectName +"\\" + Main.RuleName[Main.RoopNum-1] + "\\RecursiveDataSet "+Main.RuleName[Main.RoopNum-1]+Ri+".txt");
		Main.openTrainingSample2(TrainingFile);
		Main.learning();
		Main.pruning();
		Main.startRecog(Ri); //テストデータ認識開始
		try{
			J48Test.RecursiveJ_48(Ri);
		}catch(Exception e){
			System.out.println("Error: Exceptionです．");
		}
		Main.statusView.append(Main.RuleName[Main.RoopNum-1]+Ri+".Recursive.txtが作成されました\n");
		File AddRuleFile = new File("c:\\FullAutoReRX\\" + Main.ProjectName + "\\" + Main.RuleName[Main.RoopNum-1] + "\\"+Main.RuleName[Main.RoopNum-1]+Ri+".Recursive.txt"); //追加ルールファイルを指定
		AddRule(AddRuleFile,Ri);
	}
	
	//新しいファイルのルールと今までのルールを統合する
	private static void AddRule(File AddRuleFile,int Ri){
		int AddRuleNum = 0;
		AddRuleNum = InsertRuleNum(AddRuleFile);//追加ルールファイルのルール数を調べる
		/*
		 *  11月5日修正 -=2 を -=1に。追加ルール数はルール数通りのため？
		 */
		if(AddRuleNum != 0){//細分化して出来たルールが無い(空のファイルのときはルール数0として流す)
			AddRuleNum -= 1; //細分化して出来たルール数ー１が増加するルール数(１つは元のルールに統合されるため)
		}
		Main.statusView.append("■"+AddRuleFile.getName()+ "を使って" + Main.RuleName[Main.RoopNum-1] +"にプラスされるルール数 : "+AddRuleNum+"\n");
		System.out.println("AddRuleNum = "+AddRuleNum);
		CreateSavingRuleArray(AddRuleNum,Ri); //RuleNumは更新前
		CreateSavingCDistinction(); //CDistinctionの内容を保存
		CreateCDistinction((RuleNum + AddRuleNum)); //CDisitinctionを再生成
		FormatCDistinction((RuleNum + AddRuleNum));
		ReturnCDistinction(); //CDistinctionに保存した内容を返す
		SavingListRuleArray();
		AddRuleArray(AddRuleFile,Ri); //SavingRuleArrayに追加されたルールを書き込んでいく
		SavingListRuleArray();
		RuleNum = RuleNum + AddRuleNum; //RuleNumはここで更新
		ReturnRuleArray(); //SavingRuleArrayの内容をRuleArrayに戻す
		System.out.println("RuleNum = "+RuleNum);
		ListRuleArray();
		WriteRule(RuleArray,Ri); //新しいルールをファイルに保存
	}
	
	//ルールを追加していく作業用のSavingRuleArrayを作成、今までのルールを移していく
	private static void CreateSavingRuleArray(int AddRuleNum,int Ri){
		SavingRuleArray = new BigDecimal[RuleNum+AddRuleNum+1][zokusei+1+2];
		// こっから不要
		System.out.println("RuleNum:"+RuleNum);
		System.out.println("AddRuleNum:"+AddRuleNum);
		System.out.println("SavingRuleArray.length: "+SavingRuleArray.length);
		// ここまで不要
		for(int i=1;i<=RuleNum;i++){//今まであったルールの所はそのままコピー
			for(int j=1;j<=zokusei+2;j++){
				SavingRuleArray[i][j] = RuleArray[i][j];
			}
		}
		for(int copycount=0;copycount<AddRuleNum;copycount++){//追加分は元のルールをコピーしておく
			for(int j=1;j<=zokusei+2;j++){
			SavingRuleArray[RuleNum+copycount+1][j] = RuleArray[Ri][j];
			}
		}
	}
	
	private static void AddRuleArray(File AddRuleFile,int Ri){
		 try{
				FileReader fr = new FileReader(AddRuleFile);
				StreamTokenizer token = new StreamTokenizer(fr);
				token.eolIsSignificant(true);
				String str;
				int i;
				int tmp;
				int Rj=1;
				//不等号判別用に一時的に記憶する配列　
				int a[] = new int[2];
				boolean eof = false;
				i = Ri;
			while(!eof){
				tmp = token.nextToken();
				while((i != Ri) && (i <= RuleNum)){//既に使われているルール番号は飛ばす RuleNumは更新前の値
					i++;
				}
				//if(i>RuleNum) break;
				switch(tmp){//以下ルールの読み取り部分
				case StreamTokenizer.TT_NUMBER:
					break;
					
				case StreamTokenizer.TT_WORD:
				str = token.sval.replaceAll("[0-9]", "");
				if(str.equals("D")){
					Rj = Integer.parseInt(token.sval.replaceAll("[^0-9]", ""));
					tmp = token.nextToken();	//=
					tmp = token.nextToken();	//値まで移動
					SavingRuleArray[i][Rj] = BigDecimal.valueOf(token.nval);
				}
				if(str.equals("C")){
					Rj = Integer.parseInt(token.sval.replaceAll("[^0-9]", ""));
					tmp = token.nextToken();
					a[0] = tmp;		//一つ目の記号
					tmp = token.nextToken();
					if(tmp != StreamTokenizer.TT_NUMBER){
						a[1] = tmp;  //次も記号なら読み込む	
						tmp = token.nextToken();
					}
					InsertCDistinction(a,i,Rj);	//CDistinctionの格納
					CrearArray(a,2);		//記号読み取り用配列のクリア
					SavingRuleArray[i][Rj] = BigDecimal.valueOf(token.nval);
				}
				if(str.equals("Class")){
					tmp = token.nextToken();
					if(token.nval == 1){
						SavingRuleArray[i][zokusei+1] = BigDecimal.valueOf(0);
						SavingRuleArray[i][zokusei+2] = BigDecimal.valueOf(1);
					}
					else{
						SavingRuleArray[i][zokusei+1] = BigDecimal.valueOf(1);
						SavingRuleArray[i][zokusei+2] = BigDecimal.valueOf(0);
					}
				}
				break;
					
				case StreamTokenizer.TT_EOL:		
					i++; //改行でルールNoが変わる
					break;
					
				case StreamTokenizer.TT_EOF:
					eof = true;
					break;
					
				default:	
					break;
			}
		}
		}catch(FileNotFoundException e){
			Main.statusView.append("Error: AddRuleArray(File AddRuleFile,int Ri)内でFileNotFoundExceptionです．\n");
		}catch(IOException e){
			Main.statusView.append("Error: AddRuleArray(File AddRuleFile,int Ri)内でIOExceptionです．\n");
		}
	}
	
	//RuleArrayに作成されたルールを戻す　RuleNumは更新後
	private static void ReturnRuleArray(){
		CreateRuleArray(); //RuleArrayを再生成
		FormatRuleArray();
		for(int i=1;i<=RuleNum;i++){//RuleArrayに追加されたルールをコピー
			for(int j=1;j<=zokusei+2;j++){
				RuleArray[i][j] = SavingRuleArray[i][j];
			}
		}
	}
	
	private static void WriteRule(BigDecimal[][] RuleArray,int Ri){
		try{
			File newfile = new File("c:\\FullAutoReRX\\" + Main.ProjectName + "\\" + Main.RuleName[Main.RoopNum-1] + "\\" + Main.RuleName[Main.RoopNum-1] + ".txt");
			PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter(newfile)));
			for(int i=1;i<=RuleNum;i++){
				pw.print("R" + i + ": ");
				for(int j=1;j<=zokusei;j++){
					if(RuleArray[i][j].compareTo(BigDecimal.valueOf(2)) != 0){
						if(DCArray[j]==0){
							pw.print("D" + j + "=" + RuleArray[i][j].setScale(0) + " ");
						}
						if(DCArray[j]==1){ 
							if(CDistinction[i][j] == 1){
								pw.print("C" + j + ">" + df.format(RuleArray[i][j]) + " ");
							}
							else if(CDistinction[i][j] == 2){
								pw.print("C" + j + ">=" + df.format(RuleArray[i][j]) + " ");
							}
							else if(CDistinction[i][j] == 3){
								pw.print("C" + j + "=" + df.format(RuleArray[i][j]) + " ");
							}
							else if(CDistinction[i][j] == 4){
								pw.print("C" + j + "<=" + df.format(RuleArray[i][j]) + " ");
							}
							else if(CDistinction[i][j] == 5){
								pw.print("C" + j + "<" + df.format(RuleArray[i][j]) + " ");
							}
						}
					}
					if(j==zokusei){
						if(RuleArray[i][zokusei+1].compareTo(BigDecimal.valueOf(0)) == 0){
							pw.println("then Class 1");
						}
						else{
							pw.println("then Class 2");
						}
					}
				}
			}
			pw.close();
		}catch(FileNotFoundException e){
			Main.statusView.append("Error: WriteRule(BigDecimal[][] RuleArray,int Ri)内でFileNotFoundExceptionです．\n");
		}catch(IOException e){
			Main.statusView.append("Error: WriteRule(BigDecimal[][] RuleArray,int Ri)内でIOExceptionです．\n");
		}
	}
	
	private static void SavingListRuleArray(){
		for(int i=1;i<=RuleNum;i++){
			Main.statusView.append("R" + i + ": ");
			for(int j=1;j<=zokusei;j++){
				if(SavingRuleArray[i][j].compareTo(BigDecimal.valueOf(2)) != 0){
					if(DCArray[j]==0){
						Main.statusView.append("D" + j + " = " + SavingRuleArray[i][j].setScale(0) + " ");
					}
					if(DCArray[j]==1){ 
						if(CDistinction[i][j] == 1){
							Main.statusView.append("C" + j + " > " + df.format(SavingRuleArray[i][j]) + " ");
						}
						else if(CDistinction[i][j] == 2){
							Main.statusView.append("C" + j + " >= " + df.format(SavingRuleArray[i][j]) + " ");
						}
						else if(CDistinction[i][j] == 3){
							Main.statusView.append("C" + j + " = " + df.format(SavingRuleArray[i][j]) + " ");
						}
						else if(CDistinction[i][j] == 4){
							Main.statusView.append("C" + j + " <= " + df.format(SavingRuleArray[i][j]) + " ");
						}
						else if(CDistinction[i][j] == 5){
							Main.statusView.append("C" + j + " < " + df.format(SavingRuleArray[i][j]) + " ");
						}
					}
				}
				if(j==zokusei){
					if(SavingRuleArray[i][zokusei+1].compareTo(BigDecimal.valueOf(0)) == 0){
						Main.statusView.append("then Class 1\n");
					}
					else{
						Main.statusView.append("then Class 2\n");
					}
				}
			}
		}
	}
	
}
