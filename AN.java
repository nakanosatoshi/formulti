/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package fullauto;

/**
 *
 * @author Akihiro
 */
/**
 * 人口ニューロンクラス.
 * 結合荷重は前層のニューロン数分を保持
 */
public class AN {

    /** 結合荷重 */
    double[] weight;
    /** 重み付け総和 */
    private double weightedSum;
    /** ニューロンの出力 */
    private double output;
    /** トレーニング対象決定変数 */
    //private boolean[] trainable;
    boolean[] trainable;

    /** 各フィールドを初期化 */
    public AN() {
        weight = null;
        weightedSum = 0.0;
        output = 0.0;

    }

    /**
     * 各フィールドを初期化後、numの値分の結合荷重を-0.1～0.1に初期化
     * num 入力数
     */
    public AN(int num) {
        super();
        weight = new double[num];
        trainable = new boolean[num];
        for (int i = 0; i < num; i++) {
        	//Math.random():0.000~0.999...
        	//0~0.199...
        	//-1~0.099
        	//if(-0.1)-0.1~0.9999
            //weight[i] = Math.random() / 10.0 * 2.0 - 1.0;
            //weight[i] = Math.random() * 2.0 - 1.0;
            weight[i] = Math.random() / 10.0 * 2 - 0.1;
            //結果同じ
            trainable[i] = true;
        }

    }

    public AN(double d) {
        output = d;
    }

    /**
     * 結合荷重の取得
     * @return 結合荷重
     */
    public double[] getWeight() {
        return weight;
    }

    public void setWeight(int i, double w) {
        weight[i] = w;
    }
    public void delWeight(int num) {
        weight[num] = 0.0;
        trainable[num] = false;
    }

    /**
     * 重み付け総和の取得
     * @return 重み付け総和
     */
    public double getWeightedSum() {
        return weightedSum;
    }

    /**
     * 出力値を取得
     * @return 出力値
     */
    public double getOutput() {
        return output;
    }

    /**
     * 入力をそのままニューロンの出力に設定
     * @param input 入力
     */
    public void forward(double input) {
        output = input;
    }

    /**
     * 人口ニューロンの配列を受け取り，重み付け総和を求めsigmoid()メソッドによる
     * 非線形変換を行う．変換後の値はこのニューロンの出力となる．
     *
     * @param input AN[] の出力
     * @see #sigmoid(double)
     */
    public void forward(AN[] input) {
        weightedSum = 0.0;
        for (int i = 0; i < input.length; i++) {
            weightedSum += input[i].getOutput() * weight[i];
        }
        output = sigmoid(weightedSum);
    }
    public void forward(AN[][] input,int num) {
        weightedSum = 0.0;
    	for (int i = 0; i < input[num].length; i++) {
        	weightedSum += input[num][i].getOutput() * weight[i];
        }
        output = sigmoid(weightedSum);
    }
    public boolean isTrainable(int num) {
        return trainable[num];
    }

    public void setTrainable(int i, boolean b) {
        trainable[i] = b;
    }
    /**
     * 引数をシグモイド関数により変換する
     * @param s 入力
     * @return シグモイド関数により変換された値
     */
    private double sigmoid(double s) {
        return (1.0 / (1.0 + Math.exp(-s)));
    }
}
