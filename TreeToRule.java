package fullauto;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StreamTokenizer;
import java.math.BigDecimal;
import java.text.DecimalFormat;

import javax.swing.JFileChooser;

/*
 * J48によって取得した決定木を可読できるルール表記に変換するクラスです
 * エラーが出たら、　linenum[]とzokuseiname[]の配列数を増やしてみてください
 */
public class TreeToRule {
	public static void TreeReading(int cnum, String[] cname){
		//*J48の葉の数
		int LINE = 200;
		int[] dcarray = new int[LINE];		//その行の属性が離散属性なら１，連続属性なら２を代入
		int[] znumber = new int[LINE];		//その行の属性番号を保存
		int[] zdistinction = new int[LINE];	//不等号を保存 >:1 ≧:2 =:3 ≦:4 <:5
		BigDecimal[] zvalue = new BigDecimal[LINE];		//値を保存
		//小数10桁表示
	    DecimalFormat df = new DecimalFormat("#.##########");
		int[] linenum = new int[LINE];		//縦棒数を保存
		int[] ruleclass = new int[LINE];	//各ルールのクラスを数字で保存.0は使わない. cname[0]:Class 1 = ruleclass[]は1, cname[1]:Class 2 = ruleclass[]は2
		int rnum = 1; //ルール番号
		int ruleline = 0; //ルール作成時に使用する。ライン数を保存しておき、どの行の命題を利用するかを判別するのに用いる。
		int[] zokusei = new int[LINE]; //ルール作成時に使用する。そのルールの命題として使われる行番号を保存していく。
		int box = 0; //ルール作成時に使用する。zokusei[box]のようにzokusei[]の箱番号として使用する。
		
		try{
			File treefile = new File("c:\\FullAutoReRX\\" + Main.ProjectName + "\\" + Main.RuleName[Main.RoopNum-1] + "\\"+ Main.RuleName[Main.RoopNum-1] +"Tree.txt");
			FileReader filereader = new FileReader(treefile);
			File treetorule = new File("c:\\FullAutoReRX\\"+Main.ProjectName+"\\" + Main.RuleName[Main.RoopNum-1] + "\\"+Main.RuleName[Main.RoopNum-1]+".txt");
			int rn = 1; //rnの初期値は書き換えないこと。決定木からルール表記に書き換える時のルール番号記入用。（最終的なルール数が入る）
			int indent = 0; //決定木の縦線の数の差を比べるのに使用
			try{
				if (treetorule.createNewFile()){
					Main.statusView.append(Main.RuleName[Main.RoopNum-1]+".txtの作成に成功しました\n");
				}else{
					Main.statusView.append(Main.RuleName[Main.RoopNum-1]+".txtはすでに存在しているため上書きします\n");
				}
			}catch(IOException e){
				System.out.println(e);
			}
			//ファイルに書き込むPrintWriterクラス
			PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter(treetorule)));
			
			// StreamTokenizerオブジェクトを作成.取得したトークンは数字ならnval, 文字列ならsval, その他ならttypeにデータが入る
			// 【注意】 改行はメモ帳上では行われておらず(\nで判別はできるが)、TT_EOLは発動しない
			StreamTokenizer token = new StreamTokenizer(filereader);
			int tmp;
			String strdc, strnum;
			
			tmp = token.nextToken(); // J48
			tmp = token.nextToken(); // pruned
			tmp = token.nextToken(); // tree
			tmp = token.nextToken(); // -
			// '-'の終わりまで回す. -のttype:45
			while(token.ttype == 45){
				tmp = token.nextToken();
			}
			
			if(token.ttype == 58){//すぐにコロンが来る＝ルールが存在しない場合ファイルは空のまま終了する
				pw.close();
				return;
			}
			
			/*
			 * 決定木のNumber of Leaves~まで調べて書き換える。Numberまで来たら終了。
			 *  【注意】 クラス属性名などに"Number"を使用していたらソースの状体によっては正しく動かないです
			 *  "|"のttype:124
			 */
			int i = 0; //行数
			
			while(tmp != StreamTokenizer.TT_EOF){
				linenum[i] = 0;
				// '|'の終わりまで回す
				while(token.ttype == 124){
					linenum[i]++;
					tmp = token.nextToken();
				}
				if(tmp == StreamTokenizer.TT_WORD){
					//属性名の判別
					strdc = token.sval.replaceAll("[0-9]","");
					if(strdc.equals("D")){
						dcarray[i] = 1;	//離散属性として1を代入しておく
						znumber[i] = Integer.parseInt(token.sval.replaceAll("[^0-9]", ""));//属性番号を代入
						tmp = token.nextToken(); // 不等号へ移動
						if(tmp == '<'){
							tmp = token.nextToken(); // '=' に移動
							zdistinction[i] = 3;
							tmp = token.nextToken();//属性値へ移動
							zvalue[i] = BigDecimal.valueOf(token.nval);
						}
						else if(tmp == '>'){
							zdistinction[i] = 1;
							tmp = token.nextToken();
							zvalue[i] = BigDecimal.valueOf(token.nval);
						}
						else{
							System.out.println("ERROR1: TreeToRuleクラス内でエラーが発生しています。ソースと決定木テキストファイルを確認してください。");
						}
						
						tmp = token.nextToken(); // コロンか次の行へ移動
						
						if(token.ttype == 58){ // コロンの場合
							tmp = token.nextToken(); // クラス名に移動
							for(int number=0; number < cnum; number++){ //class数分，どのクラスと同じか比較して番号にしてruleclass[]に代入
								if(token.sval.equals(cname[number])){
									ruleclass[rnum] = number + 1;
								}
							}
							/*
							 * 次の行まで移動させる
							 * class名の後ろが，(x)か(x/y)かで読み込み方が違う
							 * (x)の場合のtmp推移　(:40， x:-2， ):41
							 * (x/y)の場合のtmp推移　(:40, x/y):-2 
							 */
							for(int j = 0; j < 3; j++){
								tmp = token.nextToken();
							}
							// (x)だった場合もう1度nextTokenして次の行頭へ移動させる
							if(tmp == 41){
								tmp = token.nextToken();
							}
							
							/*
							 * ルールの表示
							 */
							pw.print("R" + rnum + ": ");
							ruleline = linenum[i];
							box = 0;
							zokusei[box] = i;
							for(int k = i; k >= 0; k--){
								if(linenum[k] < ruleline){
									ruleline = linenum[k]; // rulelineを更新(木の幹に近づける)
									box++;
									zokusei[box] = k;
								}
							}
							for(int a = box; a >= 0; a--){
								if(dcarray[zokusei[a]] == 1){
									pw.print("D");
									pw.print(znumber[zokusei[a]]);
									pw.print("=");
									if(zdistinction[zokusei[a]] == 3){
										pw.print("0 ");
									}
									else{
										pw.print("1 ");
									}
								}
								else if(dcarray[zokusei[a]] == 2){
									pw.print("C");
									pw.print(znumber[zokusei[a]]);
									switch(zdistinction[zokusei[a]]){
									case 1:
										pw.print(">");
										break;
									case 2:
										pw.print(">=");
										break;
									case 3:
										pw.print("=");
										break;
									case 4:
										pw.print("<=");
										break;
									case 5:
										pw.print("<");
										break;
									}
									pw.print(df.format(zvalue[zokusei[a]])+" ");
								}
								//if(a != 0){
								//	pw.print(" and ");
								//}
							}
							//RuleのClass記述
							pw.println("then Class " + ruleclass[rnum]);
							rnum++; //ルール番号を次に移動
						}
					}
					else if(strdc.equals("C")){
						dcarray[i] = 2;	//離散属性として1を代入しておく
						znumber[i] = Integer.parseInt(token.sval.replaceAll("[^0-9]", ""));//属性番号を代入
						tmp = token.nextToken(); // 不等号へ移動
						if(tmp == '>'){
							tmp = token.nextToken(); // '=' か属性値へ移動
							zdistinction[i] = 1;
							if(tmp == '='){
								zdistinction[i] = 2;
								tmp = token.nextToken(); // 属性値へ移動
							}
							zvalue[i] = BigDecimal.valueOf(token.nval);
						}
						else if(tmp == '='){
							zdistinction[i] = 3;
							tmp = token.nextToken(); // 属性値へ移動
							zvalue[i] = BigDecimal.valueOf(token.nval);
						}
						else if(tmp == '<'){
							zdistinction[i] = 5;
							tmp = token.nextToken(); // '=' か属性値へ移動
							if(tmp == '='){
								zdistinction[i] = 4;
								tmp = token.nextToken(); //属性値へ移動
							}
							zvalue[i] = BigDecimal.valueOf(token.nval);
						}
						else{
							System.out.println("ERROR:2 TreeToRuleクラス内でエラーが発生しています。ソースと決定木テキストファイルを確認してください。");
						}
						
						tmp = token.nextToken(); // コロンか次の行へ移動
						
						if(token.ttype == 58){ // コロンの場合
							tmp = token.nextToken(); // クラス名に移動
							for(int number=0; number < cnum; number++){ //class数分，どのクラスと同じか比較して番号にしてruleclass[]に代入
								if(token.sval.equals(cname[number])){
									ruleclass[rnum] = number + 1;
								}
							}
							/*
							 * 次の行まで移動させる
							 * class名の後ろが，(x)か(x/y)かで読み込み方が違う
							 * (x)の場合のtmp推移　(:40， x:-2， ):41
							 * (x/y)の場合のtmp推移　(:40, x/y):-2 
							 */
							for(int j = 0; j < 3; j++){
								tmp = token.nextToken();
							}
							// (x)だった場合もう1度nextTokenして次の行頭へ移動させる
							if(tmp == 41){
								tmp = token.nextToken();
							}
							
							/*
							 * ルールの表示
							 */
							pw.print("R" + rnum + ": ");
							ruleline = linenum[i];
							box = 0;
							zokusei[box] = i;
							for(int k = i; k >= 0; k--){
								if(linenum[k] < ruleline){
									ruleline = linenum[k]; // rulelineを更新(木の幹に近づける)
									box++;
									zokusei[box] = k;
								}
							}
							for(int a = box; a >= 0; a--){
								if(dcarray[zokusei[a]] == 1){
									pw.print("D");
									pw.print(znumber[zokusei[a]]);
									pw.print("=");
									if(zdistinction[zokusei[a]]==3){
										pw.print("0 ");
									}
									else{
										pw.print("1 ");
									}
								}
								else if(dcarray[zokusei[a]] == 2){
									pw.print("C");
									pw.print(znumber[zokusei[a]]);
									switch(zdistinction[zokusei[a]]){
									case 1:
										pw.print(">");
										break;
									case 2:
										pw.print(">=");
										break;
									case 3:
										pw.print("=");
										break;
									case 4:
										pw.print("<=");
										break;
									case 5:
										pw.print("<");
										break;
									}
									pw.print(df.format(zvalue[zokusei[a]])+" ");
								}
								//if(a != 0){
								//	pw.print(" and ");
								//}
							}
							//RuleのClass記述
							pw.println("then Class " + ruleclass[rnum]);
							rnum++; //ルール番号を次に移動
						}
					}
					//Number of~　まで来たらbreak
					else{
						break;
					}
					
					i++; // 行番号を次の行に変更
				}
			}
			//PrintWriterの終了
			pw.close();
		}catch(FileNotFoundException e){
			System.out.println("Error: FileNotFoundExceptionです．");
		}catch(IOException e){
			System.out.println("Error: IOExceptionです．");
		}
	}
	
	public static void RecursiveTreeReading(int cnum, String[] cname, int Ri){
		int LINE = 100;
		int[] dcarray = new int[LINE];		//その行の属性が離散属性なら１，連続属性なら２を代入
		int[] znumber = new int[LINE];		//その行の属性番号を保存
		int[] zdistinction = new int[LINE];	//不等号を保存 >:1 ≧:2 =:3 ≦:4 <:5
		BigDecimal[] zvalue = new BigDecimal[LINE];		//値を保存
		//小数10桁表示
	    DecimalFormat df = new DecimalFormat("#.##########");
		int[] linenum = new int[LINE];		//縦棒数を保存
		int[] ruleclass = new int[LINE];	//各ルールのクラスを数字で保存.0は使わない. cname[0]:Class 1 = ruleclass[]は1, cname[1]:Class 2 = ruleclass[]は2
		int rnum = 1; //ルール番号
		int ruleline = 0; //ルール作成時に使用する。ライン数を保存しておき、どの行の命題を利用するかを判別するのに用いる。
		int[] zokusei = new int[LINE]; //ルール作成時に使用する。そのルールの命題として使われる行番号を保存していく。
		int box = 0; //ルール作成時に使用する。zokusei[box]のようにzokusei[]の箱番号として使用する。
		
		try{
			File treefile = new File("c:\\FullAutoReRX\\" + Main.ProjectName + "\\" + Main.RuleName[Main.RoopNum-1] + "\\"+ Main.RuleName[Main.RoopNum-1] +"TreeRule" + Ri + ".txt");
			FileReader filereader = new FileReader(treefile);
			File treetorule = new File("c:\\FullAutoReRX\\" + Main.ProjectName + "\\" + Main.RuleName[Main.RoopNum-1] + "\\"+Main.RuleName[Main.RoopNum-1]+Ri+".Recursive.txt");
			int rn = 1; //rnの初期値は書き換えないこと。決定木からルール表記に書き換える時のルール番号記入用。（最終的なルール数が入る）
			int indent = 0; //決定木の縦線の数の差を比べるのに使用
			try{
				if (treetorule.createNewFile()){
					System.out.println(Main.RuleName[Main.RoopNum-1]+Ri+".Recursive.txtの作成に成功しました");
				}else{
					System.out.println(Main.RuleName[Main.RoopNum-1]+Ri+".Recursive.txtはすでに存在しているため上書きします");
				}
			}catch(IOException e){
				System.out.println(e);
			}
			//ファイルに書き込むPrintWriterクラス
			PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter(treetorule)));
			
			// StreamTokenizerオブジェクトを作成.取得したトークンは数字ならnval, 文字列ならsval, その他ならttypeにデータが入る
			// 【注意】 改行はメモ帳上では行われておらず(\nで判別はできるが)、TT_EOLは発動しない
			StreamTokenizer token = new StreamTokenizer(filereader);
			int tmp;
			String strdc, strnum;
			
			tmp = token.nextToken(); // J48
			tmp = token.nextToken(); // pruned
			tmp = token.nextToken(); // tree
			tmp = token.nextToken(); // -
			// '-'の終わりまで回す. -のttype:45
			while(token.ttype == 45){
				tmp = token.nextToken();
			}
			
			if(token.ttype == 58){//すぐにコロンが来る＝ルールが存在しない場合ファイルは空のまま終了する
				pw.close();
				return;
			}
			
			/*
			 * 決定木のNumber of Leaves~まで調べて書き換える。Numberまで来たら終了。
			 *  【注意】 クラス属性名などに"Number"を使用していたらソースの状体によっては正しく動かないです
			 *  "|"のttype:124
			 */
			int i = 0; //行数
			
			while(tmp != StreamTokenizer.TT_EOF){
				linenum[i] = 0;
				// '|'の終わりまで回す
				while(token.ttype == 124){
					linenum[i]++;
					tmp = token.nextToken();
				}
				if(tmp == StreamTokenizer.TT_WORD){
					//属性名の判別
					strdc = token.sval.replaceAll("[0-9]","");
					if(strdc.equals("D")){
						dcarray[i] = 1;	//離散属性として1を代入しておく
						znumber[i] = Integer.parseInt(token.sval.replaceAll("[^0-9]", ""));//属性番号を代入
						tmp = token.nextToken(); // 不等号へ移動
						if(tmp == '<'){
							tmp = token.nextToken(); // '=' に移動
							zdistinction[i] = 3;
							tmp = token.nextToken();//属性値へ移動
							zvalue[i] = BigDecimal.valueOf(token.nval);
						}
						else if(tmp == '>'){
							zdistinction[i] = 1;
							tmp = token.nextToken();
							zvalue[i] = BigDecimal.valueOf(token.nval);
						}
						else{
							System.out.println("ERROR1: TreeToRuleクラス内でエラーが発生しています。ソースと決定木テキストファイルを確認してください。");
						}
						
						tmp = token.nextToken(); // コロンか次の行へ移動
						
						if(token.ttype == 58){ // コロンの場合
							tmp = token.nextToken(); // クラス名に移動
							for(int number=0; number < cnum; number++){ //class数分，どのクラスと同じか比較して番号にしてruleclass[]に代入
								if(token.sval.equals(cname[number])){
									ruleclass[rnum] = number + 1;
								}
							}
							/*
							 * 次の行まで移動させる
							 * class名の後ろが，(x)か(x/y)かで読み込み方が違う
							 * (x)の場合のtmp推移　(:40， x:-2， ):41
							 * (x/y)の場合のtmp推移　(:40, x/y):-2 
							 */
							for(int j = 0; j < 3; j++){
								tmp = token.nextToken();
							}
							// (x)だった場合もう1度nextTokenして次の行頭へ移動させる
							if(tmp == 41){
								tmp = token.nextToken();
							}
							
							/*
							 * ルールの表示
							 */
							pw.print("R" + rnum + ": ");
							ruleline = linenum[i];
							box = 0;
							zokusei[box] = i;
							for(int k = i; k >= 0; k--){
								if(linenum[k] < ruleline){
									ruleline = linenum[k]; // rulelineを更新(木の幹に近づける)
									box++;
									zokusei[box] = k;
								}
							}
							for(int a = box; a >= 0; a--){
								if(dcarray[zokusei[a]] == 1){
									pw.print("D");
									pw.print(znumber[zokusei[a]]);
									pw.print("=");
									if(zdistinction[zokusei[a]] == 3){
										pw.print("0 ");
									}
									else{
										pw.print("1 ");
									}
								}
								else if(dcarray[zokusei[a]] == 2){
									pw.print("C");
									pw.print(znumber[zokusei[a]]);
									switch(zdistinction[zokusei[a]]){
									case 1:
										pw.print(">");
										break;
									case 2:
										pw.print(">=");
										break;
									case 3:
										pw.print("=");
										break;
									case 4:
										pw.print("<=");
										break;
									case 5:
										pw.print("<");
										break;
									}
									pw.print(df.format(zvalue[zokusei[a]])+" ");
								}
								//if(a != 0){
								//	pw.print(" and ");
								//}
							}
							//RuleのClass記述
							pw.println("then Class " + ruleclass[rnum]);
							rnum++; //ルール番号を次に移動
						}
					}
					else if(strdc.equals("C")){
						dcarray[i] = 2;	//離散属性として1を代入しておく
						znumber[i] = Integer.parseInt(token.sval.replaceAll("[^0-9]", ""));//属性番号を代入
						tmp = token.nextToken(); // 不等号へ移動
						if(tmp == '>'){
							tmp = token.nextToken(); // '=' か属性値へ移動
							zdistinction[i] = 1;
							if(tmp == '='){
								zdistinction[i] = 2;
								tmp = token.nextToken(); // 属性値へ移動
							}
							zvalue[i] = BigDecimal.valueOf(token.nval);
						}
						else if(tmp == '='){
							zdistinction[i] = 3;
							tmp = token.nextToken(); // 属性値へ移動
							zvalue[i] = BigDecimal.valueOf(token.nval);
						}
						else if(tmp == '<'){
							zdistinction[i] = 5;
							tmp = token.nextToken(); // '=' か属性値へ移動
							if(tmp == '='){
								zdistinction[i] = 4;
								tmp = token.nextToken(); //属性値へ移動
							}
							zvalue[i] = BigDecimal.valueOf(token.nval);
						}
						else{
							System.out.println("ERROR:2 TreeToRuleクラス内でエラーが発生しています。ソースと決定木テキストファイルを確認してください。");
						}
						
						tmp = token.nextToken(); // コロンか次の行へ移動
						
						if(token.ttype == 58){ // コロンの場合
							tmp = token.nextToken(); // クラス名に移動
							for(int number=0; number < cnum; number++){ //class数分，どのクラスと同じか比較して番号にしてruleclass[]に代入
								if(token.sval.equals(cname[number])){
									ruleclass[rnum] = number + 1;
								}
							}
							/*
							 * 次の行まで移動させる
							 * class名の後ろが，(x)か(x/y)かで読み込み方が違う
							 * (x)の場合のtmp推移　(:40， x:-2， ):41
							 * (x/y)の場合のtmp推移　(:40, x/y):-2 
							 */
							for(int j = 0; j < 3; j++){
								tmp = token.nextToken();
							}
							// (x)だった場合もう1度nextTokenして次の行頭へ移動させる
							if(tmp == 41){
								tmp = token.nextToken();
							}
							
							/*
							 * ルールの表示
							 */
							pw.print("R" + rnum + ": ");
							ruleline = linenum[i];
							box = 0;
							zokusei[box] = i;
							for(int k = i; k >= 0; k--){
								if(linenum[k] < ruleline){
									ruleline = linenum[k]; // rulelineを更新(木の幹に近づける)
									box++;
									zokusei[box] = k;
								}
							}
							for(int a = box; a >= 0; a--){
								if(dcarray[zokusei[a]] == 1){
									pw.print("D");
									pw.print(znumber[zokusei[a]]);
									pw.print("=");
									if(zdistinction[zokusei[a]]==3){
										pw.print("0 ");
									}
									else{
										pw.print("1 ");
									}
								}
								else if(dcarray[zokusei[a]] == 2){
									pw.print("C");
									pw.print(znumber[zokusei[a]]);
									switch(zdistinction[zokusei[a]]){
									case 1:
										pw.print(">");
										break;
									case 2:
										pw.print(">=");
										break;
									case 3:
										pw.print("=");
										break;
									case 4:
										pw.print("<=");
										break;
									case 5:
										pw.print("<");
										break;
									}
									pw.print(df.format(zvalue[zokusei[a]])+" ");
								}
								//if(a != 0){
								//	pw.print(" and ");
								//}
							}
							//RuleのClass記述
							pw.println("then Class " + ruleclass[rnum]);
							rnum++; //ルール番号を次に移動
						}
					}
					//Number of~　まで来たらbreak
					else{
						break;
					}
					
					i++; // 行番号を次の行に変更
				}
			}
			//PrintWriterの終了
			pw.close();
		}catch(FileNotFoundException e){
			System.out.println("Error: FileNotFoundExceptionです．");
		}catch(IOException e){
			System.out.println("Error: IOExceptionです．");
		}
	}
}
