/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package autorerxxx;

/**
 *
 * @author Atsushi
 */
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JFileChooser;
import javax.swing.SwingWorker;

public class Main extends javax.swing.JFrame {

    protected FileOperationForNN trainingSample;
    protected FileOperationForNN testSample;
    private FileOperationForNN correctSample;
    AN[] inputLayer;
    protected AN[] hiddenLayer;
    protected AN[] outputLayer;
    protected double maxError;
    protected int numOfAccordance;
    protected boolean stopStatus = false;
    protected double rate;
    protected int numOfCut = 0;
    protected boolean[][] tempTrainableItoH;
    protected boolean[][] tempTrainableHtoO;
    protected double[][] tempWeightItoH;
    protected double[][] tempWeightHtoO;
    private FileOperationForNN cValidationSample;
    private FileOperationForNN allSample;
    protected boolean[][] beforePruningTrainableItoH;
    protected boolean[][] beforePruningTrainableHtoO;
    protected double[][] beforePruningWeightItoH;
    protected double[][] beforePruningWeightHtoO;
    protected boolean[][] networkTable;
    private ArrayList<Object>[] list;
    //protected ArrayList list = new ArrayList();
    /** Creates new form Main */
    public Main() {
        initComponents();
    }

    private void writeOutNameFile() {
        boolean flag = false; // 入力が省けるかどうかのフラグの作成

        try {
            FileOutputStream nameFileStream = new FileOutputStream("3class.names");

            OutputStreamWriter nameFileOSW = new OutputStreamWriter(nameFileStream);

            BufferedWriter nameFileBW = new BufferedWriter(nameFileOSW);

            nameFileBW.write("Class 1, Class 2, Class 3.");
            nameFileBW.newLine();
            nameFileBW.newLine();

            for (int i = 0; i < inputLayer.length - 1; i++) {
                for (int j = 0; j < hiddenLayer.length - 1; j++) {
                    if (hiddenLayer[j].isTrainable(i)) {
                        testSample.setWritable(i, true);
                    }
                }
                if (testSample.getWritable(i)) {
                    nameFileBW.write(testSample.getLabel(i) + ": " + testSample.getType(i) + ".");
                    nameFileBW.newLine();
                }
            }

            nameFileBW.close();

        } catch (IOException e) {
            System.err.println(e);
        }


    }

    private void writeOutArffFile(ArrayList<Object>[] list){

        FileOutputStream arffFileStream = null;

        try {
            arffFileStream = new FileOutputStream("3class.arff");

            OutputStreamWriter arffFileOSW = new OutputStreamWriter(arffFileStream);

            BufferedWriter arffFileBW = new BufferedWriter(arffFileOSW);

            //データの種類を書き出す
            arffFileBW.write("@relation 3class");
            arffFileBW.newLine();
            arffFileBW.newLine();

            //属性を書き出す
            //@attribute D7 real  D7: discrete 2.
            System.out.println("hiddenLayer: " + hiddenLayer.length );
            for (int i = 0; i < inputLayer.length - 1; i++) {
                for (int j = 0; j < hiddenLayer.length - 1; j++) {
                    if (hiddenLayer[j].isTrainable(i)) {
                        testSample.setWritable(i, true);
                        System.out.println("i: " + i +", trainable: " + hiddenLayer[j].isTrainable(i));
                    }
                    else if(!hiddenLayer[j].isTrainable(i)){
                        testSample.setWritable(i, false);
                        System.out.println("i: " + i +", trainable: " + hiddenLayer[j].isTrainable(i));
                    }
                }
                if (testSample.getWritable(i)) {
                    arffFileBW.write("@attribute " + testSample.getLabel(i) + " real");
                    arffFileBW.newLine();
                }
            }

            //クラスを書く
            arffFileBW.write("@attribute Play {yes, no, 3rd}");
            arffFileBW.newLine();
            arffFileBW.newLine();

            arffFileBW.write("@data");
            arffFileBW.newLine();

            for (int i = 0; i < list.length; i++) {
                for (int j = 0; j < list[i].size(); j++) {
                    if (String.valueOf(list[i].get(j)).equals("0.0")) {
                        arffFileBW.write(String.valueOf("0, "));
                    } else if (String.valueOf(list[i].get(j)).equals("1.0")) {
                        arffFileBW.write(String.valueOf("1, "));
                    } else if (String.valueOf(list[i].get(j)).equals("Class 1")){
                        arffFileBW.write("yes");
                    }else if (String.valueOf(list[i].get(j)).equals("Class 2")){
                        arffFileBW.write("no");
                    }else if (String.valueOf(list[i].get(j)).equals("Class 3")){
                        arffFileBW.write("3rd");
                    }else {
                        arffFileBW.write(String.valueOf(list[i].get(j)) + ", ");
                    }

                }

                arffFileBW.newLine();
            }

            arffFileBW.close();
        } catch (IOException e) {
            System.err.println(e);
        } finally {
            try {
                arffFileStream.close();
            } catch (IOException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    private void writeOutWeightFile(){

        boolean flag = false; // 入力が省けるかどうかのフラグの作成

        try {
            FileOutputStream weightFileStream = new FileOutputStream("weight.txt");

            OutputStreamWriter weightFileOSW = new OutputStreamWriter(weightFileStream);

            BufferedWriter weightFileBW = new BufferedWriter(weightFileOSW);

            for (int i = 0; i < inputLayer.length - 1; i++) {
                for (int j = 0; j < hiddenLayer.length - 1; j++) {
                    if (hiddenLayer[j].isTrainable(i)) {
                        testSample.setWritable(i, true);
                    }
                }
                if (testSample.getWritable(i)) {
                    weightFileBW.write(testSample.getLabel(i) + ": " + Double.toString(inputLayer[i].getWeight()[i]));
                    weightFileBW.newLine();
                }
            }

            weightFileBW.close();

        } catch (IOException e) {
            System.err.println(e);
        }


    }

    private void writeOutDataFile(ArrayList<Object>[] list) {
        FileOutputStream dataFileStream = null;
        try {
            dataFileStream = new FileOutputStream("3class.data");
            OutputStreamWriter dataFileOSW = new OutputStreamWriter(dataFileStream);
            BufferedWriter dataFileBW = new BufferedWriter(dataFileOSW);

            for (int i = 0; i < list.length; i++) {
                for (int j = 0; j < list[i].size(); j++) {
                    if (String.valueOf(list[i].get(j)).equals("0.0")) {
                        dataFileBW.write(String.valueOf("0, "));
                    } else if (String.valueOf(list[i].get(j)).equals("1.0")) {
                        dataFileBW.write(String.valueOf("1, "));
                    }  else if (String.valueOf(list[i].get(j)).equals("Class 1")){
                        dataFileBW.write("Class 1.");
                    }   else if (String.valueOf(list[i].get(j)).equals("Class 2")){
                        dataFileBW.write("Class 2.");
                    }   else if (String.valueOf(list[i].get(j)).equals("Class 3")){
                        dataFileBW.write("Class 3.");
                    }
                    else {
                        dataFileBW.write(String.valueOf(list[i].get(j) + ", "));
                    }

                }


//                if (String.valueOf(list[i].get(list[i].size() - 1)).equals("0.0")) {
//                    dataFileBW.write(String.valueOf("0"));
//                } else if (String.valueOf(list[i].get(list[i].size() - 1)).equals("1.0")) {
//                    dataFileBW.write(String.valueOf("1"));
//                } else {
//                    dataFileBW.write(String.valueOf(list[i].get(list[i].size() - 1)));
//                }
                dataFileBW.newLine();
            }

            dataFileBW.close();
        } catch (IOException e) {
            System.err.println(e);
        } finally {
            try {
                dataFileStream.close();
            } catch (IOException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    private void crossValidate() {
        final int numOfHiddenUnits = Integer.parseInt(numOfHiddenUnitsTF.getText());
        final double epsilon = Double.parseDouble(epsilonTF.getText());
        int loopMax;

        //誤差
        double error = 0.0;



        double yBack[], hBack[], netInput;
        double sumOfErrors = 0.0;
        for (loopMax = 0; loopMax < Integer.parseInt(numOfMaxLoopTF.getText()); loopMax++) {  //学習上限回数までループ
            if (stopStatus) {
                break;
            }
            maxError = -1.0;
            sumOfErrors = 0.0;
            yBack = new double[outputLayer.length];
            hBack = new double[numOfHiddenUnits + 1];

            for (int iSample = 0; iSample < cValidationSample.getNumOfSamples(); iSample++) {    //全サンプル数分のループ

                error = 0.0;
                for (int j = 0; j < cValidationSample.getNumOfInputs(); j++) {
                    inputLayer[j].forward(cValidationSample.getInput()[iSample][j]);
                }
                inputLayer[cValidationSample.getNumOfInputs()].forward(1.0);

                for (int j = 0; j < numOfHiddenUnits; j++) {
                    hiddenLayer[j].forward(inputLayer);
                }
                hiddenLayer[numOfHiddenUnits].forward(1.0);

                for (int j = 0; j < cValidationSample.getNumOfOutputs(); j++) {
                    outputLayer[j].forward(hiddenLayer);
                    error += Math.pow(cValidationSample.getTarget()[iSample][j] - outputLayer[j].getOutput(), 2.0);
                }

                error = error / 2;
                if (error > maxError) {
                    maxError = error;
                }
                sumOfErrors += error;
                //以下、誤差逆伝播セクション

                for (int j = 0; j < cValidationSample.getNumOfOutputs(); j++) {
                    yBack[j] = (outputLayer[j].getOutput() - cValidationSample.getTarget()[iSample][j]) * (1.0 - outputLayer[j].getOutput()) * outputLayer[j].getOutput() + Double.parseDouble(momentum.getText()) * yBack[j];
                }
                for (int i = 0; i < numOfHiddenUnits + 1; i++) {
                    netInput = 0.0;
                    for (int j = 0; j < outputLayer.length; j++) {
                        netInput += outputLayer[j].getWeight()[i] * yBack[j];
                    }
                    hBack[i] = netInput * (1.0 - hiddenLayer[i].getOutput()) * hiddenLayer[i].getOutput() + Double.parseDouble(momentum.getText()) * hBack[i];
                }
                for (int i = 0; i < cValidationSample.getNumOfInputs() + 1; i++) {
                    for (int j = 0; j < numOfHiddenUnits; j++) {
                        if (hiddenLayer[j].isTrainable(i)) {
                            hiddenLayer[j].setWeight(i, hiddenLayer[j].getWeight()[i] - epsilon * inputLayer[i].getOutput() * hBack[j]);
                        }
                    }
                }
                for (int i = 0; i < numOfHiddenUnits + 1; i++) {
                    for (int j = 0; j < cValidationSample.getNumOfOutputs(); j++) {
                        if (outputLayer[j].isTrainable(i)) {
                            outputLayer[j].setWeight(i, outputLayer[j].getWeight()[i] - epsilon * hiddenLayer[i].getOutput() * yBack[j]);
                        }
                    }
                }
            }
            //エラーが閾値以下になったら学習を終了する
            if ((Double.parseDouble(errorThresholdTF.getText()) > maxError)) {
                break;
            }


        }

        double averageError = sumOfErrors / cValidationSample.getNumOfSamples();
        statusView.append("相互検証完了\n");
        statusView.append("\t学習回数 : " + String.valueOf(loopMax) + "\n");
        statusView.append("\t最大誤差 : " + String.valueOf(maxError) + "\n");
        statusView.append("\t平均誤差 : " + String.valueOf(averageError) + "\n");
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        statusView = new javax.swing.JTextArea();
        viewFileName1 = new javax.swing.JTextField();
        viewFileName2 = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        numOfHiddenUnitsTF = new javax.swing.JTextField();
        errorThresholdTF = new javax.swing.JTextField();
        numOfMaxLoopTF = new javax.swing.JTextField();
        epsilonTF = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        doNormalizationCB = new javax.swing.JCheckBox();
        startLearning = new javax.swing.JButton();
        startRecog = new javax.swing.JButton();
        clearBT = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        zerTF = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        momentum = new javax.swing.JTextField();
        pruningBt = new javax.swing.JButton();
        stopProcedure = new javax.swing.JButton();
        pruningThresholdTF = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        endPruningTF = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        viewCValidationFileName = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        crossValidationBt = new javax.swing.JButton();
        recogAllDataBt = new javax.swing.JButton();
        allDataTF = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        recallBeforePruningBt = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        openAllDataMI = new javax.swing.JMenuItem();
        openTrainingSampleMI = new javax.swing.JMenuItem();
        jMenuItem1 = new javax.swing.JMenuItem();
        openTestSampleMI = new javax.swing.JMenuItem();
        terminateProgramMI = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setText("ステータス表示");

        statusView.setColumns(20);
        statusView.setRows(5);
        jScrollPane1.setViewportView(statusView);

        viewFileName1.setEditable(false);

        viewFileName2.setEditable(false);

        jLabel2.setText("トレーニングサンプル");

        jLabel3.setText("テストサンプル");

        numOfHiddenUnitsTF.setText("1");

        errorThresholdTF.setText("0.01");

        numOfMaxLoopTF.setText("1000");

        epsilonTF.setText("0.2");

        jLabel4.setText("中間層の数");

        jLabel5.setText("誤差閾値");

        jLabel6.setText("学習回数");

        jLabel7.setText("重みの修正");

        doNormalizationCB.setText("正規化");

        startLearning.setText("学習を行う");
        startLearning.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                startLearningActionPerformed(evt);
            }
        });

        startRecog.setText("テストサンプル認識開始");
        startRecog.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                startRecogActionPerformed(evt);
            }
        });

        clearBT.setText("クリア");
        clearBT.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clearBTActionPerformed(evt);
            }
        });

        jLabel8.setText("学習終了判定係数");

        zerTF.setText("0.4");

        jLabel9.setText("慣性項係数");

        momentum.setText("0.0");

        pruningBt.setText("プルーニング");
        pruningBt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pruningBtActionPerformed(evt);
            }
        });

        stopProcedure.setText("中止");
        stopProcedure.setEnabled(false);
        stopProcedure.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                stopProcedureActionPerformed(evt);
            }
        });

        pruningThresholdTF.setText("0.3");

        jLabel10.setText("枝刈り頻度");

        endPruningTF.setText("0.92");

        jLabel11.setText("枝刈り終了エラーレート");

        viewCValidationFileName.setEditable(false);

        jLabel12.setText("相互検証サンプル");

        crossValidationBt.setText("相互検証");
        crossValidationBt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                crossValidationBtActionPerformed(evt);
            }
        });

        recogAllDataBt.setText("全データ認識開始");
        recogAllDataBt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                recogAllDataBtActionPerformed(evt);
            }
        });

        allDataTF.setEditable(false);

        jLabel13.setText("全データ");

        recallBeforePruningBt.setText("プルーニング前に戻す");
        recallBeforePruningBt.setEnabled(false);
        recallBeforePruningBt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                recallBeforePruningBtActionPerformed(evt);
            }
        });

        jButton1.setText("正しく認識されたものを保存");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jMenu1.setText("File");

        openAllDataMI.setText("データセットを開く");
        openAllDataMI.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                openAllDataMIActionPerformed(evt);
            }
        });
        jMenu1.add(openAllDataMI);

        openTrainingSampleMI.setText("トレーニングサンプルを開く");
        openTrainingSampleMI.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                openTrainingSampleMIActionPerformed(evt);
            }
        });
        jMenu1.add(openTrainingSampleMI);

        jMenuItem1.setText("相互検証サンプルを開く");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem1);

        openTestSampleMI.setText("テストサンプルを開く");
        openTestSampleMI.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                openTestSampleMIActionPerformed(evt);
            }
        });
        jMenu1.add(openTestSampleMI);

        terminateProgramMI.setText("プログラムの終了");
        terminateProgramMI.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                terminateProgramMIActionPerformed(evt);
            }
        });
        jMenu1.add(terminateProgramMI);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Edit");
        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 542, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel11)
                            .addComponent(jLabel10)
                            .addComponent(jLabel3)
                            .addComponent(jLabel4)
                            .addComponent(jLabel5)
                            .addComponent(jLabel6)
                            .addComponent(jLabel7)
                            .addComponent(jLabel8)
                            .addComponent(jLabel9)
                            .addComponent(jLabel12)
                            .addComponent(jLabel13))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(viewCValidationFileName, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 230, Short.MAX_VALUE)
                            .addComponent(momentum, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 230, Short.MAX_VALUE)
                            .addComponent(zerTF, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 230, Short.MAX_VALUE)
                            .addComponent(numOfHiddenUnitsTF, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 230, Short.MAX_VALUE)
                            .addComponent(viewFileName2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 230, Short.MAX_VALUE)
                            .addComponent(errorThresholdTF, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 230, Short.MAX_VALUE)
                            .addComponent(numOfMaxLoopTF, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 230, Short.MAX_VALUE)
                            .addComponent(epsilonTF, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 230, Short.MAX_VALUE)
                            .addComponent(pruningThresholdTF, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 230, Short.MAX_VALUE)
                            .addComponent(endPruningTF, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 230, Short.MAX_VALUE)
                            .addComponent(viewFileName1, javax.swing.GroupLayout.DEFAULT_SIZE, 230, Short.MAX_VALUE)
                            .addComponent(allDataTF, javax.swing.GroupLayout.DEFAULT_SIZE, 230, Short.MAX_VALUE)
                            .addComponent(doNormalizationCB, javax.swing.GroupLayout.DEFAULT_SIZE, 230, Short.MAX_VALUE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGap(87, 87, 87)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(clearBT, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(startLearning, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(pruningBt, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(crossValidationBt, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(stopProcedure, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(recallBeforePruningBt, javax.swing.GroupLayout.DEFAULT_SIZE, 165, Short.MAX_VALUE)
                            .addComponent(recogAllDataBt, javax.swing.GroupLayout.DEFAULT_SIZE, 165, Short.MAX_VALUE)
                            .addComponent(startRecog, javax.swing.GroupLayout.DEFAULT_SIZE, 165, Short.MAX_VALUE)
                            .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(6, 6, 6)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(allDataTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel13))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel2)
                                    .addComponent(viewFileName1, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel12)
                                    .addComponent(viewCValidationFileName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(223, 223, 223))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER, false)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(3, 3, 3)
                                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 13, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(12, 12, 12)
                                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 13, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(12, 12, 12)
                                        .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 13, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(12, 12, 12)
                                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 13, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(12, 12, 12)
                                        .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(viewFileName2, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(6, 6, 6)
                                        .addComponent(numOfHiddenUnitsTF, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(6, 6, 6)
                                        .addComponent(errorThresholdTF, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(6, 6, 6)
                                        .addComponent(numOfMaxLoopTF, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(6, 6, 6)
                                        .addComponent(epsilonTF, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel8)
                                    .addComponent(zerTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel9)
                                    .addComponent(momentum, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(4, 4, 4)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(pruningThresholdTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel10))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(endPruningTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel11))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(doNormalizationCB)
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(startLearning)
                            .addComponent(startRecog))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(crossValidationBt)
                            .addComponent(recogAllDataBt))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(pruningBt)
                            .addComponent(recallBeforePruningBt))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(clearBT)
                            .addComponent(jButton1))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(stopProcedure))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 556, Short.MAX_VALUE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    void learning() {

        final int numOfHiddenUnits = Integer.parseInt(numOfHiddenUnitsTF.getText());
        final double epsilon = Double.parseDouble(epsilonTF.getText());
        int loopMax;

        //誤差
        double error = 0.0;



        double yBack[], hBack[], netInput;
        double sumOfErrors = 0.0;
        for (loopMax = 0; loopMax < Integer.parseInt(numOfMaxLoopTF.getText()); loopMax++) {  //学習上限回数までループ
            if (stopStatus) {
                break;
            }
            maxError = -1.0;
            sumOfErrors = 0.0;
            yBack = new double[outputLayer.length];
            hBack = new double[numOfHiddenUnits + 1];

            for (int iSample = 0; iSample < trainingSample.getNumOfSamples(); iSample++) {    //全サンプル数分のループ

                error = 0.0;
                for (int j = 0; j < trainingSample.getNumOfInputs(); j++) {
                    inputLayer[j].forward(trainingSample.getInput()[iSample][j]);
                }
                inputLayer[trainingSample.getNumOfInputs()].forward(1.0);

                for (int j = 0; j < numOfHiddenUnits; j++) {
                    hiddenLayer[j].forward(inputLayer);
                }
                hiddenLayer[numOfHiddenUnits].forward(1.0);

                for (int j = 0; j < trainingSample.getNumOfOutputs(); j++) {
                    outputLayer[j].forward(hiddenLayer);
                    error += Math.pow(trainingSample.getTarget()[iSample][j] - outputLayer[j].getOutput(), 2.0);
                }

                error = error / 2;
                if (error > maxError) {
                    maxError = error;
                }
                sumOfErrors += error;
                //以下、誤差逆伝播セクション

                for (int j = 0; j < trainingSample.getNumOfOutputs(); j++) {
                    yBack[j] = (outputLayer[j].getOutput() - trainingSample.getTarget()[iSample][j]) * (1.0 - outputLayer[j].getOutput()) * outputLayer[j].getOutput() + Double.parseDouble(momentum.getText()) * yBack[j];
                }
                for (int i = 0; i < numOfHiddenUnits + 1; i++) {
                    netInput = 0.0;
                    for (int j = 0; j < outputLayer.length; j++) {
                        netInput += outputLayer[j].getWeight()[i] * yBack[j];
                    }
                    hBack[i] = netInput * (1.0 - hiddenLayer[i].getOutput()) * hiddenLayer[i].getOutput() + Double.parseDouble(momentum.getText()) * hBack[i];
                }
                for (int i = 0; i < trainingSample.getNumOfInputs() + 1; i++) {
                    for (int j = 0; j < numOfHiddenUnits; j++) {
                        if (hiddenLayer[j].isTrainable(i)) {
                            hiddenLayer[j].setWeight(i, hiddenLayer[j].getWeight()[i] - epsilon * inputLayer[i].getOutput() * hBack[j]);
                        }
                    }
                }
                for (int i = 0; i < numOfHiddenUnits + 1; i++) {
                    for (int j = 0; j < trainingSample.getNumOfOutputs(); j++) {
                        if (outputLayer[j].isTrainable(i)) {
                            outputLayer[j].setWeight(i, outputLayer[j].getWeight()[i] - epsilon * hiddenLayer[i].getOutput() * yBack[j]);
                        }
                    }
                }
            }
            //エラーが閾値以下になったら学習を終了する
            if ((Double.parseDouble(errorThresholdTF.getText()) > maxError)) {
                break;
            }


        }

        double averageError = sumOfErrors / trainingSample.getNumOfSamples();
        statusView.append("学習完了\n");
        statusView.append("\t学習回数 : " + String.valueOf(loopMax) + "\n");
        statusView.append("\t最大誤差 : " + String.valueOf(maxError) + "\n");
        statusView.append("\t平均誤差 : " + String.valueOf(averageError) + "\n");
    }

    protected void saveWeight() {
        tempTrainableItoH = new boolean[trainingSample.getNumOfInputs() + 1][Integer.parseInt(numOfHiddenUnitsTF.getText())];
        tempTrainableHtoO = new boolean[Integer.parseInt(numOfHiddenUnitsTF.getText()) + 1][trainingSample.getNumOfOutputs()];
        tempWeightItoH = new double[trainingSample.getNumOfInputs() + 1][Integer.parseInt(numOfHiddenUnitsTF.getText())];
        tempWeightHtoO = new double[Integer.parseInt(numOfHiddenUnitsTF.getText()) + 1][trainingSample.getNumOfOutputs()];

        for (int i = 0; i < trainingSample.getNumOfInputs() + 1; i++) {
            for (int j = 0; j < Integer.parseInt(numOfHiddenUnitsTF.getText()); j++) {
                tempTrainableItoH[i][j] = hiddenLayer[j].isTrainable(i);
                tempWeightItoH[i][j] = hiddenLayer[j].getWeight()[i];
            }
        }

        for (int i = 0; i < Integer.parseInt(numOfHiddenUnitsTF.getText()) + 1; i++) {
            for (int j = 0; j < trainingSample.getNumOfOutputs(); j++) {
                tempTrainableHtoO[i][j] = outputLayer[j].isTrainable(i);
                tempWeightHtoO[i][j] = outputLayer[j].getWeight()[i];
            }
        }
    }

    private void initNetworkTable() {
        networkTable = new boolean[allSample.getNumOfInputs()][Integer.parseInt(numOfHiddenUnitsTF.getText())];
        for (int i = 0; i < allSample.getNumOfInputs(); i++) {
            for (int h = 0; h < Integer.parseInt(numOfHiddenUnitsTF.getText()); h++) {
                networkTable[i][h] = true;
            }
        }
    }

    private void openAllData() {
        JFileChooser fc = new JFileChooser(".");
        int res = fc.showOpenDialog(null);
        fc.getSelectedFile();
        if (res == JFileChooser.APPROVE_OPTION) {
            File file = fc.getSelectedFile();
            allSample = new FileOperationForNN(file);
            System.out.println("Loading test file has been completed. (filename : " + file.getName() + ")");
            statusView.append("全サンプル読み込み完了（ファイル名 : " + file.getName() + "）\n");
            allDataTF.setText(file.getName());
            if (doNormalizationCB.isSelected()) {
                allSample.normalize();
                statusView.append("正規化を行いました.\n");
            }
        } else if (res == JFileChooser.CANCEL_OPTION) {
            System.out.println("Loading all sample file has been canceled.");
        }
        initNetworkTable();
    }

    private void openCrossValidationSample() {
        JFileChooser fc = new JFileChooser(".");
        int res = fc.showOpenDialog(null);
        fc.getSelectedFile();
        if (res == JFileChooser.APPROVE_OPTION) {
            File file = fc.getSelectedFile();
            cValidationSample = new FileOperationForNN(file);
            System.out.println("Loading a cross-validation sample file has been completed. (filename : " + file.getName() + ")");
            statusView.append("相互検証サンプル読み込み完了（ファイル名 : " + file.getName() + "）\n");
            viewCValidationFileName.setText(file.getName());
            if (doNormalizationCB.isSelected()) {
                cValidationSample.normalize();
                statusView.append("正規化を行いました.\n");
            }
        } else if (res == JFileChooser.CANCEL_OPTION) {
            System.out.println("Loading a cross-validation sample file has been canceled.");
        }
    }

    private void recallNetwork() {
        for (int i = 0; i < trainingSample.getNumOfInputs() + 1; i++) {
            for (int j = 0; j < Integer.parseInt(numOfHiddenUnitsTF.getText()); j++) {
                hiddenLayer[j].setTrainable(i, tempTrainableItoH[i][j]);
                hiddenLayer[j].setWeight(i, tempWeightItoH[i][j]);

            }
        }

        for (int i = 0; i < Integer.parseInt(numOfHiddenUnitsTF.getText()) + 1; i++) {
            for (int j = 0; j < trainingSample.getNumOfOutputs(); j++) {
                outputLayer[j].setTrainable(i, tempTrainableHtoO[i][j]);
                outputLayer[j].setWeight(i, tempWeightHtoO[i][j]);
            }
        }
    }

    private void recallBeforePruning() {
        for (int i = 0; i < trainingSample.getNumOfInputs() + 1; i++) {
            for (int j = 0; j < Integer.parseInt(numOfHiddenUnitsTF.getText()); j++) {
                hiddenLayer[j].setTrainable(i, beforePruningTrainableItoH[i][j]);
                hiddenLayer[j].setWeight(i, beforePruningWeightItoH[i][j]);

            }
        }

        for (int i = 0; i < Integer.parseInt(numOfHiddenUnitsTF.getText()) + 1; i++) {
            for (int j = 0; j < trainingSample.getNumOfOutputs(); j++) {
                outputLayer[j].setTrainable(i, beforePruningTrainableHtoO[i][j]);
                outputLayer[j].setWeight(i, beforePruningWeightHtoO[i][j]);
            }
        }
    }

    private void pruning() {
        saveBeforePruning();
        recallBeforePruningBt.setEnabled(true);
        stopStatus = false;
        numOfCut = 0;
        int countprune=0;

        while (!stopStatus) {

            saveWeight();
            pruningMain();
 //           System.out.println("一巡");
            learning();
            checkRate();
            System.out.println("rate = "+rate);
            if (rate < Double.parseDouble(endPruningTF.getText())) {
                break;
            }
            countprune++;
        }
        numOfCut--;
//        if(countprune!=1)
       	recallNetwork();
        System.out.println("Pruning has be done.");
        System.out.println("The number of pruned weight : " + numOfCut);
        statusView.append("プルーニング終了\n");
        statusView.append("枝刈りされた接続 : " + numOfCut + "\n");
    }

    private void pruningMain() {
        double EATA2 = Double.parseDouble(pruningThresholdTF.getText());
        double maxW1W2 = 0.0;
        double minW1W2 = 0.0;
        double w1w2;
        int i1 = 0;
        int h1 = 0;
        boolean notPruned = true;


//入力層→中間層の枝刈り
        for (int i = 0; i < inputLayer.length; i++) {
            for (int j = 0; j < hiddenLayer.length - 1; j++) {
                maxW1W2 = 0.0;
                for (int k = 0; k < outputLayer.length; k++) {
                    if (hiddenLayer[j].isTrainable(i) && outputLayer[k].isTrainable(j)) {
                        w1w2 = Math.abs(hiddenLayer[j].getWeight()[i] * outputLayer[k].getWeight()[j]);
                        if (minW1W2 == 0.0) {
                            minW1W2 = w1w2;
                            i1 = i;
                            h1 = j;
                        }
                        if (maxW1W2 < w1w2) {
                            maxW1W2 = w1w2;
                        }
                    }

                }
                if (maxW1W2 != 0.0 && maxW1W2 <= 4 * EATA2) {
                    System.out.println("条件1\ti = " + i + ", h = " + j);
                    statusView.append("条件1\ti = " + String.valueOf(i) + ", h = " + String.valueOf(j) + "\n");
                    hiddenLayer[j].delWeight(i);
                    numOfCut++;
                    notPruned = false;
                }
                if (maxW1W2 != 0.0 && minW1W2 > maxW1W2) {
                    minW1W2 = maxW1W2;
                    i1 = i;
                    h1 = j;
                }
            }
        }
//中間層→出力層の枝刈り
        
        for (int i = 0; i < hiddenLayer.length; i++) {
            for (int j = 0; j < outputLayer.length; j++) {
                if (outputLayer[j].isTrainable(i)) {
                    if (Math.abs(outputLayer[j].getWeight()[i]) <= 4 * EATA2) {
                        System.out.println("条件2\th = " + i + ", o = " + j);
                        statusView.append("条件2\th = " + i + ", o = " + j + "\n");
                        outputLayer[j].delWeight(i);
                        numOfCut++;
                        notPruned = false;
                    }
                }
            }
        }
//上記のどれにも当てはまらない場合の入力層→出力層の枝刈り
        if (notPruned) {

            if (hiddenLayer[h1].isTrainable(i1)) {
                System.out.println("条件3\ti = " + i1 + ", h = " + h1);
                statusView.append("条件3\ti = " + i1 + ", h = " + h1 + "\n");
                hiddenLayer[h1].delWeight(i1);
                numOfCut++;
            } else {
                System.out.println("Not pruned");
            }
        }


    }

    private void checkRate() {
        double ZER = Double.parseDouble(zerTF.getText());
        boolean flag = true;
        numOfAccordance = 0;
        for (int i = 0; i < trainingSample.getNumOfSamples(); i++) {
            for (int j = 0; j < trainingSample.getNumOfInputs(); j++) {
                inputLayer[j].forward(trainingSample.getInput()[i][j]);
            }
            for (int j = 0; j < hiddenLayer.length - 1; j++) {
                hiddenLayer[j].forward(inputLayer);
            }
            for (int j = 0; j < outputLayer.length; j++) {
                outputLayer[j].forward(hiddenLayer);

            }

            for (int j = 0; j < outputLayer.length; j++) {
                if (outputLayer[j].getOutput() > 1.0 - ZER && outputLayer[j].getOutput() < 1.0 + ZER) {

                    if ((int) trainingSample.getTarget()[i][j] != 1) {
                        flag = false;
                    }
                } else if (outputLayer[j].getOutput() < ZER && outputLayer[j].getOutput() > -ZER) {

                    if ((int) trainingSample.getTarget()[i][j] != 0) {
                        flag = false;
                    }
                } else {

                    flag = false;
                }

            }
            if (flag) {
                numOfAccordance++;
            }

            flag = true;
        }
        rate = (double) numOfAccordance / (double) trainingSample.getNumOfSamples();

    }

    private void recogAllData() {
        double ZER = Double.parseDouble(zerTF.getText());
        boolean flag = true;
        numOfAccordance = 0;
        for (int i = 0; i < allSample.getNumOfSamples(); i++) {
            for (int j = 0; j < allSample.getNumOfInputs(); j++) {
                inputLayer[j].forward(allSample.getInput()[i][j]);
            }
            for (int j = 0; j < hiddenLayer.length - 1; j++) {
                hiddenLayer[j].forward(inputLayer);
            }
            for (int j = 0; j < outputLayer.length; j++) {
                outputLayer[j].forward(hiddenLayer);

            }
            statusView.append("第" + (i + 1) + "サンプルの目標出力 : ");
            for (int j = 0; j < allSample.getNumOfOutputs(); j++) {
                statusView.append(String.valueOf((int) (allSample.getTarget()[i][j])) + "\t");
            }
            statusView.append("ネットワークの出力 : ");
            for (int j = 0; j < outputLayer.length; j++) {
                if (outputLayer[j].getOutput() > 1.0 - ZER && outputLayer[j].getOutput() < 1.0 + ZER) {
                    statusView.append("1\t");
                    if ((int) allSample.getTarget()[i][j] != 1) {
                        flag = false;
                    }
                } else if (outputLayer[j].getOutput() < ZER && outputLayer[j].getOutput() > -ZER) {
                    statusView.append("0\t");
                    if ((int) allSample.getTarget()[i][j] != 0) {
                        flag = false;
                    }
                } else {
                    statusView.append("*\t");
                    flag = false;
                }

            }
            if (flag) {
                numOfAccordance++;

            }
            statusView.append("\n");
            flag = true;
        }
        rate = (double) numOfAccordance / (double) allSample.getNumOfSamples();
        statusView.append("認識率 : " + String.valueOf(rate * 100.0) + " %\n");
        statusView.append("正しく認識されたサンプル数 : " + numOfAccordance + "\n");
    }

    private void saveBeforePruning() {
        beforePruningTrainableItoH = new boolean[trainingSample.getNumOfInputs() + 1][Integer.parseInt(numOfHiddenUnitsTF.getText())];
        beforePruningTrainableHtoO = new boolean[Integer.parseInt(numOfHiddenUnitsTF.getText()) + 1][trainingSample.getNumOfOutputs()];
        beforePruningWeightItoH = new double[trainingSample.getNumOfInputs() + 1][Integer.parseInt(numOfHiddenUnitsTF.getText())];
        beforePruningWeightHtoO = new double[Integer.parseInt(numOfHiddenUnitsTF.getText()) + 1][trainingSample.getNumOfOutputs()];

        for (int i = 0; i < trainingSample.getNumOfInputs() + 1; i++) {
            for (int j = 0; j < Integer.parseInt(numOfHiddenUnitsTF.getText()); j++) {
                beforePruningTrainableItoH[i][j] = hiddenLayer[j].isTrainable(i);
                beforePruningWeightItoH[i][j] = hiddenLayer[j].getWeight()[i];
            }
        }

        for (int i = 0; i < Integer.parseInt(numOfHiddenUnitsTF.getText()) + 1; i++) {
            for (int j = 0; j < trainingSample.getNumOfOutputs(); j++) {
                beforePruningTrainableHtoO[i][j] = outputLayer[j].isTrainable(i);
                beforePruningWeightHtoO[i][j] = outputLayer[j].getWeight()[i];
            }
        }

    }

    private void startRecog() {
        writeOutNameFile();
        String cls = null;

        list = new ArrayList[testSample.getNumOfSamples()];
        for (int i = 0; i < testSample.getNumOfSamples(); i++) {

            list[i] = new ArrayList<Object>();
        }

        double ZER = Double.parseDouble(zerTF.getText());
        boolean flag = true;
        numOfAccordance = 0;
        for (int i = 0; i < testSample.getNumOfSamples(); i++) {
            for (int j = 0; j < testSample.getNumOfInputs(); j++) {
                inputLayer[j].forward(testSample.getInput()[i][j]);
            }
            for (int j = 0; j < hiddenLayer.length - 1; j++) {
                hiddenLayer[j].forward(inputLayer);
            }
            for (int j = 0; j < outputLayer.length; j++) {
                outputLayer[j].forward(hiddenLayer);

            }
            statusView.append("第" + (i + 1) + "サンプルの目標出力 : ");
            for (int j = 0; j < testSample.getNumOfOutputs(); j++) {
                statusView.append(String.valueOf((int) (testSample.getTarget()[i][j])) + "\t");
            }
            statusView.append("ネットワークの出力 : ");
            for (int j = 0; j < outputLayer.length; j++) {
                if (outputLayer[j].getOutput() > 1.0 - ZER && outputLayer[j].getOutput() < 1.0 + ZER) {
                    statusView.append("1\t");
                    if ((int) testSample.getTarget()[i][j] != 1) {
                        flag = false;
                    }
                } else if (outputLayer[j].getOutput() < ZER && outputLayer[j].getOutput() > -ZER) {
                    statusView.append("0\t");
                    if ((int) testSample.getTarget()[i][j] != 0) {
                        flag = false;
                    }
                } else {
                    statusView.append("*\t");
                    flag = false;
                }

            }
            if (flag) {
                // 正しく認識できたデータを配列に格納
                for (int j = 0; j < testSample.getNumOfInputs(); j++) {

                    if (testSample.getWritable(j)) {
                        list[i].add(testSample.getInput(i, j));
                    }
                }
/*                if(((int) testSample.getTarget()[i][0] == 0) && ((int) testSample.getTarget()[i][1] == 1)) {
                    list[i].add("Class 1");
                } else if(((int) testSample.getTarget()[i][0] == 1) && ((int) testSample.getTarget()[i][1] == 0)) {
                    list[i].add("Class 2");
                }
*/                
                //3クラス用
               if(((int) testSample.getTarget()[i][0] == 0) && ((int) testSample.getTarget()[i][1] == 0) && ((int) testSample.getTarget()[i][2] == 1)){
                    list[i].add("Class 1");
                } else if(((int) testSample.getTarget()[i][0] == 0) && ((int) testSample.getTarget()[i][1] == 1) && ((int) testSample.getTarget()[i][2] == 0)) {
                    list[i].add("Class 2");
                } else if(((int) testSample.getTarget()[i][0] == 1) && ((int) testSample.getTarget()[i][1] == 0) && ((int) testSample.getTarget()[i][2] == 0)) {
                    list[i].add("Class 3");
                }
               
                numOfAccordance++;
            }
            statusView.append("\n");
            flag = true;
        }
        rate = (double) numOfAccordance / (double) testSample.getNumOfSamples();
        statusView.append("認識率 : " + String.valueOf(rate * 100.0) + " %\n");

        //writeOutNameFile();

        /* dataファイルの作成 */


        writeOutArffFile(list);
        writeOutDataFile(list);
        writeOutWeightFile();

    }

    private void openTrainingSample() {
        JFileChooser fc = new JFileChooser(".");
        int res = fc.showOpenDialog(null);
        if (res == JFileChooser.APPROVE_OPTION) {
            File file = fc.getSelectedFile();
            trainingSample = new FileOperationForNN(file);
            System.out.println("Loading training file has been completed. (filename : " + file.getName() + ")");
            statusView.append("トレーニングサンプル読み込み完了（ファイル名 : " + file.getName() + "）\n");
            viewFileName1.setText(file.getName());
            if (doNormalizationCB.isSelected()) {
                trainingSample.normalize();
                statusView.append("正規化を行いました.\n");
            }
            int numOfHiddenUnits = Integer.parseInt(numOfHiddenUnitsTF.getText());
            //入力層のユニット数は閾値処理のため１つ増やす
            inputLayer = new AN[trainingSample.getNumOfInputs() + 1];
            //同様に中間層ユニット数もひとつ増やす
            hiddenLayer = new AN[Integer.parseInt(numOfHiddenUnitsTF.getText()) + 1];
            //出力層のユニット数
            outputLayer = new AN[trainingSample.getNumOfOutputs()];
            //各層の初期化
            for (int i = 0; i < trainingSample.getNumOfInputs(); i++) {
                inputLayer[i] = new AN(trainingSample.getNumOfInputs());
            }
            inputLayer[trainingSample.getNumOfInputs()] = new AN(1.0);

            for (int i = 0; i < numOfHiddenUnits; i++) {
                hiddenLayer[i] = new AN(inputLayer.length);
            }
            hiddenLayer[numOfHiddenUnits] = new AN(1.0);

            for (int i = 0; i < trainingSample.getNumOfOutputs(); i++) {
                outputLayer[i] = new AN(hiddenLayer.length);
            }

        } else if (res == JFileChooser.CANCEL_OPTION) {
            System.out.println("Loading training file has been canceled.");
        }
    }

    private void openTestSample() {
        JFileChooser fc = new JFileChooser(".");
        int res = fc.showOpenDialog(null);
        fc.getSelectedFile();
        if (res == JFileChooser.APPROVE_OPTION) {
            File file = fc.getSelectedFile();
            testSample = new FileOperationForNN(file);
            System.out.println("Loading test file has been completed. (filename : " + file.getName() + ")");
            statusView.append("テストサンプル読み込み完了（ファイル名 : " + file.getName() + "）\n");
            viewFileName2.setText(file.getName());
            if (doNormalizationCB.isSelected()) {
                testSample.normalize();
                statusView.append("正規化を行いました.\n");
            }
        } else if (res == JFileChooser.CANCEL_OPTION) {
            System.out.println("Loading test file has been canceled.");
        }
    }

private void openTrainingSampleMIActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_openTrainingSampleMIActionPerformed
    openTrainingSample();
}//GEN-LAST:event_openTrainingSampleMIActionPerformed

private void openTestSampleMIActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_openTestSampleMIActionPerformed
    openTestSample();
}//GEN-LAST:event_openTestSampleMIActionPerformed

private void terminateProgramMIActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_terminateProgramMIActionPerformed
    System.out.println("GoodBye!");
    System.exit(0);
}//GEN-LAST:event_terminateProgramMIActionPerformed

private void startLearningActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_startLearningActionPerformed
    Learn l = new Learn();
    startLearning.setEnabled(false);
    startLearning.setText("学習中");
    stopProcedure.setEnabled(true);
    stopStatus = false;
    l.execute();
}//GEN-LAST:event_startLearningActionPerformed

private void startRecogActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_startRecogActionPerformed
    startRecog();
}//GEN-LAST:event_startRecogActionPerformed

private void clearBTActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_clearBTActionPerformed
    statusView.setText("");
}//GEN-LAST:event_clearBTActionPerformed

private void pruningBtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pruningBtActionPerformed
    pruningBt.setEnabled(false);
    pruningBt.setText("プルーニング中");
    stopProcedure.setEnabled(true);
    Prune p = new Prune();
    p.execute();
}//GEN-LAST:event_pruningBtActionPerformed

private void stopProcedureActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_stopProcedureActionPerformed
    stopStatus = true;
}//GEN-LAST:event_stopProcedureActionPerformed

private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
    openCrossValidationSample();
}//GEN-LAST:event_jMenuItem1ActionPerformed

private void crossValidationBtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_crossValidationBtActionPerformed
    //crossValidate();
    crossValidationBt.setEnabled(false);
    crossValidationBt.setText("相互検証中");
    stopProcedure.setEnabled(true);
    CrossValidate cv = new CrossValidate();
    cv.execute();
}//GEN-LAST:event_crossValidationBtActionPerformed

private void recogAllDataBtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_recogAllDataBtActionPerformed
    recogAllData();
}//GEN-LAST:event_recogAllDataBtActionPerformed

private void openAllDataMIActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_openAllDataMIActionPerformed
    openAllData();
}//GEN-LAST:event_openAllDataMIActionPerformed

private void recallBeforePruningBtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_recallBeforePruningBtActionPerformed
    recallBeforePruning();
    statusView.append("ネットワークをプルーニング前の状態に戻しました.\n");
}//GEN-LAST:event_recallBeforePruningBtActionPerformed

private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
    writeOutNameFile();
}//GEN-LAST:event_jButton1ActionPerformed

    /**
    * @param args the command line arguments
    */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Main().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField allDataTF;
    private javax.swing.JButton clearBT;
    private javax.swing.JButton crossValidationBt;
    private javax.swing.JCheckBox doNormalizationCB;
    private javax.swing.JTextField endPruningTF;
    private javax.swing.JTextField epsilonTF;
    private javax.swing.JTextField errorThresholdTF;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField momentum;
    private javax.swing.JTextField numOfHiddenUnitsTF;
    private javax.swing.JTextField numOfMaxLoopTF;
    private javax.swing.JMenuItem openAllDataMI;
    private javax.swing.JMenuItem openTestSampleMI;
    private javax.swing.JMenuItem openTrainingSampleMI;
    private javax.swing.JButton pruningBt;
    private javax.swing.JTextField pruningThresholdTF;
    private javax.swing.JButton recallBeforePruningBt;
    private javax.swing.JButton recogAllDataBt;
    private javax.swing.JButton startLearning;
    private javax.swing.JButton startRecog;
    private javax.swing.JTextArea statusView;
    private javax.swing.JButton stopProcedure;
    private javax.swing.JMenuItem terminateProgramMI;
    private javax.swing.JTextField viewCValidationFileName;
    private javax.swing.JTextField viewFileName1;
    private javax.swing.JTextField viewFileName2;
    private javax.swing.JTextField zerTF;
    // End of variables declaration//GEN-END:variables



    class Prune extends SwingWorker<Object, Object> {

        @Override
        protected Object doInBackground() throws Exception {
            pruning();
            return null;
        }

        @Override
        protected void done() {
            pruningBt.setEnabled(true);
            pruningBt.setText("プルーニング");
            stopProcedure.setEnabled(false);
        }

    }

    class Learn extends SwingWorker<Object, Object> {

        @Override
        protected Object doInBackground() throws Exception {

            learning();
            return null;
        }

        @Override
        protected void done() {
            stopStatus = false;
            stopProcedure.setEnabled(false);
            startLearning.setEnabled(true);
            startLearning.setText("学習開始");
        }




    }

    class CrossValidate extends SwingWorker<Object, Object> {

        @Override
        protected Object doInBackground() throws Exception {
            crossValidate();
            return null;
        }

        @Override
        protected void done() {
            stopStatus = false;
            stopProcedure.setEnabled(false);
            crossValidationBt.setEnabled(true);
            crossValidationBt.setText("相互検証");
        }

    }

}


