/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package autorerxxx;

/**
 *
 * 
 */
public class AttributeType {
    private String label;
    private String type;
    private boolean writable;

    public void setLabel(String label) {
        this.label = label;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setWritable(boolean b) {
        writable = b;
    }

    public String getLabel() {
        return label;
    }

    public String getType() {
        return type;
    }

    public boolean getWritable() {
        return writable;
    }
}
