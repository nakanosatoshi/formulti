package fullauto;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;

/*
 * 過学習防止用クラス. LDからLD'を作るメソッド
 */
public class SelectedLearningDataSet {
	
	public static void main(String[] args) {
		//String ProjectName = "CARD";
		//MakeDirectory(ProjectName);
		//File file = OriginalFile();
		//PreventOverFitting(file, 0.8, "LD");
	}
	
	// 実験用ディレクトリ作成
	public static void MakeDirectory(){
		File newdir = new File("c:\\FullAutoReRX\\" + Main.ProjectName);
		if(newdir.mkdirs()) Main.statusView.append("ディレクトリの作成に成功しました。\nc:\\FullAutoReRX\\" + Main.ProjectName + " 直下に各ファイルを格納していきます。\n");
		else Main.statusView.append("実験用ディレクトリは既に存在しています。\nc:\\FullAutoReRX\\" + Main.ProjectName + " 直下に各ファイルを格納していきます。\n");
	}
	
	// 階層別ディレクトリ作成　　プロジェクトフォルダの直下に階層別のフォルダが作成される　
	public static void MakeFileFolder(String FileFolderName){
		File newdir = new File("c:\\FullAutoReRX\\" + Main.ProjectName + "\\"+FileFolderName);
		if(newdir.mkdirs()) Main.statusView.append("ディレクトリの作成に成功しました。nc:\\FullAutoReRX\\" + Main.ProjectName + "\\"+FileFolderName+"直下に各ファイルを格納していきます。\n");
		else Main.statusView.append("実験用ディレクトリは既に存在しています。nc:\\FullAutoReRX\\" + Main.ProjectName + "\\"+FileFolderName+"直下に各ファイルを格納していきます。\n");
	}
	
	// 階層別ディレクトリ作成
	public static void MakeChildDirectory(){
		File newdir = new File("c:\\FullAutoReRX\\" + Main.ProjectName);
	}
	
	// 元となるデータセットの読み込み. ループプログラミング時は要工夫.
	public static File OriginalFile(String FilePath){
		//String FilePath = "c:\\FullAutoReRX\\" + Main.ProjectName + "\\NoisePreserve.txt";
		// ファイルの読み込み
		File file = new File(FilePath);
		System.out.println(file.getName() + "を利用してダッシュ付きデータセットを作成します。");
		try{
			FileReader filereader = new FileReader(file);
			System.out.println(file.getName() + "の読み込みに成功しました。");
		}catch(FileNotFoundException e){
			System.out.println("例外: " + e);
			System.out.println("ダッシュ付きファイル作成クラスにおいて例外が発生したため、強制終了します。");
			System.exit(-1);
		}
		return file;
	}
	
	// データセットをシャッフルしてランダムに取得し、ダッシュ付きLerningDataSetを作成する
	public static void PreventOverFitting(File ld, double percent,String LDName){
		try{
			int datanum, Dashdatanum, zokuseinum, classnum;
			// トレーニングデータの過学習対策ダッシュ付き作成
			File LDDash = new File("c:\\FullAutoReRX\\" + Main.ProjectName + "\\" + Main.RuleName[Main.RoopNum-1] + "\\" + Main.LearningDataName + "'.txt");
			String filename = LDDash.getName();
			try{
				if (LDDash.createNewFile()){
					System.out.println(filename + "の作成に成功しました");
				}else{
					System.out.println(filename + "はすでに存在しているため上書きします");
				}
			}catch(IOException e){
				System.out.println(e);
			}
			//ArrayListを生成する
			ArrayList<String> al = new ArrayList<String>();
			// ファイルに書き込むPrintWriterクラス
			PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter(LDDash)));
			// ダッシュ付きトレーニングデータの名前を取得
			int index = filename.lastIndexOf('.');
			String LDDashName = filename.substring(0, index);
			
			// 読み込むファイルのエラーチェック
			if (checkBeforeReadfile(ld)){
				BufferedReader br = new BufferedReader(new FileReader(ld));
				String str;
				str = br.readLine();
				// データ数
				System.out.println(LDName + " Data Num: " + str);
				datanum = Integer.parseInt(str);
				// ダッシュ付きLDのデータ数 = LD * 割合
				Dashdatanum = (int)(datanum * percent);
				System.out.println("使用倍率: " + percent);
				System.out.println(LDDashName + " Data Num: " + Dashdatanum);
				pw.println(Dashdatanum);
				
				// 属性数
				str = br.readLine();
				System.out.println("属性数: " + str);
				zokuseinum = Integer.parseInt(str);
				pw.println(str);
				// クラス数
				str = br.readLine();
				System.out.println("クラス数: " + str);
				classnum = Integer.parseInt(str);
				pw.println(str);
				// 属性名
				str = br.readLine();
				System.out.println(str);
				pw.println(str);
				
				// データを1列ずつリストに入れる。
				while((str = br.readLine()) != null){
					al.add(str);
				}
				// シャッフルする
				Collections.shuffle(al);
				// 倍率分のデータだけ取得
				System.out.println("######## Create DashDataSet ########");
				for(int i = 0; i < Dashdatanum; i++){
					//System.out.println(al.get(i));
					pw.println(al.get(i));
				}
				// BufferedReaderの終了
				br.close();
				// PrintWriterの終了
				pw.close();
			}else{
				System.out.println("ファイルが見つからないか開けません");
			}
		}catch(FileNotFoundException e){
			System.out.println("Error: FileNotFoundExceptionです．");
		}catch(IOException e){
			System.out.println("Error: IOExceptionです．");
		}
	}

	// 読み込むファイルのエラーチェック
	private static boolean checkBeforeReadfile(File file) {
		// TODO Auto-generated method stub
		if (file.exists()){
			if (file.isFile() && file.canRead()){
				return true;
			}
		}
		return false;
	}
}
