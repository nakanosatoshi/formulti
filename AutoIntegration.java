package fullauto;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StreamTokenizer;
import java.math.BigDecimal;
import java.util.ArrayList;

public class AutoIntegration {

	static int[][] CDistinction, CDistinction2;
	static int IntegratedRuleNum; // 統合後のルール数
	static int zokusei;
	static int[] DCArray;	//各属性のDC判別用配列
	
	public static void Integrate() throws IOException{
		int ParentRuleNum;	// 統合比較先のルール数
		int ChildRuleNum;	// 新しいルールの数
		IntegratedRuleNum = 0; // 統合後のルール数初期化
		DCArray = AutoException.DCArray; // AutoExceptionからDCArrayをコピー
		zokusei = AutoException.zokusei; // 属性数
		File ParentRuleSet;
		// 三段目の場合は、IntegratedRule1とThirdlyRuleの統合
		if(Main.RoopNum == 3){
			ParentRuleSet = ReceiveRuleSet("IntegratedRuleSet"+(Main.RoopNum-2), Main.ProjectName,(Main.RoopNum-2));
		}
		else{
			ParentRuleSet = ReceiveRuleSet(Main.RuleName[Main.RoopNum-2], Main.ProjectName,(Main.RoopNum-2));
		}
		File ChildRuleSet = ReceiveRuleSet(Main.RuleName[Main.RoopNum-1], Main.ProjectName,(Main.RoopNum-1));
		// ルール数測定（不要なら消す）
		ParentRuleNum = CountRuleNum(ParentRuleSet);
		ChildRuleNum = CountRuleNum(ChildRuleSet);
		//連続値の場合、＞≧＝≦＜の5パターン考えられる.それぞれ順に、1,2,3,4,5を値に加算し、判別できるようにする.
		CDistinction = new int[ParentRuleNum+1][zokusei+1+2];
		CDistinction2 = new int[ChildRuleNum+1][zokusei+1+2];
		
		BigDecimal[][] ParentArray = new BigDecimal[ParentRuleNum+1][zokusei+1+2]; // 上の段までのルール（統合比較先のルール） ※+1で実際の数字に合わせる,+2はクラス用
		BigDecimal[][] ChildArray = new BigDecimal[ChildRuleNum+1][zokusei+1+2]; // 新規ルール
		Boolean[] ParentRuleFlg = new Boolean[ParentRuleNum+1]; // 統合後、そのルールが残るかどうか。
		Boolean[] ChildRuleFlg = new Boolean[ChildRuleNum+1]; // 統合後、そのルールが残るかどうか。
		boolean newrulecheck = false; // 統合後、そのルールが残るかどうかを判別するための簡易保存用。ひとつでも親と子で異なっている部分があればtrueが入る。
		//boolean PAcheck = false, CAcheck = false; // 統合法則２で親と子がまったく別々のルールであるかの判別に使用。
		boolean PAcheck, CAcheck, acheck, bcheck;
		for(int i=1;i<=ParentRuleNum;i++){
			for(int j=1;j<=zokusei;j++){
				ParentArray[i][j] = BigDecimal.valueOf(2);
			}
		}
		for(int i=1;i<=ChildRuleNum;i++){
			for(int j=1;j<=zokusei;j++){
				ChildArray[i][j] = BigDecimal.valueOf(2);
			}
		}
		for(int i=1; i<=ParentRuleNum;i++){
			ParentRuleFlg[i] = true;
		}
		for(int i=1; i<=ChildRuleNum;i++){
			ChildRuleFlg[i] = true;
		}
		
		InsertParentRuleArray(ParentRuleSet, ParentArray, zokusei);
		InsertChildRuleArray(ChildRuleSet, ChildArray, zokusei);
		/*
		 * 統合ルール１： 同ルール
		 * 属性とその値がすべて同一であり、クラスが同じである、つまり同ルールであれば統合される。
		 */
		for(int ci=1; ci <= ChildRuleNum; ci++){
			for(int pi=1; pi <= ParentRuleNum; pi++){
				newrulecheck = false;
				for(int j=1; j < zokusei+1+2; j++){
					// ### もしC属性だったら大小の比較部も込みで確認したほうがよい？？
					if(ParentArray[pi][j].compareTo(ChildArray[ci][j]) != 0){
						newrulecheck = true;
					}
				}
				if(!newrulecheck){
					ChildRuleFlg[ci] = false;
				}
			}
		}
		/*
		 * 統合ルール２： 包含
		 * クラスラベルによらず、使用属性が少ない方の属性を使用属性が多い方が全て所持していてその値が同一である場合、##親ルールに統合する。##
		 * newrulecheckがtrueにならず、falseのまま通過したら統合法則２にのっとっているとし、そのルールは統合される。
		 */
		for(int ci=1; ci <= ChildRuleNum; ci++){
			//while(!ChildRuleFlg[ci]) ci++; // 既にこれまでの統合法則によりルールが消失していたら次のルールへ。つまり統合法則１を抜けてfalseなら統合法則１に沿っていたルールだったのでここでは調べる必要がない。
			for(int pi=1; pi <= ParentRuleNum; pi++){
				//if(!ParentRuleFlg[pi]) pi++;
				newrulecheck = false;
				PAcheck = false;
				CAcheck = false;
				// jはzokusei+1まででクラスはいらないかも
				for(int j=1; j<zokusei+1; j++){
					// 親ルールに統合するようにしています。実際は「長い方(属性が多い方)のルールに包含される(?)」そうです。
					if(j <= zokusei+1){
						// 親と子で値が違ってどちらも２でなければ「両方ともその属性を持っているが値が違う」ので統合法則２に当てはまらない。
						if(ParentArray[pi][j].compareTo(ChildArray[ci][j]) != 0 && ParentArray[pi][j].compareTo(BigDecimal.valueOf(2)) != 0 && ChildArray[ci][j].compareTo(BigDecimal.valueOf(2)) != 0){
							newrulecheck = true;
						}
						// 親だけが持ってる属性と子だけが持ってる属性がそれぞれあった場合、まったく違うルールである。
						if(ParentArray[pi][j].compareTo(ChildArray[ci][j]) != 0 && (ParentArray[pi][j].compareTo(BigDecimal.valueOf(2)) == 0 || ChildArray[ci][j].compareTo(BigDecimal.valueOf(2)) == 0)){
							if(ParentArray[pi][j].compareTo(BigDecimal.valueOf(2)) == 0){
								PAcheck = true;
								if(CAcheck == true){
									newrulecheck = true;
								}
							}
							else if(ChildArray[ci][j].compareTo(BigDecimal.valueOf(2)) == 0){
								CAcheck = true;
								if(PAcheck == true){
									newrulecheck = true;
								}
							}
						}
					}
				}
				if(!newrulecheck){
					ChildRuleFlg[ci] = false;
				}
			}
		}
		/*
		 * 統合ルール３： 矛盾
		 * a: 属性とその値が全て同一だがクラスラベルが異なる場合、該当する親ルールに統合する。
		 * b: 属性とクラスラベルが全て同一だが値が異なる場合、該当する親ルールに統合する。
		 */
		for(int ci=1; ci <= ChildRuleNum; ci++){
			for(int pi=1; pi <= ParentRuleNum; pi++){
				acheck = false;
				bcheck = false;
				for(int j=1; j<zokusei+1; j++){
					// a: クラスラベル以外の値がひとつでも異なっていればaの統合法則には沿っていない。統合しない。
					if(ParentArray[pi][j].compareTo(ChildArray[ci][j]) != 0)
						acheck = true;
					// b: 
					if((ParentArray[pi][j].compareTo(BigDecimal.valueOf(2)) == 0 && ChildArray[ci][j].compareTo(BigDecimal.valueOf(2)) != 0) || (ParentArray[pi][j].compareTo(BigDecimal.valueOf(2)) != 0 && ChildArray[ci][j].compareTo(BigDecimal.valueOf(2)) == 0))
						bcheck = true;
				}
				// acheckがfalseならもうクラスが同じか別かに限らず統合される。クラスが同じ場合は完全一致で法則１、クラスが別の場合が法則３のa
				if(!acheck) ChildRuleFlg[ci] = false;
				// bcheckが入っている⇒違う属性を使っている、bcheckがfalseなら値が同じか別かは置いといて同じ属性を使っているのでクラスが同じなら法則３のb、値も同じなら完全一致で法則１
				if(!bcheck) if(ParentArray[pi][zokusei+1].compareTo(ChildArray[ci][zokusei+1]) == 0) ChildRuleFlg[ci] = false;
			}
		}
		
		//統合後のルール数を数えるために使用するParentRuleとChildRuleを調べる
		for(int i=1;i<=ParentRuleNum;i++){
			System.out.println("ParentRuleFlg["+i+"] = " +ParentRuleFlg[i]);
			if(ParentRuleFlg[i])
				IntegratedRuleNum++;
		}
		for(int i=1;i<=ChildRuleNum;i++){
			System.out.println("ChildRuleFlg["+i+"] = " +ChildRuleFlg[i]);
			if(ChildRuleFlg[i])
				IntegratedRuleNum++;
		}
		
		// 統合後のルールセット用テキストファイル作成
		File IntegratedRuleSet = new File("c:\\FullAutoReRX\\" + Main.ProjectName + "\\" + Main.RuleName[Main.RoopNum-1] + "\\IntegratedRuleSet仮保存" + (Main.RoopNum-1) + ".txt");
		String filename = IntegratedRuleSet.getName();
		try{
			if (IntegratedRuleSet.createNewFile()) { System.out.println(filename + "の作成に成功しました"); }
			else { System.out.println(filename + "は既に存在しているため上書きします"); }
		}catch(IOException e){
			System.out.println(e);
		}
		PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter(IntegratedRuleSet)));
		// 統合後の残ったルール全てをファイルに保存
		BufferedReader br = new BufferedReader(new FileReader(ParentRuleSet));
		String str;
		for(int i=1;i<=ParentRuleNum;i++){
			str = br.readLine();
			if(ParentRuleFlg[i]){
				pw.print(str);
				pw.println();
			}
		}
		BufferedReader br2 = new BufferedReader(new FileReader(ChildRuleSet));
		for(int i=1;i<=ChildRuleNum;i++){
			str = br2.readLine();
			if(ChildRuleFlg[i]){
				pw.print(str);
				pw.println();
			}
		}
		pw.close();
		// ルール番号がずれているのを修正
		RenumberRule(IntegratedRuleSet);
	}

	public static int CountRuleNum(File RuleSet) throws IOException {
		int num = 0;
		BufferedReader br = new BufferedReader(new FileReader(RuleSet));
		String str;
		ArrayList<String> al = new ArrayList<String>();

		while((str = br.readLine()) != null) al.add(str);
		num = al.size();
		System.out.println("al.size() = " + al.size());
		return num;
	}

	public static File ReceiveRuleSet(String RuleSetName, String ProjectName,int FolderName){
		String FilePath = "c:\\FullAutoReRX\\" + ProjectName + "\\" + Main.RuleName[FolderName] + "\\" + RuleSetName + ".txt";
		File file = new File(FilePath);
		if(file.exists()) {
			System.out.println(file.getName() + "の読み込みに成功しました。");
		}
		else {
			System.out.println("### ReceiveRuleSetメソッド内でエラーが発生しました ###");
			System.out.println(file + " はありません\n作業を強制終了します");
			System.exit(-1);
		}
		return file;
	}
	
	//RuleArrayの代入
	private static void InsertParentRuleArray(File RuleSet, BigDecimal[][] RuleArray, int zokusei){
		try{
			FileReader fr = new FileReader(RuleSet);
			StreamTokenizer token = new StreamTokenizer(fr);
			token.eolIsSignificant(true);
			String str;
			int tmp;
			int i=1,j=1;
			//不等号判別用に一時的に記憶する配列　
			int a[] = new int[2];
			boolean eof = false;
		while(!eof){	
			tmp = token.nextToken();
			switch(tmp){
			case StreamTokenizer.TT_NUMBER:
				break;
			case StreamTokenizer.TT_WORD:
				str = token.sval.replaceAll("[0-9]", "");
				if(str.equals("D")){
					j = Integer.parseInt(token.sval.replaceAll("[^0-9]", ""));
					tmp = token.nextToken();	//=
					tmp = token.nextToken();	//値まで移動
					RuleArray[i][j] = BigDecimal.valueOf(token.nval);	//　DJの値を格納(正確には上書き更新)
				}
				if(str.equals("C")){
					j = Integer.parseInt(token.sval.replaceAll("[^0-9]", ""));
					tmp = token.nextToken();
					a[0] = tmp;		//一つ目の記号
					tmp = token.nextToken();
					if(tmp != StreamTokenizer.TT_NUMBER){
						a[1] = tmp;  //次も記号なら読み込む	
						tmp = token.nextToken();
					}
					InsertParentCDistinction(a,i,j);	//CDistinctionの格納
					CrearArray(a,2);		//記号読み取り用配列のクリア
					RuleArray[i][j] = BigDecimal.valueOf(token.nval);
				}
				if(str.equals("Class")){
					tmp = token.nextToken();
					if(token.nval == 1){
						RuleArray[i][zokusei+1] = BigDecimal.valueOf(0);
						RuleArray[i][zokusei+2] = BigDecimal.valueOf(1);
					}
					else{
						RuleArray[i][zokusei+1] = BigDecimal.valueOf(1);
						RuleArray[i][zokusei+2] = BigDecimal.valueOf(0);
					}
				}
				break;
				
			case StreamTokenizer.TT_EOL:
				i++;  //改行の数でルールの数を把握する
				break;
				
			case StreamTokenizer.TT_EOF:
				eof = true; 
				break;
				
			default:	
				break;
			}
		}
		}catch(FileNotFoundException e){
			System.out.println("### InsertRuleArray(File RuleSet)メソッド内でエラーが発生しました ###");
			System.out.println("作業を強制終了します");
			System.exit(-1);
		}catch(IOException e){
			System.out.println("### InsertRuleArray(File RuleSet)内でエラーが発生しました ###");
			System.out.println("作業を強制終了します");
			System.exit(-1);
		}
	}
	
	//RuleArrayの代入
		private static void InsertChildRuleArray(File RuleSet, BigDecimal[][] RuleArray, int zokusei){
			try{
				FileReader fr = new FileReader(RuleSet);
				StreamTokenizer token = new StreamTokenizer(fr);
				token.eolIsSignificant(true);
				String str;
				int tmp;
				int i=1,j=1;
				//不等号判別用に一時的に記憶する配列　
				int a[] = new int[2];
				boolean eof = false;
			while(!eof){	
				tmp = token.nextToken();
				switch(tmp){
				case StreamTokenizer.TT_NUMBER:
					break;
				case StreamTokenizer.TT_WORD:
					str = token.sval.replaceAll("[0-9]", "");
					if(str.equals("D")){
						j = Integer.parseInt(token.sval.replaceAll("[^0-9]", ""));
						tmp = token.nextToken();	//=
						tmp = token.nextToken();	//値まで移動
						RuleArray[i][j] = BigDecimal.valueOf(token.nval);	//　DJの値を格納(正確には上書き更新)
					}
					if(str.equals("C")){
						j = Integer.parseInt(token.sval.replaceAll("[^0-9]", ""));
						tmp = token.nextToken();
						a[0] = tmp;		//一つ目の記号
						tmp = token.nextToken();
						if(tmp != StreamTokenizer.TT_NUMBER){
							a[1] = tmp;  //次も記号なら読み込む	
							tmp = token.nextToken();
						}
						InsertChildCDistinction(a,i,j);	//CDistinctionの格納
						CrearArray(a,2);		//記号読み取り用配列のクリア
						RuleArray[i][j] = BigDecimal.valueOf(token.nval);
					}
					if(str.equals("Class")){
						tmp = token.nextToken();
						if(token.nval == 1){
							RuleArray[i][zokusei+1] = BigDecimal.valueOf(0);
							RuleArray[i][zokusei+2] = BigDecimal.valueOf(1);
						}
						else{
							RuleArray[i][zokusei+1] = BigDecimal.valueOf(1);
							RuleArray[i][zokusei+2] = BigDecimal.valueOf(0);
						}
					}
					break;
					
				case StreamTokenizer.TT_EOL:
					i++;  //改行の数でルールの数を把握する
					break;
					
				case StreamTokenizer.TT_EOF:
					eof = true; 
					break;
					
				default:	
					break;
				}
			}
			}catch(FileNotFoundException e){
				System.out.println("### InsertRuleArray(File RuleSet)メソッド内でエラーが発生しました ###");
				System.out.println("作業を強制終了します");
				System.exit(-1);
			}catch(IOException e){
				System.out.println("### InsertRuleArray(File RuleSet)内でエラーが発生しました ###");
				System.out.println("作業を強制終了します");
				System.exit(-1);
			}
		}

	//CDistinctionに値を代入
	private static void InsertParentCDistinction(int a[],int i, int j){
		if(a[0] == '>'){
			if(a[1] == '='){
				CDistinction[i][j] = 2;
			}
			else{
				CDistinction[i][j] = 1;
			}
		}
		else if(a[0] == '='){
			CDistinction[i][j] = 3;
		}
		else if(a[0] == '<'){
			if(a[1] == '='){
				CDistinction[i][j] = 4;
			}
			else{
				CDistinction[i][j] = 5;
			}
		}
	}
	
	//CDistinction2に値を代入
	private static  void InsertChildCDistinction(int a[],int i, int j){
		if(a[0] == '>'){
			if(a[1] == '='){
				CDistinction2[i][j] = 2;
			}
			else{
				CDistinction2[i][j] = 1;
			}
		}
		else if(a[0] == '='){
			CDistinction2[i][j] = 3;
		}
		else if(a[0] == '<'){
			if(a[1] == '='){
				CDistinction2[i][j] = 4;
			}
			else{
				CDistinction2[i][j] = 5;
			}
		}
	}

	//不等号判別用配列を初期化
	private static void CrearArray(int a[],int b){
		for(int i=0;i<b;i++){
			a[i] = 0;
		}
	}

	
	private static void RenumberRule(File OriginRuleSet) throws IOException{
		File IntegratedRuleSet = new File("c:\\FullAutoReRX\\" + Main.ProjectName + "\\" + Main.RuleName[Main.RoopNum-1] + "\\IntegratedRuleSet" + (Main.RoopNum-1) + ".txt");
		
		PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter(IntegratedRuleSet)));
		FileReader fr = new FileReader(OriginRuleSet);
		StreamTokenizer token = new StreamTokenizer(fr);
		token.eolIsSignificant(true);
		String str;
		int tmp;
		int i=1; // ルールナンバー記入用
		boolean eof = false;
		tmp = token.nextToken();
		tmp = token.nextToken();
		pw.print("R1: ");
		while(!eof){
			tmp = token.nextToken();
			switch(tmp){
			case StreamTokenizer.TT_NUMBER:
				pw.print(BigDecimal.valueOf(token.nval)+" ");
				break;
			case StreamTokenizer.TT_WORD:
				str = token.sval.replaceAll("[0-9]", "");
				if(str.equals("R")){
					pw.print("R"+i+": ");
				}
				if(str.equals("D")){
					pw.print(token.sval);
					tmp = token.nextToken();
					pw.print("=");
					tmp = token.nextToken();
					pw.print((int)token.nval+" ");
				}
				if(str.equals("C")){
					pw.print(token.sval);
					tmp = token.nextToken();
					if(tmp == '>'){
						pw.print(">");
						tmp = token.nextToken();
					}
					if(tmp == '<'){
						pw.print("<");
						tmp = token.nextToken();
					}
					if(tmp == '='){
						pw.print("=");
						tmp = token.nextToken();
					}
					pw.print(BigDecimal.valueOf(token.nval)+" ");
				}
				if(str.equals("then")){
					pw.print(token.sval+" ");
					tmp = token.nextToken();
					pw.print(token.sval+" ");
					tmp = token.nextToken();
					pw.println((int)token.nval);
				}
				break;
			case StreamTokenizer.TT_EOL:
				i++;  //改行の数でルールの数を把握する
				break;
				//			
			case StreamTokenizer.TT_EOF:
				eof = true; 
				break;
				//			
			default:
				if(tmp == '=') pw.print("=");
				else if(tmp == '>') pw.print(">");
				else if(tmp == '<') pw.print("<");
				break;
			}
		}
		pw.close();
	}

}