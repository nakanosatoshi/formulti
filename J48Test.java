package fullauto;

import java.awt.BorderLayout;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import javax.swing.JFrame;
import javax.xml.crypto.dsig.spec.XPathType.Filter;

import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.trees.J48;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;
import weka.filters.*;
import weka.filters.unsupervised.attribute.Remove;
import weka.gui.treevisualizer.PlaceNode2;
import weka.gui.treevisualizer.TreeVisualizer;
/*
 * wekaのJ48を使用するクラスです。
 * arffファイルのみあれば使用可能です。
 */
public class J48Test {
	
	static public void J_48() throws Exception {
		//classnum:wekaにarffから取得してもらったクラス数を格納する
				//classname:wekaにarffから取得してもらった各クラス名を格納する
				int classnum;
				String[] classname = new String[100];

				Main.statusView.append("\n### "+Main.RuleName[Main.RoopNum-1] + Main.ProjectName + ".arffからルールを抽出します ###\n");
				// データ（事例）を ARFF ファイルから読み込む
				// プロジェクト直下においておくこと
				DataSource source = new DataSource("c:\\FullAutoReRX\\" + Main.ProjectName + "\\" + Main.RuleName[Main.RoopNum-1] + "\\" + Main.RuleName[Main.RoopNum-1] +".arff");
				Instances data = source.getDataSet();

				// 読み込んだデータセットにて，クラスを表す属性の番号を指定する
				// (注) weather データは最後の属性がクラス名を表すから，この指定でよい．
				// (注) numAttributes() は属性数を返す．
				data.setClassIndex(data.numAttributes() - 1);

				// 使うデータの表示
				//System.out.println(data);
				
				// 決定木のアルゴリズムとしてJ48を指定
				J48 j48 = new J48();
				// データをセット
				j48.buildClassifier(data);
				// 評価クラスの生成
				Evaluation eval = new Evaluation(data);
				// データを評価
				eval.evaluateModel(j48, data);
				// 評価結果の表示
				Main.statusView.append(eval.toSummaryString());
				Main.statusView.append(eval.toClassDetailsString());
				Main.statusView.append(eval.toMatrixString());

				//ルールツリーの表示
				File newfile1 = new File("c:\\FullAutoReRX\\" + Main.ProjectName + "\\" + Main.RuleName[Main.RoopNum-1] + "\\"+ Main.RuleName[Main.RoopNum-1] +"Tree.txt");
				try{
					if (newfile1.createNewFile()){
						Main.statusView.append(Main.RuleName[Main.RoopNum-1]+"Tree.txtの作成に成功しました\n");
					}else{
						Main.statusView.append(Main.RuleName[Main.RoopNum-1]+"Tree.txtはすでに存在しているため上書きします\n");
					}
				}catch(IOException e){
					System.out.println(e);
				}
				PrintWriter pw1 = new PrintWriter(new BufferedWriter(new FileWriter(newfile1)));
				Classifier cls = new weka.classifiers.trees.J48();
				cls.buildClassifier(data);
				pw1.println(cls);
				pw1.close();

				/*
				 *  ルールの出力をする部分
				 */ 		
				// 生成するjavaファイル名
				String className = Main.ProjectName;
				// javaコードの生成
				String cl = j48.toSource(className);
				// ファイルの生成
				PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter(
						new File("src/" + className + ".java"))));
				// ファイルにルールを書き込む
				pw.println(cl);
				// ファイルを閉じる
				pw.close();
				
				//　クラス数とクラス属性名が取得できたので、これを適当な変数にいれて
				//　値と名前を渡す	### マルチクラス時は処理を変更 ###
				classnum = data.classAttribute().numValues();
				for(int i = 0; i < classnum; i++){
					classname[i] = data.classAttribute().value(i);
				}
				Main.statusView.append("Class数: " + classnum+"\n");
				Main.statusView.append("Class 1: " + classname[0]+"\n");
				Main.statusView.append("Class 2: " + classname[1]+"\n");
				Main.statusView.update(Main.statusView.getGraphics());
				TreeToRule.TreeReading(classnum, classname);
	}

	static public void RecursiveJ_48(int Ri) throws Exception {
		//classnum:wekaにarffから取得してもらったクラス数を格納する
		//classname:wekaにarffから取得してもらった各クラス名を格納する
		int classnum;
		String[] classname = new String[100];

		Main.statusView.append("\n### "+Main.RuleName[Main.RoopNum-1] + Ri + ".arffからルールを抽出します ###\n");
		// データ（事例）を ARFF ファイルから読み込む
		// プロジェクト直下においておくこと
		DataSource source = new DataSource("c:\\FullAutoReRX\\" + Main.ProjectName + "\\" + Main.RuleName[Main.RoopNum-1] + "\\" + Main.RuleName[Main.RoopNum-1] + Ri + ".arff");
		Instances data = source.getDataSet();

		// 読み込んだデータセットにて，クラスを表す属性の番号を指定する
		// (注) weather データは最後の属性がクラス名を表すから，この指定でよい．
		// (注) numAttributes() は属性数を返す．
		data.setClassIndex(data.numAttributes() - 1);

		// 使うデータの表示
		//System.out.println(data);
		
		// 決定木のアルゴリズムとしてJ48を指定
		J48 j48 = new J48();
		// データをセット
		j48.buildClassifier(data);
		// 評価クラスの生成
		Evaluation eval = new Evaluation(data);
		// データを評価
		eval.evaluateModel(j48, data);
		// 評価結果の表示
		Main.statusView.append(eval.toSummaryString());
		Main.statusView.append(eval.toClassDetailsString());
		Main.statusView.append(eval.toMatrixString());

		//ルールツリーの表示
		File newfile1 = new File("c:\\FullAutoReRX\\" + Main.ProjectName + "\\" + Main.RuleName[Main.RoopNum-1] + "\\"+ Main.RuleName[Main.RoopNum-1] +"TreeRule" + Ri + ".txt");
		try{
			if (newfile1.createNewFile()){
				Main.statusView.append(Main.RuleName[Main.RoopNum-1] + "TreeRule" + Ri + ".txtの作成に成功しました\n");
			}else{
				Main.statusView.append(Main.RuleName[Main.RoopNum-1] + "TreeRule" + Ri + ".txtはすでに存在しているため上書きします\n");
			}
		}catch(IOException e){
			System.out.println(e);
		}
		PrintWriter pw1 = new PrintWriter(new BufferedWriter(new FileWriter(newfile1)));
		Classifier cls = new weka.classifiers.trees.J48();
		cls.buildClassifier(data);
		pw1.println(cls);
		pw1.close();

		/*
		 *  ルールの出力をする部分
		 */ 		
		// 生成するjavaファイル名
		String className = Main.RuleName[Main.RoopNum-1] + Ri;
		// javaコードの生成
		String cl = j48.toSource(className);
		// ファイルの生成
		PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter(
				new File("src/" + className + ".java"))));
		// ファイルにルールを書き込む
		pw.println(cl);
		// ファイルを閉じる
		pw.close();
		
		//　クラス数とクラス属性名が取得できたので、これを適当な変数にいれて
		//　値と名前を渡す	### マルチクラス時は処理を変更 ###
		classnum = data.classAttribute().numValues();
		for(int i = 0; i < classnum; i++){
			classname[i] = data.classAttribute().value(i);
		}
		Main.statusView.append("Class数: " + classnum+"\n");
		Main.statusView.append("Class 1: " + classname[0]+"\n");
		Main.statusView.append("Class 2: " + classname[1]+"\n");
		Main.statusView.update(Main.statusView.getGraphics());
		TreeToRule.RecursiveTreeReading(classnum, classname, Ri);
	}
}