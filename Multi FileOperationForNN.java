/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package autorerxxx;

/**
 *
 * 
 */
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * ニューラルネットワークのためのファイル処理を担当するクラス
 *
 */
class FileOperationForNN {

    /** サンプル数 */
    private int numOfSamples;
    /** 出力数 */
    private int numOfOutputs;
    /** 入力数 */
    private int numOfInputs;
    /** 全サンプルの入力を格納した配列 */
    double[][] input;
    /** 全サンプルの目標出力を格納した配列 */
    double[][] target;
    /** 離散値（整数）か実数値（連続値）かを判別するためのフラグ */
    private AttributeType[] attributeType;
    private String aLabel = null;

    /** brの文字列を返す関数　*/
    private String[][] splitData(BufferedReader br) {
        String[][] temp = new String[numOfSamples][numOfInputs + numOfOutputs];
        for(int i = 0; i < numOfSamples; i++) {
            try {
                System.out.println("i: "+i);
                temp[i] = br.readLine().split(",");//サンプル数の数だけ","を読み込む

            } catch (IOException ex) {
                Logger.getLogger(FileOperationForNN.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return temp;
    }




    private FileOperationForNN() {
        // 引数なしでのコンストラクタ呼び出し禁止
    }

    public FileOperationForNN(File file) {
    	
    	
        BufferedReader br;
        try {
            br = new BufferedReader(new FileReader(file));
            // サンプル数の読み込み（ファイル１行目）
            numOfSamples = Integer.parseInt(br.readLine());
            // 入力数の読み込み（ファイル２行目）
            numOfInputs = Integer.parseInt(br.readLine());
            // 出力数の読み込み（ファイル３行目）
            numOfOutputs = Integer.parseInt(br.readLine());
            // データラベルを格納
            aLabel = br.readLine();
            // データラベルの配列
            String[] aLabelArray = aLabel.split(",");

            attributeType = new AttributeType[numOfInputs];
            input = new double[numOfSamples][numOfInputs];
            target = new double[numOfSamples][numOfOutputs];

            // カンマで区切ってある数値を配列にセット
            String[][] temp_s = splitData(br);
                                   
    /*        //確認
            for(int a=0;a<numOfSamples;a++){
            	for(int b=0;b<numOfInputs;b++)
            	System.out.print(temp_s[a][b]+",");
            	System.out.println();
            }
*/
            // 文字列としての数値を実数に変換
            for (int i = 0; i < numOfSamples; i++) {
                for (int j = 0; j < numOfInputs; j++) {
                    input[i][j] = Double.parseDouble(temp_s[i][j]);
                }
                for (int k = 0; k < numOfOutputs; k++) {
                    target[i][k] = Double.parseDouble(temp_s[i][numOfInputs + k]);
                }
            }
           
       
           //input変換後の確認 
            for(int a=0;a<numOfSamples;a++){
            	for(int b=0;b<numOfInputs;b++)
            	System.out.print(input[a][b]+",");
            	System.out.println();
            }

            // 属性タイプテーブルの初期化
            for (int i = 0; i < numOfInputs; i++) {
                attributeType[i] = new AttributeType();
                attributeType[i].setWritable(false);
            }
            if (aLabel.equals("#")) {
                for (int i = 0; i < numOfInputs; i++) {

                    if (temp_s[0][i].equals("0") || temp_s[0][i].equals("1")) {
                        attributeType[i].setLabel("D" + String.valueOf(i + 1));
                        //attributeType[i].setType("continuous");
                        attributeType[i].setType("discrete 2");
                    } else {
                        attributeType[i].setLabel("C" + String.valueOf(i + 1));
                        attributeType[i].setType("ignore");
                    }
                }
            } else {
                for(int i = 0; i < numOfInputs; i++) {
                    attributeType[i].setLabel(aLabelArray[i]);
                    if (temp_s[0][i].equals("0") || temp_s[0][i].equals("1")) {
                        //attributeType[i].setType("continuous");
                        attributeType[i].setType("discrete 2");
                    } else {
                        attributeType[i].setType("ignore");
                    }
                }
            }
        } catch (IOException e) {
            System.err.println(e);
            System.exit(1);
        }
    }

    /**
     * 正規化を行うメソッド
     */
    public void normalize() {
        double max = input[0][0];
        double min = input[0][0];
        boolean sign = false;       // 符号判別フラグ（falseで正、trueで負）

        // 入力に負数が含まれているかの判定、および入力の最大値・最小値の選定
        for (int i = 0; i < numOfSamples; i++) {
            for (int j = 0; j < numOfInputs; j++) {
                if (input[i][j] < 0) {
                    sign = true;
                }
                if (input[i][j] > max) {
                    max = input[i][j];
                }
                if (input[i][j] < min) {
                    min = input[i][j];
                }
            }
        }

        // 入力に負数が含まれていた場合の正規化
        if (sign) {
            min = Math.abs(min);
            for (int i = 0; i < numOfSamples; i++) {
                for (int j = 0; j < numOfInputs; j++) {
                    input[i][j] = (input[i][j] + min) / (max + min);
                }
            }
        } else {
            for (int i = 0; i < numOfSamples; i++) {
                for (int j = 0; j < numOfInputs; j++) {
                    input[i][j] = (input[i][j] - min) / (max - min);
                }
            }
        }
    }
    // 以下getterおよびsetter
    public double[][] getInput() {
        return input;
    }

    public double getInput(int i, int j) {
        return input[i][j];
    }

    public double[][] getTarget() {
        return target;
    }

    public int getNumOfSamples() {
        return numOfSamples;
    }

    public int getNumOfInputs() {
        return numOfInputs;
    }

    public int getNumOfOutputs() {
        return numOfOutputs;
    }

    public String getLabel(int i) {
        return attributeType[i].getLabel();
    }

    public String getType(int i) {
        return attributeType[i].getType();
    }

    public void setWritable(int i, boolean b) {
        attributeType[i].setWritable(b);
    }

    public boolean getWritable(int i) {
        return attributeType[i].getWritable();
    }
}


